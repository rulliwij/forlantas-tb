const path = require('path')
const manifestJSON = require('./public/manifest.json')

const GATEWAY_URL = process.env.VUE_APP_GATEWAY_URL;
const BUILD_DIRECTORY = "app";

let publicPath = '/';
let buildDirectory = '';
let proxyBaseUrl = GATEWAY_URL;

if (process.env.NODE_ENV === 'production') {
  publicPath = GATEWAY_URL;
  buildDirectory = '/' + BUILD_DIRECTORY;
  proxyBaseUrl = '';
}

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  publicPath: publicPath + buildDirectory,
  outputDir: path.resolve(__dirname, '../public' + buildDirectory),
  devServer: {
    proxy: {
      '/api': {
        target: proxyBaseUrl + '/api',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
  pwa: {
    themeColor: manifestJSON.theme_color,
    name: manifestJSON.short_name,
    msTileColor: manifestJSON.background_color,
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'service-worker.js',
    },
  },
}