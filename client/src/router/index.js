import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";
import storage from "@/plugins/storage";

import TheLoginLayout from "@/views/TheLoginLayout";
import ThePOSLayout from "@/views/ThePOSLayout";
import The404Layout from "@/views/The404Layout";

Vue.use(VueRouter);

let token = storage.get("token");
let userData = storage.get("userData");
// console.log(userData);

if (token && userData) {
  store.commit("SET", {
    key: "token",
    value: token,
  });

  store.commit("SET", {
    key: "userData",
    value: userData,
  });

  store.commit("SET", {
    key: "isLogedIn",
    value: true,
  });
}

let routeOwner = userData.user_user_group_id == 1;
let routeAdmin = userData.user_user_group_id == 2;
let routeKasir = userData.user_user_group_id == 3;

const routes = [{
    path: "/",
    name: "dashboard",
    component: () => import("@/components/TheDashboard"),
    meta: {
      title: "Dashboard",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/user",
    name: "user",
    component: () => import("@/components/TheUser"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Karyawan",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/category",
    name: "category",
    component: () => import("@/components/TheCategoryRevolution"),
    meta: {
      title: "Kategori Produk",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/product",
    name: "product",
    component: () => import("@/components/TheProductRevolution"),
    meta: {
      title: "Produk",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/product_wawan",
    name: "product_wawan",
    component: () => import("@/components/TheProduct"),
    meta: {
      title: "Produk",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/ingredient",
    name: "ingredient",
    component: () => import("@/components/TheIngredientRevolution"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Bahan Baku",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/cashier",
    name: "cashier",
    component: () => import("@/components/TheCashier"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Laporan Kas",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/stock_adjustment",
    name: "stock_adjustment",
    component: () => import("@/components/TheStockAdjustment"),
    meta: {
      title: "Data Penyesuaian Stok",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/stock",
    name: "stock",
    component: () => import("@/components/TheStock"),
    beforeEnter: (to, from, next) => {
      if (!routeKasir) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Data Monitoring Stok",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/stock_log",
    name: "stock_log",
    component: () => import("@/components/TheStockLog"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Laporan Riwayat Stok",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/customer",
    name: "customer",
    component: () => import("@/components/TheCustomer"),
    meta: {
      title: "Data Customer",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/pos",
    name: "pos",
    component: () => import("@/components/ThePOS"),
    meta: {
      title: "Penjualan",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/pos-revolution",
    name: "posRevolution",
    component: () => import("@/components/ThePosRevolution"),
    meta: {
      title: "Penjualan",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/components/TheLogin"),
    meta: {
      title: "Login",
      layout: TheLoginLayout,
    },
  },
  {
    path: "/sales-type",
    name: "salesType",
    component: () => import("@/components/TheSalesType"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Tipe Penjualan",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/discount",
    name: "discount",
    component: () => import("@/components/TheDiscount"),
    beforeEnter: (to, from, next) => {
      if (!routeKasir) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Diskon",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/shift",
    name: "shift",
    component: () => import("@/components/TheShift"),
    meta: {
      title: "Shift",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/recipe",
    name: "recipe",
    component: () => import("@/components/TheRecipeRevolution"),
    // Privillage
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Resep",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/report",
    name: "report",
    component: () => import("@/components/TheReport"),
    // Privillage
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Laporan Penjualan",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/report_purchase",
    name: "report_purchase",
    component: () => import("@/components/TheLabaRugiPembelian"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Laporan Laba/Rugi Pembelian",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/report_laba_rugi",
    name: "report_laba_rugi",
    component: () => import("@/components/TheLabaRugiReport"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Laporan Laba/Rugi",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/report_profit",
    name: "report_profit",
    component: () => import("@/components/TheReportProfit"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Laporan Laba / Rugi",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/profit",
    name: "profit",
    component: () => import("@/components/TheReportProfitAndi"),
    beforeEnter: (to, from, next) => {
      if (routeOwner) {
        next()
      } else {
        next({
          name: 'notFound'
        });
      }
    },
    meta: {
      title: "Laporan Laba / Rugi",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "/po",
    name: "po",
    component: () => import("@/components/ThePO"),
    meta: {
      title: "Pembelian",
      auth: "admin",
      layout: ThePOSLayout,
    },
  },
  {
    path: "*",
    name: "notFound",
    meta: {
      title: "404",
      auth: "public",
      layout: The404Layout,
    },
  },
];

let routeConfig = {
  mode: "history",
  base: process.env.BASE_URL,
  routes,
};

if (process.env.NODE_ENV === 'production') {
  routeConfig.base = "/app";
}

const router = new VueRouter(routeConfig);

router.beforeEach((to, from, next) => {
  window.document.title = to.meta.title + " | next POS";

  // cek ketika user belum login
  if (to.meta.auth === "admin" && store.state.isLogedIn === false) {
    next({
      path: "/login",
    });
    return;
  }

  // jika sudah login tapi mengakses halaman login, maka kembalikan ke halaman dashboard
  if (store.state.isLogedIn === true) {
    if ((to.path == "/login") === true) {
      next({
        path: "/",
      });
      return;
    }
  }

  next();
});

export default router;