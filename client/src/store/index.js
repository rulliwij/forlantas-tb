import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: "",
    userData: {},
    isLogedIn: false,
    loading: false,
    networkNotFound: false,
    networkError: false,
    expiredSession: false,
    expiredMessage: '',
    darkMode: false,
    dialogShiftDetail: false,
    dialogSOPending: false,
    dialogSOComplete: false,
    dialogInputCash: false,
    dialogDatePicker: false,
    dialogEditPin: false,
    dialogCheckPin: false,
    totalSOPending: 0,
    dialogEditProfile: false,

    // Product
    dialogAddProduct: false,
    dialogUpdateProduct: false,

    // Ingredients
    dialogAddIngredients: false,
    dialogUpdateIngredients: false,

    // Recipe
    dialogAddRecipe: false,
    dialogUpdateRecipe: false,
    dialogDetailRecipe: false,

    // Category
    dialogAddCategory: false,
    dialogUpdateCategory: false,

    intervalNotificationStock: null,

    // POS KASIR
    pos: {
      loadingFetch: {
        dialog: false,
        text: "",
      },
      overlayLoading: false,
      snackbar: {
        show: false,
        text: ""
      },
      selectedCategory: 0,
      searchMenu: null,

      //======= DATA MASTER ========
      products: [],
      customers: [],
      categories: [],
      salesTypes: [],
      discountsItem: [],
      discountsGlobal: [],

      //====== DATA CART =========
      cart: {
        dialogEdit: false,
        dialogDiscount: false,
        itemEdit: {
          dialog: false,
          index: 0,
          data: {
            id: "0",
            category_id: "0",
            name: "",
            image: "",
            price: 0,
            sellingPrice: 0,
            qty: 0,
            discountTitle: "",
            discountPercent: 0,
            discountNominal: 0,
            stock: 0,
            stockWarning: false
          }
        },
        holdId: 0,
        customer: {
          text: "Umum",
          value: "0",
        },
        table: "Tanpa Meja",
        salesType: {
          text: "Dine In",
          slug: "dine_in",
        },
        products: [],
        subTotal: 0,
        discountTitle: "",
        totalDiscountPercent: 0,
        totalDiscountNominal: 0,
        totalPayable: 0,
      },
    }

  },
  mutations: {
    SET(state, {
      key,
      value
    }) {
      state[key] = value;
    },
  },
  actions: {},
  modules: {}
})