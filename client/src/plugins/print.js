import Vue from 'vue'
import dayjs from 'dayjs';
import helpers from '@/plugins/helpers'
import 'dayjs/locale/id'

const print = {
  debug: false, //set false in production

  footMark: '(Powered by next-pos.my.id)',

  //initialize data transaction
  dataTrans: {
    companyName: "Next POS",
    companyAddress: "Yogyakarta",
    companyPhone: "082135965203",

    orderNumber: "0001",
    orderDate: new Date(),
    orderTransNumber: "INV/00001",
    orderAdmin: "Setyo",
    orderTable: "Tanpa Meja",
    orderCustomer: "Wawan",
    orderSalesType: "Dine In",

    items: [{
      name: "Kopi Americano",
      qty: 2,
      price: 18000,
      discountName: "",
      discountPercent: 0,
      discountNominal: 1800
    }],

    totalPrice: 0,
    discountName: "#MAKANHAPPY",
    totalDiscountPercent: 0,
    totalDiscountNominal: 1000,
    totalPayable: 0,
    paymentMethod: 'Tunai',
    totalPaid: 0,
    change: 0,
  },

  //initialize data order
  dataOrder: {
    orderNumber: "0001",
    orderDate: new Date(),
    orderTable: "Tanpa Meja",
    orderCustomer: "Wawan",
    orderSalesType: "Dine In",

    items: [{
      name: "Kopi Americano",
      qty: 2,
      note: "Tidak pakai gula"
    }],
  },

  //initialize data closing
  dataClosing: {
    companyName: "Next POS",
    companyAddress: "Yogyakarta",
    companyPhone: "082135965203",

    date: new Date(),
    admin: "Setyo",

    cashIn: 0,
    cashOut: 0,
    totalCash: 0,

    salesCash: 0,
    salesEdc: 0,
    salesTransfer: 0,
    salesOvo: 0,
    salesGopay: 0,
    // salesRefund: 0,

    totalNonCash: 0,
    totalSales: 0,

    details: [{
      transactionNumber: "INV/00001",
      date: new Date(),
      paymentMethod: "Cash",
      grandTotal: 0
    }]
  },

  //PRINT mini 58mm untuk aplikasi RawBt Android
  mini58RawBtUniAndroidTrans() {
    const _self = this;

    //COMMAND PRINTER ESC/POS (tergantung printer)
    const ESC = `\u001B`;
    const GS = `\u001D`;
    const boldOn = `${ESC}E\u0001`;
    const boldOff = `${ESC}E\0`;
    const doubleOn = `${GS}!\u0011`; // 2x sized text (double-high + double-wide)
    const doubleOff = `${GS}!\0`;
    const centerOn = `${ESC}a\u0001`;
    const centerOff = `${ESC}a\0`;
    const leftOn = `${ESC}a\u0000`;
    const leftOff = `${ESC}a\0`;
    const rightOn = `${ESC}a\u0002`;
    const rightOff = `${ESC}a\0`;
    const enter = `\u000A`;
    const space = ` `;
    const divider = `--------------------------------`;

    //build string untuk printing intent ke aplikasi rawBt
    //NOTE: untuk 58mm maksimal 32 character
    //       12345678901234567890123456789012

    let text = ``;

    //===== HEADER BLOCK START ======
    text += centerOn;

    //company name
    if (_self.dataTrans.companyName) {
      text += `${boldOn}`;
      text += `${_self.centerText32(_self.dataTrans.companyName, enter)}`;
      text += `${boldOff}`;

      text += `${enter}`;
    }

    //company address
    if (_self.dataTrans.companyAddress) {
      text += `${_self.centerText32(_self.dataTrans.companyAddress, enter)}`;

      text += `${enter}`;
    }

    //company phone
    if (_self.dataTrans.companyPhone) {
      text += `${_self.centerText32(_self.dataTrans.companyPhone, enter)}`;

    }
    text += centerOff;

    if (_self.dataTrans.companyName || _self.dataTrans.companyAddress || _self.dataTrans.companyPhone) {
      text += `${enter}${enter}`;
    }
    //===== HEADER BLOCK END ======


    //===== ORDER INFO START ======
    text += `No. Order: ${_self.dataTrans.orderNumber}`;
    text += `${enter}`;
    text += `Tanggal  : ${dayjs(_self.dataTrans.orderDate).format("DD MMM YYYY HH:mm")}`;
    text += `${enter}`;
    text += `No. Struk: ${_self.dataTrans.orderTransNumber}`;
    text += `${enter}`;
    text += `Kasir    : ${_self.dataTrans.orderAdmin}`;
    text += `${enter}`;

    if (_self.dataTrans.orderTable) {
      text += `Meja     : ${_self.dataTrans.orderTable}`;
      text += `${enter}`;
    }

    if (_self.dataTrans.orderCustomer) {
      text += `Pelanggan: ${_self.dataTrans.orderCustomer}`;
      text += `${enter}`;
    }

    text += `${divider}`;
    text += `${enter}`;

    text += `${centerOn}`;
    text += `${_self.centerText32('*' + _self.dataTrans.orderSalesType.toUpperCase() + '*', enter)}`;

    text += `${enter}${enter}`;
    text += `${centerOff}`;

    //===== ORDER INFO END ======


    //===== ORDER ITEMS START ======

    if (_self.dataTrans.items.length > 0) {
      _self.dataTrans.items.forEach(function (item, index) {
        text += `${boldOn}`;
        text += `${item.name}`;
        text += `${boldOff}`;
        text += `${enter}`

        text += `${_self.centerSpace32(item.qty + "x @" + helpers.numberFormat(item.price, 0, ",", "."), helpers.numberFormat(item.price * item.qty, 0, ",", "."))}`;
        text += `${enter}`;

        if (item.discountNominal > 0) {
          if (item.discountPercent > 0) {
            text += `${_self.centerSpace32("Diskon (" + item.discountPercent + "%) @" + helpers.numberFormat(item.discountNominal, 0, ",", "."), helpers.numberFormat((item.price - item.discountNominal) * item.qty, 0, ",", "."))}`;
          } else {
            text += `${_self.centerSpace32("Diskon @" + helpers.numberFormat(item.discountNominal, 0, ",", "."), helpers.numberFormat((item.price - item.discountNominal) * item.qty, 0, ",", "."))}`;
          }
          text += `${enter}`;

          if (item.discountName != ``) {
            text += `(${item.discountName})`;
            text += `${enter}`;
          }
        }
      });
    }

    text += `${divider}`;
    text += `${enter}`;

    //===== ORDER ITEMS END ======

    //===== FOOTER BLOCK START =====

    text += `${_self.centerSpace32("Subtotal", helpers.numberFormat(_self.dataTrans.totalPrice, 0, ",", "."))}`;
    text += `${enter}`;

    if (_self.dataTrans.totalDiscountNominal > 0) {
      if (_self.dataTrans.totalDiscountPercent > 0) {
        text += `${_self.centerSpace32("Diskon (" + _self.dataTrans.totalDiscountPercent + "%)", helpers.numberFormat(_self.dataTrans.totalDiscountNominal, 0, ",", "."))}`;
      } else {
        text += `${_self.centerSpace32("Diskon", helpers.numberFormat(_self.dataTrans.totalDiscountNominal, 0, ",", "."))}`;
      }
      text += `${enter}`;

      if (_self.dataTrans.discountName != ``) {
        text += `(${_self.dataTrans.discountName})`;
        text += `${enter}`;
      }
    }

    text += `${divider}`;
    text += `${enter}`;

    text += `${boldOn}`;
    text += `${_self.centerSpace32("Total", helpers.numberFormat(_self.dataTrans.totalPayable, 0, ",", "."))}`;
    text += `${boldOff}`;
    text += `${enter}`;

    text += `${divider}`;
    text += `${enter}`;

    text += `${_self.centerSpace32(helpers.capitalizeFirstLetter(_self.dataTrans.paymentMethod), helpers.numberFormat(_self.dataTrans.totalPaid, 0, ",", "."))}`;
    text += `${enter}`;

    if (_self.dataTrans.change > 0) {
      text += `${_self.centerSpace32("Kembali", helpers.numberFormat(_self.dataTrans.change, 0, ",", "."))}`;
      text += `${enter}`;
    }

    text += `${divider}`;
    text += `${enter}`;

    text += centerOn;
    text += `${enter}${enter}`;
    text += `${_self.centerText32(_self.footMark, enter)}`;
    text += centerOff;

    //===== FOOTER BLOCK END =====

    if (_self.debug) {
      console.log(text);
    } else {
      const S = "#Intent;scheme=rawbt;";
      const P = "package=ru.a402d.rawbtprinter;end;";
      const textEncoded = encodeURI(text);
      window.location.href = "intent:" + textEncoded + S + P;
    }
  },

  mini58RawBtUniAndroidClosing() {
    const _self = this;

    //COMMAND PRINTER ESC/POS (tergantung printer)
    const ESC = `\u001B`;
    const GS = `\u001D`;
    const boldOn = `${ESC}E\u0001`;
    const boldOff = `${ESC}E\0`;
    const doubleOn = `${GS}!\u0011`; // 2x sized text (double-high + double-wide)
    const doubleOff = `${GS}!\0`;
    const centerOn = `${ESC}a\u0001`;
    const centerOff = `${ESC}a\0`;
    const leftOn = `${ESC}a\u0000`;
    const leftOff = `${ESC}a\0`;
    const rightOn = `${ESC}a\u0002`;
    const rightOff = `${ESC}a\0`;
    const enter = `\u000A`;
    const space = ` `;
    const divider = `--------------------------------`;

    //build string untuk printing intent ke aplikasi rawBt
    //NOTE: untuk 58mm maksimal 32 character
    //       12345678901234567890123456789012

    let text = ``;

    //===== HEADER BLOCK START ======
    text += centerOn;

    //company name
    if (_self.dataClosing.companyName) {
      text += `${boldOn}`;
      text += `${_self.centerText32(_self.dataClosing.companyName, enter)}`;
      text += `${boldOff}`;

      text += `${enter}`;
    }

    //company address
    if (_self.dataClosing.companyAddress) {
      text += `${_self.centerText32(_self.dataClosing.companyAddress, enter)}`;

      text += `${enter}`;
    }

    //company phone
    if (_self.dataClosing.companyPhone) {
      text += `${_self.centerText32(_self.dataClosing.companyPhone, enter)}`;

    }
    text += centerOff;

    if (_self.dataClosing.companyName || _self.dataClosing.companyAddress || _self.dataClosing.companyPhone) {
      text += `${enter}${enter}`;
    }
    //===== HEADER BLOCK END ======


    text += `Tanggal  : ${dayjs(_self.dataClosing.date).format("DD MMM YYYY HH:mm")}`;
    text += `${enter}`;
    text += `Kasir    : ${_self.dataClosing.admin}`;
    text += `${enter}`;

    text += `${divider}`;
    text += `${enter}`;

    text += `${centerOn}`;
    text += `${_self.centerText32('*TUTUP KASIR*', enter)}`;

    text += `${enter}${enter}`;
    text += `${centerOff}`;

    text += `${_self.centerSpace32("Kas Masuk", helpers.numberFormat(_self.dataClosing.cashIn, 0, ",", "."))}`;
    text += `${enter}`;
    text += `${_self.centerSpace32("Kas Keluar", helpers.numberFormat(_self.dataClosing.cashOut, 0, ",", "."))}`;
    text += `${enter}`;

    text += `${boldOn}`;
    text += `${_self.centerSpace32("TOTAL KAS", helpers.numberFormat(_self.dataClosing.totalCash, 0, ",", "."))}`;
    text += `${boldOff}`;
    text += `${enter}${enter}`;

    text += `${boldOn}`;
    text += `${_self.centerSpace32("Penjualan Tunai", helpers.numberFormat(_self.dataClosing.salesCash, 0, ",", "."))}`;
    text += `${boldOff}`;
    text += `${enter}${enter}`;

    text += `${_self.centerSpace32("Penjualan EDC", helpers.numberFormat(_self.dataClosing.salesEdc, 0, ",", "."))}`;
    text += `${enter}`;
    text += `${_self.centerSpace32("Penjualan Transfer", helpers.numberFormat(_self.dataClosing.salesTransfer, 0, ",", "."))}`;
    text += `${enter}`;
    text += `${_self.centerSpace32("Penjualan OVO", helpers.numberFormat(_self.dataClosing.salesOvo, 0, ",", "."))}`;
    text += `${enter}`;
    text += `${_self.centerSpace32("Penjualan GOPAY", helpers.numberFormat(_self.dataClosing.salesGopay, 0, ",", "."))}`;
    text += `${enter}`;
    // text += `${_self.centerSpace32("Penjualan Refund", helpers.numberFormat(_self.dataClosing.salesRefund, 0, ",", "."))}`;
    // text += `${enter}`;

    text += `${boldOn}`;
    text += `${_self.centerSpace32("TOTAL NON TUNAI", helpers.numberFormat(_self.dataClosing.totalNonCash, 0, ",", "."))}`;
    text += `${boldOff}`;
    text += `${enter}${enter}`;

    text += `${boldOn}`;
    text += `${_self.centerSpace32("TOTAL PENJUALAN", helpers.numberFormat(_self.dataClosing.totalSales, 0, ",", "."))}`;
    text += `${boldOff}`;
    text += `${enter}`;

    text += `${divider}`;
    text += `${enter}`;

    if (_self.dataClosing.details.length > 0) {
      text += `${centerOn}`;
      text += `${_self.centerText32('*DETAIL TRANSAKSI*', enter)}`;

      text += `${enter}${enter}`;
      text += `${centerOff}`;

      let totalGrandTotal = 0;
      _self.dataClosing.details.forEach(item => {
        totalGrandTotal += parseInt(item.grandTotal);
        text += `${_self.centerSpace32(item.transactionNumber, item.paymentMethod.toUpperCase())}`;
        text += `${enter}`;
        text += `${_self.centerSpace32(dayjs(item.date).format('HH:mm'),  helpers.numberFormat(item.grandTotal, 0, ",", "."))}`;
        text += `${enter}${enter}`;
      });

      text += `${_self.centerSpace32("TOTAL",  helpers.numberFormat(totalGrandTotal, 0, ",", "."))}`;
      text += `${enter}`;

      text += `${divider}`;
      text += `${enter}`;
    }

    text += centerOn;
    text += `${enter}${enter}`;
    text += `${_self.centerText32(_self.footMark, enter)}`;
    text += centerOff;

    if (_self.debug) {
      console.log(text);
    } else {
      const S = "#Intent;scheme=rawbt;";
      const P = "package=ru.a402d.rawbtprinter;end;";
      const textEncoded = encodeURI(text);
      window.location.href = "intent:" + textEncoded + S + P;
    }
  },

  mini58RawBtUniAndroidOrder() {
    const _self = this;

    //COMMAND PRINTER ESC/POS (tergantung printer)
    const ESC = `\u001B`;
    const GS = `\u001D`;
    const boldOn = `${ESC}E\u0001`;
    const boldOff = `${ESC}E\0`;
    const doubleOn = `${GS}!\u0011`; // 2x sized text (double-high + double-wide)
    const doubleOff = `${GS}!\0`;
    const centerOn = `${ESC}a\u0001`;
    const centerOff = `${ESC}a\0`;
    const leftOn = `${ESC}a\u0000`;
    const leftOff = `${ESC}a\0`;
    const rightOn = `${ESC}a\u0002`;
    const rightOff = `${ESC}a\0`;
    const enter = `\u000A`;
    const space = ` `;
    const divider = `--------------------------------`;

    //build string untuk printing intent ke aplikasi rawBt
    //NOTE: untuk 58mm maksimal 32 character
    //       12345678901234567890123456789012

    let text = ``;

    text += `No. Order: ${_self.dataOrder.orderNumber}`;
    text += `${enter}`;
    text += `Tanggal  : ${dayjs(_self.dataOrder.orderDate).format("DD MMM YYYY HH:mm")}`;
    text += `${enter}`;

    if (_self.dataOrder.orderTable) {
      text += `Meja     : ${_self.dataOrder.orderTable}`;
      text += `${enter}`;
    }

    if (_self.dataOrder.orderCustomer) {
      text += `Pelanggan: ${_self.dataOrder.orderCustomer}`;
      text += `${enter}`;
    }

    text += `${divider}`;
    text += `${enter}`;

    text += `${centerOn}`;
    text += `${_self.centerText32('*' + _self.dataOrder.orderSalesType.toUpperCase() + '*', enter)}`;

    text += `${enter}${enter}`;
    text += `${centerOff}`;

    if (_self.dataOrder.items.length > 0) {
      _self.dataOrder.items.forEach(function (item, index) {
        text += `${boldOn}`;
        text += `${item.qty}x ${item.name}`;
        text += `${boldOff}`;
        text += `${enter}`

        if (item.note) {
          text += `${item.note}`;
          text += `${enter}`
        }

        text += `${enter}`
      });
    }

    text += `${divider}`;
    text += `${enter}`;

    text += centerOn;
    text += `${enter}${enter}`;
    text += `${_self.centerText32(_self.footMark, enter)}`;
    text += centerOff;

    if (_self.debug) {
      console.log(text);
    } else {
      const S = "#Intent;scheme=rawbt;";
      const P = "package=ru.a402d.rawbtprinter;end;";
      const textEncoded = encodeURI(text);
      window.location.href = "intent:" + textEncoded + S + P;
    }
  },

  //PRINT mini 58mm untuk aplikasi Pass PRNT Star MPop Ipad
  mini58PassPRNTStarMPopIpadTrans() {
    let passprnt_uri = "starpassprnt://v1/print/nopreview?";

    let _self = this;

    let header = '';

    if (_self.dataTrans.companyName) {
      header += `
        <tr>
            <td>${_self.dataTrans.companyName}</td>
        </tr>
      `;
    }

    //company address
    if (_self.dataTrans.companyAddress) {
      header += `
        <tr>
           <td>${_self.dataTrans.companyAddress}</td>
        </tr>
      `;
    }

    //company phone
    if (_self.dataTrans.companyPhone) {
      header += `
        <tr>
            <td>${_self.dataTrans.companyPhone}</td>
        </tr>
      `;
    }


    let orderInfo = '';

    if (_self.dataTrans.orderTable) {
      orderInfo += `
        <tr>
            <td>Meja</td>
            <td>:</td>
            <td>${this.dataTrans.orderTable.toUpperCase()}</td>
        </tr>
      `;
    }

    if (_self.dataTrans.orderCustomer) {
      orderInfo += `
        <tr>
            <td>Pelanggan</td>
            <td>:</td>
            <td>${_self.dataTrans.orderCustomer}</td>
        </tr>
      `;
    }

    let cartProduct = '';
    if (_self.dataTrans.items.length > 0) {
      _self.dataTrans.items.forEach(function (item, index) {
        cartProduct += `
            <tr>
              <td colspan="3" style="font-weight: bold; padding-top: 5px;">${item.name}</td>
            </tr>
            <tr>
              <td style="width: 15%;">${helpers.numberFormat(item.qty, 0, ",", ".")}x</td>
              <td style="width: 40%;">@${helpers.numberFormat(item.price, 0, ",", ".")}</td>
              <td style="width: 45%; text-align: right;">${helpers.numberFormat(item.price * item.qty, 0, ",", ".")}</td>
            </tr>
          `;

        if (item.discountNominal > 0) {
          if (item.discountPercent > 0) {
            cartProduct += `
              <tr>
                <td style="width: 30%;">Diskon (${item.discountPercent}%)</td>
                <td style="width: 20%;">@${helpers.numberFormat(item.discountNominal, 0, ",", ".")}</td>
                <td style="width: 50%; text-align: right;">${helpers.numberFormat((item.price - item.discountNominal) * item.qty, 0, ",", ".")}</td>
              </tr>
            `;
          } else {
            cartProduct += `
              <tr>
                <td colspan="2" style="width: 50%;">Diskon @${helpers.numberFormat(item.discountNominal, 0, ",", ".")}</td>
                <td style="width: 50%; text-align: right;">${helpers.numberFormat((item.price - item.discountNominal) * item.qty, 0, ",", ".")}</td>
              </tr>
            `;
          }

          if (item.discountName != ``) {
            cartProduct += `
              <tr>
                <td colspan="3" style="padding-top: 5px;">(${item.discountName})</td>
              </tr>
            `;
          }
        }
      })
    }

    let discount = '';
    if (_self.dataTrans.totalDiscountNominal > 0) {
      if (_self.dataTrans.totalDiscountPercent > 0) {
        discount += `
           <tr>
              <td style="width: 50%;">Diskon (${_self.dataTrans.totalDiscountPercent}%)</td>
              <td style="width: 50%; text-align: right;">
              ${helpers.numberFormat(_self.dataTrans.totalDiscountNominal, 0, ",", ".")}
              </td>
          </tr>
        `;
      } else {
        discount += `
           <tr>
              <td style="width: 50%;">Diskon</td>
              <td style="width: 50%; text-align: right;">
              ${helpers.numberFormat(_self.dataTrans.totalDiscountNominal, 0, ",", ".")}
              </td>
          </tr>
        `;
      }

      if (_self.dataTrans.discountName != ``) {
        discount += `
           <tr>
              <td colspan="2" style="width: 100%;">(${_self.dataTrans.discountName})</td>
          </tr>
        `;
      }
    }

    let change = '';
    if (_self.dataTrans.change > 0) {
      change = `
          <tr>
            <td style="width: 50%;">Kembali</td>
            <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataTrans.change, 0, ",", ".")}</td>
          </tr>
        `;
    }

    // passprnt_uri += "back=#";
    // passprnt_uri += "&popup=disable";

    let html = `
        <html lang="en">
          <head>
              <meta charset="UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <meta http-equiv="X-UA-Compatible" content="ie=edge">
              <title>Print</title>
          </head>
          <body style="font-size: 25px; font-family: 'Inconsolata', monospace;">
              <div style="width: 110mm; max-width: 110mm;">
                  <table style="width: 90%; font-size: 25px; text-align: center;">
                     ${header}
                  </table>
                  <table style="width: 90%; font-size: 25px;">
                      <tr>
                          <td style="width: 30%">No. Order</td>
                          <td style="width: 2%">:</td>
                          <td style="width: 68%">${_self.dataTrans.orderNumber}</td>
                      </tr>
                      <tr>
                          <td>Tanggal</td>
                          <td>:</td>
                          <td>
                            ${dayjs(_self.dataTrans.orderDate).format("DD MMM YYYY HH:mm")}
                          </td>
                      </tr>
                      <tr>
                          <td>No. Struk</td>
                          <td>:</td>
                          <td>${_self.dataTrans.orderTransNumber}</td>
                      </tr>
                      <tr>
                          <td>Kasir</td>
                          <td>:</td>
                          <td>${_self.dataTrans.orderAdmin}</td>
                      </tr>
                      ${orderInfo}
                  </table>
                  <table style="width: 90%; font-size: 25px;">
                      <tr>
                          <td>------------------------</td>
                      </tr>
                  </table>
                  <table style="width: 90%; font-size: 25px; font-weight: bold; text-align: center;">
                      <tr>
                          <td>*${_self.dataTrans.orderSalesType.toUpperCase()}*</td>
                      </tr>
                  </table>
                  <table style="width: 90%; font-size: 25px;">
                    
                  ${cartProduct}

                  </table>
                  <table style="width: 90%; font-size: 25px;">
                      <tr>
                          <td colspan="2">------------------------</td>
                      </tr>
                      <tr>
                          <td style="width: 50%;">Subtotal</td>
                          <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataTrans.totalPrice, 0, ",", ".")}</td>
                      </tr>
                     ${discount}
                      <tr>
                          <td colspan="2">------------------------</td>
                      </tr>
                      <tr>
                          <td style="width: 50%; font-weight: bold;">Total</td>
                          <td style="width: 50%; font-weight: bold; text-align: right;">
                          ${helpers.numberFormat(_self.dataTrans.totalPayable, 0, ",", ".")}
                          </td>
                      </tr>
                      <tr>
                          <td colspan="2">------------------------</td>
                      </tr>
                      <tr>
                          <td style="width: 50%;">${helpers.capitalizeFirstLetter(_self.dataTrans.paymentMethod)}</td>
                          <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataTrans.totalPaid, 0, ",", ".")}</td>
                      </tr>
                      
                      ${change}

                  </table>
                  <table style="width: 90%; font-size: 20px; text-align: center;">
                      <tr>
                          <td>${_self.footMark}</td>
                      </tr>
                  </table>
              </div>
          </body>
      </html>
      `;

    if (_self.debug) {
      console.log(html);
    } else {
      passprnt_uri += "html=" + encodeURIComponent(html);
      window.location.href = passprnt_uri;
    }
  },

  mini58PassPRNTStarMPopIpadClosing() {
    let passprnt_uri = "starpassprnt://v1/print/nopreview?";

    let _self = this;

    let header = '';

    if (_self.dataClosing.companyName) {
      header += `
        <tr>
            <td>${_self.dataClosing.companyName}</td>
        </tr>
      `;
    }

    //company address
    if (_self.dataClosing.companyAddress) {
      header += `
        <tr>
           <td>${_self.dataClosing.companyAddress}</td>
        </tr>
      `;
    }

    //company phone
    if (_self.dataClosing.companyPhone) {
      header += `
        <tr>
            <td>${_self.dataClosing.companyPhone}</td>
        </tr>
      `;
    }


    let detail = '';
    if (_self.dataClosing.details.length > 0) {
      detail += `
        <table style="width: 90%; font-size: 25px;">
            <tr>
                <td>------------------------</td>
            </tr>
        </table>
        <table style="width: 90%; font-size: 25px; font-weight: bold; text-align: center;">
            <tr>
                <td>*DETAIL TRANSAKSI*</td>
            </tr>
        </table>
      `;

      let totalGrandTotal = 0;

      _self.dataClosing.details.forEach(function (item, index) {
        totalGrandTotal += parseInt(item.grandTotal);
        detail += `
          <table style="width: 90%; font-size: 25px;">
              <tr>
                  <td style="width: 50%;">${item.transactionNumber}</td>
                  <td style="width: 50%; text-align: right;">${item.paymentMethod.toUpperCase()}</td>
              </tr>
              <tr>
                  <td style="width: 50%;">${dayjs(item.date).format('HH:mm')}</td>
                  <td style="width: 50%; text-align: right;">${helpers.numberFormat(item.grandTotal, 0, ",", ".")}</td>
              </tr>
          </table>
          `;
      });

      detail += `
          <table style="width: 90%; font-size: 25px;">
              <tr>
                  <td style="width: 50%;">TOTAL</td>
                  <td style="width: 50%; text-align: right;">${helpers.numberFormat(totalGrandTotal, 0, ",", ".")}</td>
              </tr>
          </table>
          <table style="width: 90%; font-size: 25px;">
            <tr>
                <td>------------------------</td>
            </tr>
        </table>
        `;
    }


    // passprnt_uri += "back=#";
    // passprnt_uri += "&popup=disable";

    let html = `
        <html lang="en">
          <head>
              <meta charset="UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <meta http-equiv="X-UA-Compatible" content="ie=edge">
              <title>Print</title>
          </head>
          <body style="font-size: 25px; font-family: 'Inconsolata', monospace;">
              <div style="width: 110mm; max-width: 110mm;">
                  <table style="width: 90%; font-size: 25px; text-align: center;">
                     ${header}
                  </table>
                  <table style="width: 90%; font-size: 25px;">
                      <tr>
                          <td>Tanggal</td>
                          <td>:</td>
                          <td>
                            ${dayjs(_self.dataClosing.date).format("DD MMM YYYY HH:mm")}
                          </td>
                      </tr>
                      <tr>
                          <td>Kasir</td>
                          <td>:</td>
                          <td>${_self.dataClosing.admin}</td>
                      </tr>
                  </table>
                  <table style="width: 90%; font-size: 25px;">
                      <tr>
                          <td>------------------------</td>
                      </tr>
                  </table>
                  <table style="width: 90%; font-size: 25px; font-weight: bold; text-align: center;">
                      <tr>
                          <td>*TUTUP KASIR*</td>
                      </tr>
                  </table>
                  <table style="width: 90%; font-size: 25px;">
                    <tr>
                        <td style="width: 50%;">Kas Masuk</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.cashIn, 0, ",", ".")}</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">Kas Keluar</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.cashOut, 0, ",", ".")}</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">TOTAL KAS</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.totalCash, 0, ",", ".")}</td>
                    </tr>
                  </table>
                  <table style="width: 90%; font-size: 25px;">
                    <tr>
                        <td style="width: 50%;">Penjualan Tunai</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.salesCash, 0, ",", ".")}</td>
                    </tr>
                  </table>
                  <table style="width: 90%; font-size: 25px;">
                    <tr>
                        <td style="width: 50%;">Penjualan EDC</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.salesEdc, 0, ",", ".")}</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">Penjualan Transfer</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.salesTransfer, 0, ",", ".")}</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">Penjualan OVO</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.salesOvo, 0, ",", ".")}</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">Penjualan GOPAY</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.salesGopay, 0, ",", ".")}</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">TOTAL NON TUNAI</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.totalNonCash, 0, ",", ".")}</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">TOTAL PENJUALAN</td>
                        <td style="width: 50%; text-align: right;">${helpers.numberFormat(_self.dataClosing.totalSales, 0, ",", ".")}</td>
                    </tr>
                  </table>
                  ${detail}
                  <table style="width: 90%; font-size: 20px; text-align: center;">
                      <tr>
                          <td>${_self.footMark}</td>
                      </tr>
                  </table>
              </div>
          </body>
      </html>
      `;

    if (_self.debug) {
      console.log(html);
    } else {
      passprnt_uri += "html=" + encodeURIComponent(html);
      window.location.href = passprnt_uri;
    }
  },

  centerText32(str, enter) {
    const _self = this;
    const max = 32;

    let arrStr = str.split(" ");
    let newStr = ``;

    let tempStr = ``;
    arrStr.forEach(word => {
      let tryWord = tempStr + " " + word;
      if (tryWord.length > 32) {
        newStr += _self.centerText(tempStr, 32) + enter;
        tempStr = word;
      } else {
        tempStr = tryWord;
      }
    });

    if (tempStr != ``) {
      newStr += _self.centerText(tempStr, 32) + enter;
    }

    return newStr;
  },

  centerSpace32(str1, str2) {
    const max = 32;
    const total = str1.length + str2.length;
    const diff = max - total;
    const space = " ";

    return str1 + space.repeat(diff) + str2;
  },

  centerText(str, max) {
    const diff = max - str.length;
    const space = " ";

    if (diff > 1) {
      const spaceRight = Math.ceil(diff / 2);
      const spaceLeft = diff - spaceRight;

      return space.repeat(spaceLeft) + str + space.repeat(spaceRight);
    } else {
      return str + space.repeat(diff);
    }
  }
}

const plugin = {
  install() {
    Vue.print = print
    Vue.prototype.$print = print
  }
}

Vue.use(plugin)

export default print