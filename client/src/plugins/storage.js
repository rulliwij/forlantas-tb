import Vue from 'vue'
import helpers from '@/plugins/helpers'

const storage = {
  set(key, value) {
    if (typeof value === "object") {
      value = JSON.stringify(value);
    }
    localStorage.setItem(key, value);
  },

  get(key, objKey = "") {
    let storageItem = localStorage.getItem(key);

    if (storageItem === null) {
      return "";
    }

    if (objKey == "") {
      if (helpers.isJson(storageItem) !== false) {
        return JSON.parse(storageItem);
      } else {
        return storageItem;
      }
    }

    if (helpers.isJson(storageItem) !== false) {
      storageItem = JSON.parse(storageItem);
      if (typeof storageItem[objKey] !== "undefined") {
        return storageItem[objKey];
      } else {
        return "";
      }
    }
  },

  update(key, objKey, value = "") {
    let storageItem = localStorage.getItem(key);

    if (helpers.isJson(storageItem) !== false) {
      storageItem = JSON.parse(storageItem);
      storageItem[objKey] = value;
    }

    if (typeof storageItem === "object") {
      storageItem = JSON.stringify(storageItem);
    }

    localStorage.setItem(key, storageItem);
  },

  remove(key) {
    localStorage.removeItem(key);
  }
}

const plugin = {
  install() {
    Vue.storage = storage
    Vue.prototype.$storage = storage
  }
}

Vue.use(plugin)

export default storage