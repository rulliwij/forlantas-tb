import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import store from "@/store";
import storage from "@/plugins/storage";
import qs from "qs";

let token = storage.get("token");

axios.defaults.paramsSerializer = function (params) {
  return qs.stringify(params);
};

axios.defaults.baseURL = process.env.VUE_APP_GATEWAY_URL + '/api/'
axios.defaults.headers.common["Content-Type"] =
  "application/json;charset=utf-8";
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.timeout = 10 * 1000;

// Add a request interceptor
axios.interceptors.request.use(
  function (config) {
    // Do something before request is sent

    // If token is set, add token to header
    let headers = config.headers;
    if (token)
      config.headers = Object.assign({}, headers, {
        Authorization: 'Bearer ' + token,
      });

    // Add timestap to every request for disable cache
    let param = config.params;
    config.params = Object.assign({}, param, {
      timestamp: new Date().getTime(),
    });

    // Add form data to handle post
    let data = config.data;
    if (data) {
      let formData = new FormData();
      Object.keys(data).forEach((key) => {
        let value = data[key] === null ? "" : data[key];
        formData.append(key, value);
      });
      config.data = formData;
    }

    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
axios.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (error.response) {
      if (error.response.status == 401) {
        // If request 401 (unauthorized)
        store.commit("SET", {
          key: "token",
          value: "",
        });
        storage.remove("token");

        store.commit("SET", {
          key: "userData",
          value: {},
        });
        storage.remove("userData");

        store.commit("SET", {
          key: "isLogedIn",
          value: false,
        });

        store.commit("SET", {
          key: "expiredSession",
          value: true,
        });

        store.commit("SET", {
          key: "expiredMessage",
          value: error.response.data.msg,
        });

        return;
      } else if (error.response.status == 404) {
        // If request 404 (notfound)
        store.commit("SET", {
          key: "networkNotFound",
          value: true,
        });
        return;
      } else {
        // Handle if status outside 401, and 404
        store.commit("SET", {
          key: "networkError",
          value: true,
        });
        return;
      }
    } else {
      store.commit("SET", {
        key: "networkError",
        value: true,
      });
      return;
    }
  }
);

Vue.use(VueAxios, axios);

export default axios;