import Vue from 'vue'

const helpers = {
  capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
  },

  isJson(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  },

  objectJoin(obj, setJoin = ', ') {
    let arr = Object.keys(obj).map(item => obj[item]);
    let string = arr.join(`${setJoin}`);
    return string
  },

  objectIsEmpty(obj) {
    if (Object.keys(obj).length > 0) {
      return false
    }
    return true
  },

  cutStr(str, length) {
    if (str.length > length) {
      return str.substring(0, length) + "...";
    }

    return str;
  },

  aliasName(name) {
    let firstAlias = "";
    let secondAlias = "";
    if (name) {
      let splitname = name.split(" ");

      if (splitname[0]) {
        firstAlias = splitname[0].substring(0, 1);
      }

      if (splitname.length > 1) {
        if (splitname[splitname.length - 1]) {
          secondAlias = splitname[1].substring(0, 1);
        }
      } else {
        secondAlias = splitname[0].substring(1, 1);
      }
    }
    return firstAlias + secondAlias;
  },

  numberFormat(number = 0, decimals = 0, decPoint = ',', thousandsSep = '.') {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    let n = !isFinite(+number) ? 0 : +number;
    let prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
    let sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
    let dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
    let s = '';

    let toFixedFix = function (n, prec) {
      let k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k).toFixed(prec);
    }

    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
  },

  removeHtmlTag(strHtml) {
    return strHtml.replace(/<\/?[^>]+(>|$)/g, "");
  }
}

const plugin = {
  install() {
    Vue.helpers = helpers
    Vue.prototype.$helpers = helpers
  }
}

Vue.use(plugin)

export default helpers