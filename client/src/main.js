import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "@/plugins/axios"
import vuetify from './plugins/vuetify'
import dayjs from './plugins/dayjs'
import VCurrencyField from 'v-currency-field'
import VueApexCharts from './plugins/vueApexChart'
import { VTextField } from 'vuetify/lib'

import 'dayjs/locale/id'
import './registerServiceWorker'
import './plugins/storage'
import './plugins/helpers'
import './mixins/commonMixin'

Vue.config.productionTip = false

Vue.component('v-text-field', VTextField)
Vue.use(VCurrencyField, { 
	locale: 'id',
	decimalLength: 0,
	autoDecimalMode: true,
	min: null,
  max: null,
	defaultValue: 0,
    valueAsInteger: false,
    allowNegative: true
})

new Vue({
  router,
  store,
  axios,
  vuetify,
  VueApexCharts,
  render: h => h(App)
}).$mount('#app')