<?php

namespace app\Config;

class RestApi {
    
    public $validLoginUsername = "";
    public $validLoginPassword = "";
    
    public $ipWhitelistEnabled = false;
    public $ipWhitelist = array();

}
