<?php 
namespace Config;

/**
 * 
 */
class AuthConfig
{
	
	public $baseAuth = "basic";

	public $restValidLogins = ["supernext" => "admin123456"];

	public $rest_ip_whitelist = FALSE;

	public $rest_realm = 'REST API';

}