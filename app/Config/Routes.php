<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('App');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

//===== Route API =====

// Handle CORS
$routes->options('(:any)', 'OptionsController::index'); //one options method for all routes.

// Login
$routes->post('api/login', 'Api\Login::verify');

// Dashboard
$routes->get('api/dashboard/list', 'Api\Dashboard::list', ['filter' => 'authApi']);
$routes->get('api/dashboard/notif-stock', 'Api\Dashboard::notif_stock', ['filter' => 'authApi']);

// User
$routes->get('api/user/list', 'Api\User::list', ['filter' => 'authApi']);
$routes->post('api/user/create', 'Api\User::create', ['filter' => 'authApi']);
$routes->post('api/user/update', 'Api\User::update', ['filter' => 'authApi']);
$routes->post('api/user/delete', 'Api\User::delete', ['filter' => 'authApi']);

// User Group
$routes->get('api/user-group/list', 'Api\User_group::list', ['filter' => 'authApi']);

// Store
$routes->get('api/store/list', 'Api\Store::list', ['filter' => 'authApi']);

// CDN
$routes->post('api/cdn-upload','Api\CdnController::upload_image');

// admin

// Profile
$routes->post('api/profile/edit', 'Api\Profile::edit', ['filter' => 'authApi']);

// Profile Password
$routes->post('api/profile/edit_password', 'Api\Profile::edit_password', ['filter' => 'authApi']);

// Product
$routes->get('api/product/list', 'Api\Product::list', ['filter' => 'authApi']);

// Product Create
$routes->post('api/product/create', 'Api\Product::create', ['filter' => 'authApi']);

// Product Update
$routes->post('api/product/update', 'Api\Product::update', ['filter' => 'authApi']);

// Product Delete
$routes->post('api/product/delete', 'Api\Product::delete', ['filter' => 'authApi']);

// Product Options
$routes->get('api/product/options', 'Api\Product::options', ['filter' => 'authApi']);

// Category List
$routes->get('api/category/list', 'Api\Category::list', ['filter' => 'authApi']);

// Category Create
$routes->post('api/category/create', 'Api\Category::create', ['filter' => 'authApi']);

// Category update
$routes->post('api/category/update', 'Api\Category::update', ['filter' => 'authApi']);

// Category delete
$routes->post('api/category/delete', 'Api\Category::delete', ['filter' => 'authApi']);

// Category Options
$routes->get('api/category/options', 'Api\Category::options', ['filter' => 'authApi']);

// Cashier
$routes->get('api/cashier/list', 'Api\Cashier::list', ['filter' => 'authApi']);

// Cashier In
$routes->post('api/cashier/in', 'Api\Cashier::in', ['filter' => 'authApi']);

// Cashier Out
$routes->post('api/cashier/out', 'Api\Cashier::out', ['filter' => 'authApi']);

// Sales Order
$routes->get('api/sales_order/list', 'Api\Sales_order::list', ['filter' => 'authApi']);
$routes->post('api/sales_order/refund', 'Api\Sales_order::refund', ['filter' => 'authApi']);
$routes->post('api/sales_order/delete', 'Api\Sales_order::delete', ['filter' => 'authApi']);

// Sales Order Daily
$routes->get('api/sales_order/list_daily', 'Api\Sales_order::list_daily', ['filter' => 'authApi']);

// Sales Order Monthly
$routes->get('api/sales_order/list_monthly', 'Api\Sales_order::list_monthly', ['filter' => 'authApi']);

// Stock
$routes->get('api/stock/list', 'Api\Stock::list', ['filter' => 'authApi']);

// Stock Log
$routes->get('api/stock/list_log', 'Api\Stock::list_log', ['filter' => 'authApi']);

// Laba Rugi Pembelian
$routes->get('api/stock/list-laba-rugi', 'Api\Stock::list_laba_rugi', ['filter' => 'authApi']);

// Stock Adjustment
$routes->get('api/stock_adjustment/list', 'Api\Stock_adjustment::list', ['filter' => 'authApi']);

// Stock Adjustment get Data product
$routes->get('api/stock_adjustment/product_options', 'Api\Stock_adjustment::product_options', ['filter' => 'authApi']);

// Stock Adjustment get Data product unit
$routes->get('api/stock_adjustment/product_unit', 'Api\Stock_adjustment::product_unit', ['filter' => 'authApi']);

// Stock Adjustment create
$routes->post('api/stock_adjustment/create', 'Api\Stock_adjustment::create', ['filter' => 'authApi']);

// Customer
$routes->get('api/customer/list', 'Api\Customer::list', ['filter' => 'authApi']);

// Customer
$routes->post('api/customer/create', 'Api\Customer::create', ['filter' => 'authApi']);

// Customer
$routes->post('api/customer/update', 'Api\Customer::update', ['filter' => 'authApi']);

// Close Register
$routes->get('api/close-register/list', 'Api\Close_register::close_register_report', ['filter' => 'authApi']);
$routes->post('api/close-register/end-shift', 'Api\Close_register::end_shift', ['filter' => 'authApi']);

// Shift
$routes->get('api/shift/list', 'Api\Shift::shift_list', ['filter' => 'authApi']);

// Profit
$routes->get('api/profit/summary', 'Api\Report_profit::summary', ['filter' => 'authApi']);

// POS
$routes->get('api/pos/list/customer', 'Api\Pos::customer_list', ['filter' => 'authApi']);
$routes->get('api/pos/list/discount', 'Api\Pos::discount_list', ['filter' => 'authApi']);
$routes->get('api/pos/list/sales-type', 'Api\Pos::sales_type_list', ['filter' => 'authApi']);
$routes->get('api/pos/list/category', 'Api\Pos::category_list', ['filter' => 'authApi']);
$routes->get('api/pos/list/product', 'Api\Pos::product_list', ['filter' => 'authApi']);
$routes->get('api/pos/list/save', 'Api\Pos::save_list', ['filter' => 'authApi']);
$routes->get('api/pos/list/complete', 'Api\Pos::complete_list', ['filter' => 'authApi']);
$routes->get('api/pos/list/close', 'Api\Pos::close_list', ['filter' => 'authApi']);
$routes->get('api/pos/check', 'Api\Pos::check', ['filter' => 'authApi']);
$routes->get('api/pos/save/total', 'Api\Pos::total_save', ['filter' => 'authApi']);
$routes->post('api/pos/save', 'Api\Pos::save', ['filter' => 'authApi']);
$routes->post('api/pos/pay', 'Api\Pos::pay', ['filter' => 'authApi']);
$routes->post('api/pos/refund', 'Api\Pos::refund', ['filter' => 'authApi']);
$routes->post('api/pos/close', 'Api\Pos::close', ['filter' => 'authApi']);

// SALES TYPE
$routes->get('api/sales-type/list', 'Api\Sales_type::list', ['filter' => 'authApi']);
$routes->post('api/sales-type/create', 'Api\Sales_type::create', ['filter' => 'authApi']);
$routes->post('api/sales-type/update', 'Api\Sales_type::update', ['filter' => 'authApi']);
$routes->post('api/sales-type/delete', 'Api\Sales_type::delete', ['filter' => 'authApi']);

// DISCOUNT
$routes->get('api/discount/list', 'Api\Discount::list', ['filter' => 'authApi']);
$routes->post('api/discount/create', 'Api\Discount::create', ['filter' => 'authApi']);
$routes->post('api/discount/update', 'Api\Discount::update', ['filter' => 'authApi']);
$routes->post('api/discount/delete', 'Api\Discount::delete', ['filter' => 'authApi']);

// RECIPE
$routes->get('api/recipe/list', 'Api\Recipe::list', ['filter' => 'authApi']);
$routes->get('api/recipe/detail', 'Api\Recipe::detail', ['filter' => 'authApi']);
$routes->get('api/recipe/product-list', 'Api\Recipe::product_list', ['filter' => 'authApi']);
$routes->get('api/recipe/product-recipe-list', 'Api\Recipe::product_recipe_list', ['filter' => 'authApi']);
$routes->post('api/recipe/create', 'Api\Recipe::create', ['filter' => 'authApi']);
$routes->post('api/recipe/update', 'Api\Recipe::update', ['filter' => 'authApi']);
$routes->post('api/recipe/delete', 'Api\Recipe::delete', ['filter' => 'authApi']);

// REPORT
$routes->get('api/report/list', 'Api\Report_sales_order::list', ['filter' => 'authApi']);

// PIN
$routes->post('api/pin/create', 'Api\Pin::create', ['filter' => 'authApi']);
$routes->post('api/pin/check', 'Api\Pin::check_validation', ['filter' => 'authApi']);
//===== Route Client VueJs =====

//PO
$routes->get('api/po/list', 'Api\Po::list', ['filter' => 'authApi']);
$routes->post('api/po/create', 'Api\Po::create', ['filter' => 'authApi']);

//Supplier
$routes->get('api/supplier/options', 'Api\Supplier::options', ['filter' => 'authApi']);

//EXCEL
$routes->post('api/excel/import', 'Api\Excel::import', ['filter' => 'authApi']);

// $routes->get('api/excel/export', 'Api\Excel::export', ['filter' => 'authApi']);

$routes->get('api/excel/export-laba-rugi-pembelian', 'Api\Excel::export_laba_rugi_pembelian', ['filter' => 'authApi']);
$routes->get('api/excel/export-report-penjualan', 'Api\Excel::export_report_penjualan', ['filter' => 'authApi']);
$routes->get('api/excel/export-report-profit-wiwit', 'Api\Excel::export_report_profit_wiwit', ['filter' => 'authApi']);
$routes->get('api/excel/export-report-profit-andi', 'Api\Excel::export_report_profit_andi', ['filter' => 'authApi']);

$routes->add('(:any)', 'App::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
