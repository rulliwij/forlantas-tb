<?php 
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Config\AuthConfig;
use CodeIgniter\RESTful\ResourceController;

class TokenFilter implements FilterInterface
{
	 public function before(RequestInterface $request, $arguments = null)
    {
    	$server = $request->getServer();
        if (isset($server['HTTP_TOKEN'])) {
        	if (!empty($server['HTTP_TOKEN'])) {
        		$checkToken = $this->checkToken($server['HTTP_TOKEN']);
        		if (!$checkToken) {
        			$this->sendRespond(ResponseInterface::HTTP_UNAUTHORIZED);
        		}
        	} else {
        		$this->sendRespond(ResponseInterface::HTTP_UNAUTHORIZED);
        	}
        } else {
        	$this->sendRespond(ResponseInterface::HTTP_UNAUTHORIZED);
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }

    public function checkToken($token)
    {
    	$now = date('Y-m-d H:i:s');
    	$db = \Config\Database::connect();
    	$sql = "
    		SELECT 
    			user_auth_user_id AS user_id,
                user_auth_token AS token,
                user_auth_device_id AS device_id,
                user_auth_created AS created,
                user_auth_expired AS expired,
                user_auth_name AS name,
                user_auth_fullname AS fullname,
                user_auth_mobilephone AS mobilephone,
                user_auth_image AS image,
                user_auth_fcm_token AS fcm_token
            FROM user_auth
            WHERE user_auth_token = '$token'
    	";
    	$data = $db->query($sql)->getRow();
    	if (!empty($data)) {
			if ($now > $data->expired) {
    			return FALSE;
    		} else {
    			return TRUE;
    		}
    	} else {
    		return FALSE;
    	}
    }

    public function sendRespond($code)
    {
    	if ($code == 401) {
    		$msg = 'Unauthorized';
    	} else if ($code == 402) {
    		$msg = 'Payment Required';
    	} else if ($code == 403) {
    		$msg = 'Forbidden';
    	} else if ($code == 404) {
    		$msg = 'Not Found';
    	} else if ($code == 405) {
    		$msg = 'Method Not Allowed';
    	} else if ($code == 406) {
    		$msg = 'Not Acceptable';
    	}

    	header("Content-Type: application/json");
    	header("HTTP/1.1 ". $code ." " .$msg);

    	echo json_encode(array(
    		'status' => $code,
    		'msg' => $msg
    	));

    	exit;
    }
}