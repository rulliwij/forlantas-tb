<?php 
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Config\AuthConfig;
use CodeIgniter\RESTful\ResourceController;

class AuthFilter implements FilterInterface
{

    public function before(RequestInterface $request, $arguments = null)
    {
        $db = \Config\Database::connect();
        $server = $request->getServer();

        if (isset($server['HTTP_AUTHORIZATION'])) {
            if (!empty($server['HTTP_AUTHORIZATION'])) {
                $checkBasicAuth = $this->checkBasicAuth($server);
                if ($checkBasicAuth !== TRUE) {
                    $this->sendRespond(ResponseInterface::HTTP_UNAUTHORIZED);
                }
            } else {
                $this->sendRespond(ResponseInterface::HTTP_FORBIDDEN);
            }
        } else {
            $this->sendRespond(ResponseInterface::HTTP_FORBIDDEN);
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }

    public function checkBasicAuth($server)
    {
    	$config = new AuthConfig();

    	$username = $server['PHP_AUTH_USER'] ?? null;
        $http_auth = $server['HTTP_AUTHORIZATION'] ?? null;

        $password = NULL;
        if ($username !== NULL) {
            $password = $server['PHP_AUTH_PW'] ?? null;
        } else if ($http_auth !== NULL) {
            if (strpos(strtolower($http_auth), 'basic') === 0) {
                list($username, $password) = explode(':', base64_decode(substr($server['HTTP_AUTHORIZATION'], 6)));
            }
        }

        if (empty($username) || empty($password)) {
            return FALSE;
        } else {
        	if (array_key_exists($username, $config->restValidLogins) === FALSE) {
        		return FALSE;
        	}

        	if ($config->restValidLogins[$username] !== $password) {
        		return FALSE;
        	}
        	return TRUE;
        }
    }

    public function sendRespond($code)
    {
    	if ($code == 401) {
    		$msg = 'Unauthorized';
    	} else if ($code == 402) {
    		$msg = 'Payment Required';
    	} else if ($code == 403) {
    		$msg = 'Forbidden';
    	} else if ($code == 404) {
    		$msg = 'Not Found';
    	} else if ($code == 405) {
    		$msg = 'Method Not Allowed';
    	} else if ($code == 406) {
    		$msg = 'Not Acceptable';
    	}

    	header("Content-Type: application/json");
    	header("HTTP/1.1 ". $code ." " .$msg);

    	echo json_encode(array(
    		'status' => $code,
    		'msg' => $msg
    	));

    	exit;
    }
}