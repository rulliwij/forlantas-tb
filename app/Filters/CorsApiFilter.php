<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class CorsApiFilter implements FilterInterface {

    public function before(RequestInterface $request, $arguments = null) {
      if($_ENV['CI_ENVIRONMENT'] == 'development'){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
      }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null) {
      //todo
    }

}
