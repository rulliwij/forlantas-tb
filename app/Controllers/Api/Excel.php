<?php 
namespace App\Controllers\Api;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Excel extends \App\Controllers\ApiAuthUserController {
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }
    
	public function import() {
        $file = $this->request->getFile('file');
		$file_mimes = array(
			'text/x-comma-separated-values', 
			'text/comma-separated-values', 
			'application/octet-stream', 
			'application/vnd.ms-excel', 
			'application/x-csv', 
			'text/x-csv', 
			'text/csv', 
			'application/csv', 
			'application/excel', 
			'application/vnd.msexcel', 
			'text/plain', 
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		if(isset($_FILES['file']['name']) && in_array($_FILES['file']['type'], $file_mimes)) {
			$arr_file = explode('.', $_FILES['file']['name']);
			$extension = end($arr_file);
			if('csv' == $extension) {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			}
		 
			$spreadsheet = $reader->load($_FILES['file']['tmp_name']);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();
			for($i = 1;$i < count($sheetData);$i++) {
				$product_category_name = $sheetData[$i]['1'];
				$product_name = $sheetData[$i]['2'];
				$product_desc = $sheetData[$i]['3'];
				$product_price = $sheetData[$i]['4'];
				$product_purchase_price = $sheetData[$i]['5'];
				$product_unit = $sheetData[$i]['6'];
				$product_discount_percent = $sheetData[$i]['7'];
				$product_discount_value = $sheetData[$i]['8'];	
				$product_is_stock = $sheetData[$i]['9'];
				$total_stock = $sheetData[$i]['10'];
				$minimum_stock = $sheetData[$i]['11'];
				$product_weight = $sheetData[$i]['12'];
				$product_type = $sheetData[$i]['13'];
				$product_is_on_sale = $sheetData[$i]['14'];
				$product_input_user_name = $sheetData[$i]['15'];

				$get_category = $this->getCategory($product_category_name);
				$add_json = array(
					array(
						'name' => '',
						'price' => '0'
					)
				);
                $data_insert = [];
                $data_insert['product_category_id'] = $get_category->category_id ? $get_category->category_id : 0;
				$data_insert['product_category_name'] = $product_category_name;
				$data_insert['product_code'] = $this->generateCode('product', 'product_code');
				$data_insert['product_name'] = $product_name;
				$data_insert['product_desc'] = $product_desc;
				$data_insert['product_price'] = $product_price;
				$data_insert['product_sales_type_price_json'] = null;
				$data_insert['product_sales_type'] = 1;
				$data_insert['product_addons_json'] = json_encode($add_json);
				$data_insert['product_purchase_price'] = $product_purchase_price;
				$data_insert['product_unit'] = $product_unit;
				$data_insert['product_discount_percent'] = $product_discount_percent;
				$data_insert['product_discount_value'] = $product_discount_value;
				$data_insert['product_weight'] = $product_weight;
				$data_insert['product_type'] = $product_type;
				$data_insert['product_is_on_sale'] = $product_is_on_sale == 'ya'? 1 : 0;
				$data_insert['product_is_stock'] = $product_is_stock == 'ya'? 1 : 0;
				$data_insert['product_input_user_name'] = $product_input_user_name;
				$data_insert['product_input_datetime'] = date('Y-m-d h:i:s');
				if (!$this->db->table('product')->insert($data_insert)) {
                    $this->respondFailed("Gagal import data");
				}
				$product_id = $this->db->insertID();
				if ($data_insert['product_is_stock'] == 1) {
					$data_insert_stock = [];
					$data_insert_stock['stock_product_id'] = $product_id;
					$data_insert_stock['stock_product_store_id'] = 1;
					$data_insert_stock['stock_balance'] = $total_stock;
					$data_insert_stock['stock_minimal'] = $minimum_stock;
					if (!$this->db->table('stock')->insert($data_insert_stock)) {
						$this->respondFailed("Gagal import data");
					}
				}
			}
			$this->respondSuccess("Berhasil menambahkan import excel.");
		}
	}

	private function getCategory($category){
		$sql = "SELECT * 
			FROM category
			WHERE category_name LIKE '%$category%'
		";
		
		$queryResult = $this->db->query($sql);
		$dataResult = (Object) array();
		if ($queryResult->resultID->num_rows > 0) {
			$result = $queryResult->getResult();
            foreach ($result as $row) {
				$dataResult = nullToString($row);
			}
		}
		return $dataResult;
    }
    
    private function generateCode($table_name = '', $fieldname = '', $extra = '', $digit = 5) {
        $sql = "
            SELECT
            IFNULL(LPAD(MAX(CAST(RIGHT(" . $fieldname . ", " . $digit . ") AS SIGNED) + 1), " . $digit . ", '0'), '" . sprintf('%0' . $digit . 'd', 1) . "') AS code 
            FROM " . $table_name . "
            " . $extra . "
          ";
        $query = $this->db->query($sql);
        if ($query->resultID->num_rows > 0) {
            $row = $query->getRow();
            return $row->code;
        } else {
            return '';
        }
    }

	public function export_laba_rugi_pembelian(){
        $table = "stock";
        $defaultSort = "stock_id";
        $defaultDir = "DESC";
        // print_r(base_url());exit;
        $arrField = array(
            'stock_id',
            'stock_product_id',
            'stock_product_store_id',
            'stock_balance',
            'product_code',
            'product_name',
            'stock_log_input_datetime',
            'stok_sekarang',
            'beli',
            'jual',
            'laba',
        );

        $where = "product_is_deleted = 0 AND stock_product_store_id = '{$this->user->user_auth_user_store_id}' AND stock_log_store_id = '{$this->user->user_auth_user_store_id}'";
        $join = "
            JOIN product ON stock_product_id = product_id
            JOIN stock_log ON stock_log_product_id = product_id
        ";

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (($keyDelete = array_search('stok_sekarang', $arrField)) !== false) {
            unset($arrField[$keyDelete]);
        }

        if (($keyDelete = array_search('beli', $arrField)) !== false) {
            unset($arrField[$keyDelete]);
        }

        if (($keyDelete = array_search('jual', $arrField)) !== false) {
            unset($arrField[$keyDelete]);
        }

        if (($keyDelete = array_search('laba', $arrField)) !== false) {
            unset($arrField[$keyDelete]);
        }

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            (SUM(CASE WHEN stock_log_movement_type = 1 THEN stock_log_qty ELSE 0 END) - SUM(CASE WHEN stock_log_movement_type = 2 THEN stock_log_qty ELSE 0 END)) AS stok_sekarang,
            (SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_product_purchase_price ELSE 0 END)) AS beli,
            (SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_product_sales_price ELSE 0 END)) AS jual,
            ((SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_product_sales_price ELSE 0 END)) - (SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_product_purchase_price ELSE 0 END))) AS laba,
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            GROUP BY stock_product_id
            ORDER BY {$sort} {$dir}
        ";
        
        $queryResult = $this->db->query($sql);
        
        $totalData = 0;
        $dataResult = array();
        
        if ($queryResult->resultID->num_rows > 0) {
            
            $sqlTotal = "SELECT FOUND_ROWS() AS row";
            
            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;
            
            $result = $queryResult->getResult();
            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }
        
        $sql = "
        SELECT SQL_CALC_FOUND_ROWS
            (SUM(CASE WHEN stock_log_movement_type = 1 THEN stock_log_qty ELSE 0 END) - SUM(CASE WHEN stock_log_movement_type = 2 THEN stock_log_qty ELSE 0 END)) AS stok_sekarang,
            (SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_product_purchase_price ELSE 0 END)) AS beli,
            (SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_product_sales_price ELSE 0 END)) AS jual,
            ((SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_product_sales_price ELSE 0 END)) - (SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_product_purchase_price ELSE 0 END))) AS laba
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
        ";
            
        $querySummary = $this->db->query($sql)->getRow();

        //Menyiapakan export data
        $first_column = $cell_column = 'A';
        $cell_row = $first_row = 1;
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet;
        $sheet->getProperties()->setCreator('Next-POS')
            ->setLastModifiedBy('Next Innovation')
            ->setTitle("Laporan Laba/Rugi Pembelian")
            ->setSubject("Laporan Laba/Rugi Pembelian")
            ->setDescription("")
            ->setKeywords("Laporan Laba/Rugi Pembelian");
            
        $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
        $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(12);
        $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, strtoupper("Laporan Laba/Rugi Pembelian"));
        $cell_row++;
        $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, 'Export Date : ' . date("Y-m-d H:i:s"));
        $cell_row++;
        $cell_row++;
        
        $sheet->setActiveSheetIndex(0)->setCellValue('A4', 'NO');
        $sheet->setActiveSheetIndex(0)->setCellValue('B4', 'KODE');
        $sheet->setActiveSheetIndex(0)->setCellValue('C4', 'NAMA');
        $sheet->setActiveSheetIndex(0)->setCellValue('D4', 'STOK');
        $sheet->setActiveSheetIndex(0)->setCellValue('E4', 'TOTAL HARGA BELI (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('F4', 'TOTAL HARGA JUAL (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('G4', 'TOTAL LABA/RUGI (Rp)');

        $sheet->getActiveSheet()->getStyle("A4:G4")->getAlignment()->setHorizontal('center');
        
        $sheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setSize(10);
        
        $sheet->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true);
        
        $sheet->getActiveSheet()->getStyle("A:G")->getFont()->setSize(10);
        
        $sheet->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
        $sheet->getActiveSheet()->getColumnDimension('B')->setWidth(10); 
        $sheet->getActiveSheet()->getColumnDimension('C')->setWidth(30); 
        $sheet->getActiveSheet()->getColumnDimension('D')->setWidth(10); 
        $sheet->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
        $sheet->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
        $sheet->getActiveSheet()->getColumnDimension('G')->setWidth(20); 
        
        $column = 5;
        $no = 1;
        foreach($dataResult as $data) {
            $sheet->setActiveSheetIndex(0)->setCellValue('A'.$column, $no++);
            $sheet->setActiveSheetIndex(0)->setCellValue('B'.$column, $data->product_code);
            $sheet->setActiveSheetIndex(0)->setCellValue('C'.$column, $data->product_name);
            $sheet->setActiveSheetIndex(0)->setCellValue('D'.$column, $data->stok_sekarang);
            $sheet->setActiveSheetIndex(0)->setCellValue('E'.$column, $data->beli);
            $sheet->setActiveSheetIndex(0)->setCellValue('F'.$column, $data->jual);
            $sheet->setActiveSheetIndex(0)->setCellValue('G'.$column, $data->laba);

            $sheet->getActiveSheet()->getStyle("A".$column)->getAlignment()->setHorizontal('center');
            $sheet->getActiveSheet()->getStyle("B".$column)->getAlignment()->setHorizontal('center');
            $sheet->getActiveSheet()->getStyle("C".$column)->getAlignment()->setHorizontal('left');
            $sheet->getActiveSheet()->getStyle("D".$column)->getAlignment()->setHorizontal('center');
            $sheet->getActiveSheet()->getStyle("E".$column)->getAlignment()->setHorizontal('right');
            $sheet->getActiveSheet()->getStyle("F".$column)->getAlignment()->setHorizontal('right');
            $sheet->getActiveSheet()->getStyle("G".$column)->getAlignment()->setHorizontal('right');
            
            $sheet->getActiveSheet()->getStyle('E'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
            $sheet->getActiveSheet()->getStyle('F'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
            // $sheet->getActiveSheet()->getStyle('G'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
            $sheet->getActiveSheet()->getStyle('G'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(@_)');

            $column++;
        }
        
        $sheet->getActiveSheet()->getStyle('C'.$column)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle('D'.$column)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle('E'.$column)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle('F'.$column)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle('G'.$column)->getFont()->setBold(true);

        $sheet->setActiveSheetIndex(0)->setCellValue('C'.$column, 'TOTAL');
        $sheet->setActiveSheetIndex(0)->setCellValue('D'.$column, $querySummary->stok_sekarang);
        $sheet->setActiveSheetIndex(0)->setCellValue('E'.$column, $querySummary->beli);
        $sheet->setActiveSheetIndex(0)->setCellValue('F'.$column, $querySummary->jual);
        $sheet->setActiveSheetIndex(0)->setCellValue('G'.$column, $querySummary->laba);

        $sheet->getActiveSheet()->getStyle('C'.$column)->getAlignment()->setHorizontal('right');
        $sheet->getActiveSheet()->getStyle('D'.$column)->getAlignment()->setHorizontal('center');
        $sheet->getActiveSheet()->getStyle('E'.$column)->getAlignment()->setHorizontal('right');
        $sheet->getActiveSheet()->getStyle('F'.$column)->getAlignment()->setHorizontal('right');
        $sheet->getActiveSheet()->getStyle('G'.$column)->getAlignment()->setHorizontal('right');

        $sheet->getActiveSheet()->getStyle('E'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
        $sheet->getActiveSheet()->getStyle('F'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
        $sheet->getActiveSheet()->getStyle('G'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(@_)');

        $sheet->getActiveSheet()->getStyle('C'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');
        $sheet->getActiveSheet()->getStyle('D'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');
        $sheet->getActiveSheet()->getStyle('E'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');
        $sheet->getActiveSheet()->getStyle('F'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');
        $sheet->getActiveSheet()->getStyle('G'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');

        // tulis dalam format .xlsx
        $writer = new Xlsx($spreadsheet);
        $fileName = "Laporan-Laba-Rugi-Pembelian-".date('dmYHis').'.xlsx';

        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName);
        header('Cache-Control: max-age=0');

        $writer->save($fileName);
        // $writer->save('./assets/excel/'.$fileName);
    
        $url = base_url().'/'.$fileName;
        // $url2 = site_url().'public/'.$fileName;
        // $url3 = base_url('public/'.$fileName);

        $this->respondSuccess("Berhasil Mengexport data", array('url_export' => $url));
    }

    public function export_report_penjualan(){
        $start_date = $this->request->getGet('start');
        $end_date = $this->request->getGet('end');
        $sort = (string) $this->request->getGet('sort');
        if(empty($sort)){
            $sort = 'datetime';
        }
        $dir = strtoupper($this->request->getGet('dir'));
        if ($dir != 'ASC' && $dir != 'DESC') {
            $dir = 'DESC';
        }
        
		if(empty($start_date) && empty($end_date)) {
			$start_date = date('Y-m-01');
			$end_date = date('Y-m-t');
        }
        
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS * FROM (
                SELECT sales_order_id AS id,
                sales_order_date AS date, 
                sales_order_input_datetime AS datetime,
                sales_order_total_product_price AS product_price,
                sales_order_total_discount_item_nominal AS nominal_discount,
                sales_order_total_nett_price AS nett_price,
                sales_order_grand_total AS grand_total
                FROM sales_order
                WHERE sales_order_date BETWEEN '".$start_date."' AND '".$end_date."' 
                AND sales_order_status = 'complete'
            ) result
            ORDER BY $sort $dir
        ";

        $data = $this->db->query($sql)->getResultArray();
        $arr_result = array();
        $arr_summary = array(
            'gross_sales' => 0,
            'discount_sales' => 0,
            'nett_sales' => 0,
        );

        $totalData = 0;
		if (!empty($data)) {
            $sqlTotal = "SELECT FOUND_ROWS() AS row";
            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;
            $sql_summary = "
                SELECT SUM(sales_order_total_product_price) AS gross_sales,
                SUM(sales_order_total_discount_item_nominal) AS discount_sales,
                SUM(sales_order_grand_total) AS nett_sales
                FROM sales_order
                WHERE sales_order_date BETWEEN '".$start_date."' AND '".$end_date."' 
                AND sales_order_status = 'complete'
            ";
            
            $data_summary = $this->db->query($sql_summary)->getRow();
            if (!empty($data_summary)) {
                $arr_summary = array(
                    'gross_sales' => $data_summary->gross_sales,
                    'discount_sales' => $data_summary->discount_sales,
                    'nett_sales' => $data_summary->nett_sales,
                );
            }
            foreach ($data as $key => $row) {
                // $sql_detail = "
                //     SELECT sales_order_detail_id AS detail_id,
                //     sales_order_detail_product_name AS name, sales_order_detail_product_qty AS qty,
                //     sales_order_detail_product_selling_price AS price,
                //     sales_order_detail_product_discount_nominal AS discount,
                //     sales_order_detail_product_subtotal_price AS nett_price
                //     FROM sales_order_detail
                //     WHERE sales_order_detail_sales_order_id = {$row['id']}
                // ";
                // $data_detail = $this->db->query($sql_detail)->getResultArray();
                // $row['detail'] = $data_detail;
                if (!isset($arr_result[$row['datetime']])) {
                    $arr_result['report'][] = $row;
                } else {
                    array_push($arr_result[$row['datetime']], $row);
                }
                // $arr_result['report'] = nullToString($row);
			}
        }

        //Menyiapakan export data
        $first_column = $cell_column = 'A';
        $cell_row = $first_row = 1;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet;
        $sheet->getProperties()->setCreator('Next-POS')
            ->setLastModifiedBy('Next Innovation')
            ->setTitle("Laporan Penjualan")
            ->setSubject("Laporan Penjualan")
            ->setDescription("")
            ->setKeywords("Laporan Penjualan");

        $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
        $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(12);
        $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, strtoupper("Laporan Penjualan"));
        $cell_row++;
        $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, 'Export Date : ' . date("Y-m-d H:i:s"));
        $cell_row++;
        $cell_row++;

        $sheet->setActiveSheetIndex(0)->setCellValue('A4', 'NO');
        $sheet->setActiveSheetIndex(0)->setCellValue('B4', 'TANGGAL');
        $sheet->setActiveSheetIndex(0)->setCellValue('C4', 'HARGA (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('D4', 'DISKON (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('E4', 'NETTO (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('F4', 'TOTAL HARGA (Rp)');

        $sheet->getActiveSheet()->getStyle("A4:F4")->getAlignment()->setHorizontal('center');

        $sheet->getActiveSheet()->getStyle("A4:F4")->getFont()->setSize(10);

        $sheet->getActiveSheet()->getStyle("A4:F4")->getFont()->setBold(true);

        $sheet->getActiveSheet()->getStyle("A:F")->getFont()->setSize(10);

        $sheet->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
        $sheet->getActiveSheet()->getColumnDimension('B')->setWidth(18); 
        $sheet->getActiveSheet()->getColumnDimension('C')->setWidth(20); 
        $sheet->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
        $sheet->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
        $sheet->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
        
        $column = 5;
        $no = 1;
        foreach($arr_result['report'] as $data) {
            $sheet->setActiveSheetIndex(0)->setCellValue('A'.$column, $no++);
            $sheet->setActiveSheetIndex(0)->setCellValue('B'.$column, $data['datetime']);
            $sheet->setActiveSheetIndex(0)->setCellValue('C'.$column, $data['product_price']);
            $sheet->setActiveSheetIndex(0)->setCellValue('D'.$column, $data['nominal_discount']);
            $sheet->setActiveSheetIndex(0)->setCellValue('E'.$column, $data['nett_price']);
            $sheet->setActiveSheetIndex(0)->setCellValue('F'.$column, $data['grand_total']);

            $sheet->getActiveSheet()->getStyle("A".$column)->getAlignment()->setHorizontal('center');
            $sheet->getActiveSheet()->getStyle("B".$column)->getAlignment()->setHorizontal('left');
            $sheet->getActiveSheet()->getStyle("C".$column)->getAlignment()->setHorizontal('right');
            $sheet->getActiveSheet()->getStyle("D".$column)->getAlignment()->setHorizontal('right');
            $sheet->getActiveSheet()->getStyle("E".$column)->getAlignment()->setHorizontal('right');
            $sheet->getActiveSheet()->getStyle("F".$column)->getAlignment()->setHorizontal('right');
            
            $sheet->getActiveSheet()->getStyle('C'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
            $sheet->getActiveSheet()->getStyle('D'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
            $sheet->getActiveSheet()->getStyle('E'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
            $sheet->getActiveSheet()->getStyle('F'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
            $column++;
        }

        $sheet->getActiveSheet()->getStyle('B'.$column)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle('C'.$column)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle('D'.$column)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle('E'.$column)->getFont()->setBold(true);

        $sheet->setActiveSheetIndex(0)->setCellValue('B'.$column, 'TOTAL');
        $sheet->setActiveSheetIndex(0)->setCellValue('C'.$column, $arr_summary['gross_sales']);
        $sheet->setActiveSheetIndex(0)->setCellValue('D'.$column, $arr_summary['discount_sales']);
        $sheet->setActiveSheetIndex(0)->setCellValue('E'.$column, $arr_summary['nett_sales']);

        $sheet->getActiveSheet()->getStyle('B'.$column)->getAlignment()->setHorizontal('right');
        $sheet->getActiveSheet()->getStyle('C'.$column)->getAlignment()->setHorizontal('right');
        $sheet->getActiveSheet()->getStyle('D'.$column)->getAlignment()->setHorizontal('right');
        $sheet->getActiveSheet()->getStyle('E'.$column)->getAlignment()->setHorizontal('right');

        $sheet->getActiveSheet()->getStyle('C'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
        $sheet->getActiveSheet()->getStyle('D'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
        $sheet->getActiveSheet()->getStyle('E'.$column)->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(@_)');

        $sheet->getActiveSheet()->getStyle('B'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');
        $sheet->getActiveSheet()->getStyle('C'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');
        $sheet->getActiveSheet()->getStyle('D'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');
        $sheet->getActiveSheet()->getStyle('E'.$column)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A9A9A9');

        // tulis dalam format .xlsx
        $writer = new Xlsx($spreadsheet);
        $fileName = "Laporan-Penjualan-".date('dmYHis').'.xlsx';

        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName);
        header('Cache-Control: max-age=0');

        $writer->save($fileName);
        // $writer->save('./assets/excel/'.$fileName);
        $url = base_url().'/'.$fileName;
        // $url2 = site_url().'public/'.$fileName;
        // $url3 = base_url('public/'.$fileName);

        $this->respondSuccess("Berhasil Mengexport data", array('url_export' => $url));
    }

    public function export_report_profit_wiwit(){
        $bahan = $this->request->getGet('bahan');
        $cash_in = $this->request->getGet('cash_in');
        $cash_out = $this->request->getGet('cash_out');
        $modal_in = $this->request->getGet('modal_in');
        $modal_out = $this->request->getGet('modal_out');
        $non_bahan = $this->request->getGet('non_bahan');
        $pendapatan = $this->request->getGet('pendapatan');
        $pengeluaran = $this->request->getGet('pengeluaran');
        $profit = $this->request->getGet('profit');

        //Menyiapakan export data
        $first_column = $cell_column = 'A';
        $cell_row = $first_row = 1;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet;
        $sheet->getProperties()->setCreator('Next-POS')
            ->setLastModifiedBy('Next Innovation')
            ->setTitle("Laporan Laba Rugi")
            ->setSubject("Laporan Laba Rugi")
            ->setDescription("")
            ->setKeywords("Laporan Laba Rugi");

        $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
        $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(12);
        $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, strtoupper("Laporan Laba Rugi"));
        $cell_row++;
        $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, 'Export Date : ' . date("Y-m-d H:i:s"));
        $cell_row++;
        $cell_row++;

        $sheet->setActiveSheetIndex(0)->setCellValue('A4', 'Modal Awal (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A5', 'Pendapatan (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A6', 'Total Pendapatan (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A7', 'Pengeluaran (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A8', 'Pengembalian Modal Awal (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A9', 'Total Pengeluaran (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A10', 'PROFIT (Rp)');

        $sheet->setActiveSheetIndex(0)->setCellValue('B4', $modal_in);
        $sheet->setActiveSheetIndex(0)->setCellValue('B5', $pendapatan);
        $sheet->setActiveSheetIndex(0)->setCellValue('B6', $cash_in);
        $sheet->setActiveSheetIndex(0)->setCellValue('B7', $pengeluaran);
        $sheet->setActiveSheetIndex(0)->setCellValue('B8', $modal_in);
        $sheet->setActiveSheetIndex(0)->setCellValue('B9', $cash_out);
        $sheet->setActiveSheetIndex(0)->setCellValue('B10', $profit);

        $sheet->getActiveSheet()->getStyle("A4:B10")->getFont()->setSize(10);

        $sheet->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("B6")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("A9")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("B9")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("A10")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("B10")->getFont()->setBold(true);

        $sheet->getActiveSheet()->getColumnDimension('A')->setWidth(25); 
        $sheet->getActiveSheet()->getColumnDimension('B')->setWidth(18); 
        
        $sheet->getActiveSheet()->getStyle('B4:B10')->getAlignment()->setHorizontal('right');
        $sheet->getActiveSheet()->getStyle('B4:B9')->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
        $sheet->getActiveSheet()->getStyle('B10')->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(@_)');

        // tulis dalam format .xlsx
        $writer = new Xlsx($spreadsheet);
        $fileName = "Laporan-Laba-Rugi-".date('dmYHis').'.xlsx';

        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName);
        header('Cache-Control: max-age=0');

        $writer->save($fileName);
        // $writer->save('./assets/excel/'.$fileName);
        $url = base_url().'/'.$fileName;
      
        // $url2 = site_url().'public/'.$fileName;
        // $url3 = base_url('public/'.$fileName);

        $this->respondSuccess("Berhasil Mengexport data", array('url_export' => $url));
    }

    public function export_report_profit_andi(){
        $bahan = $this->request->getGet('bahan');
        $cash_in = $this->request->getGet('cash_in');
        $cash_out = $this->request->getGet('cash_out');
        $modal_in = $this->request->getGet('modal_in');
        $modal_out = $this->request->getGet('modal_out');
        $non_bahan = $this->request->getGet('non_bahan');
        $pendapatan = $this->request->getGet('pendapatan');
        $pengeluaran = $this->request->getGet('pengeluaran');
        $profit = $this->request->getGet('profit');
        
        //Menyiapakan export data
        $first_column = $cell_column = 'A';
        $cell_row = $first_row = 1;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet;
        $sheet->getProperties()->setCreator('Next-POS')
            ->setLastModifiedBy('Next Innovation')
            ->setTitle("Laporan Laba Rugi")
            ->setSubject("Laporan Laba Rugi")
            ->setDescription("")
            ->setKeywords("Laporan Laba Rugi");

        $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
        $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(12);
        $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, strtoupper("Laporan Laba Rugi"));
        $cell_row++;
        $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, 'Export Date : ' . date("Y-m-d H:i:s"));
        $cell_row++;
        $cell_row++;

        $sheet->setActiveSheetIndex(0)->setCellValue('A4', 'KAS Kasir');
        $sheet->setActiveSheetIndex(0)->setCellValue('A5', 'Modal Awal (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A6', 'Total Pendapatan (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A7', 'Pengeluaran');
        $sheet->setActiveSheetIndex(0)->setCellValue('A8', 'Bahan Baku (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A9', 'Biaya Operasional (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A10', 'Pengembalian Modal Awal (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A11', 'Total Pengeluaran (Rp)');
        $sheet->setActiveSheetIndex(0)->setCellValue('A12', 'PROFIT (Rp)');

        $sheet->setActiveSheetIndex(0)->setCellValue('B5', $modal_in);
        $sheet->setActiveSheetIndex(0)->setCellValue('C6', $pendapatan);
        $sheet->setActiveSheetIndex(0)->setCellValue('B8', $bahan);
        $sheet->setActiveSheetIndex(0)->setCellValue('B9', $non_bahan);
        $sheet->setActiveSheetIndex(0)->setCellValue('B10', $modal_in);
        $sheet->setActiveSheetIndex(0)->setCellValue('C11', $pengeluaran);
        $sheet->setActiveSheetIndex(0)->setCellValue('C12', $profit);

        $sheet->getActiveSheet()->getStyle("A4:C12")->getFont()->setSize(10);

        $sheet->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("C6")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("A7")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("A11")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("C11")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("A12")->getFont()->setBold(true);
        $sheet->getActiveSheet()->getStyle("C12")->getFont()->setBold(true);

        $sheet->getActiveSheet()->getColumnDimension('A')->setWidth(25); 
        $sheet->getActiveSheet()->getColumnDimension('B')->setWidth(18); 
        $sheet->getActiveSheet()->getColumnDimension('C')->setWidth(18); 
        
        $sheet->getActiveSheet()->getStyle('B4:C12')->getAlignment()->setHorizontal('right');
        $sheet->getActiveSheet()->getStyle('B4:C11')->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(* (#,##0.00);_(* "0"??_);_(@_)');
        $sheet->getActiveSheet()->getStyle('C12')->getNumberFormat()->setFormatCode('_(* #,##0.00_);_(@_)');

        // tulis dalam format .xlsx
        $writer = new Xlsx($spreadsheet);
        $fileName = "Laporan-Laba-Rugi-".date('dmYHis').'.xlsx';

        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName);
        header('Cache-Control: max-age=0');

        $writer->save($fileName);
        // $writer->save('./assets/excel/'.$fileName);

        $url = base_url().'/'.$fileName;

        // $url2 = site_url().'public/'.$fileName;
        // $url3 = base_url('public/'.$fileName);

        $this->respondSuccess("Berhasil Mengexport data", array('url_export' => $url));
    }
}