<?php

namespace App\Controllers\Api;

class Convert extends \App\Controllers\ApiAuthUserController {

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }

    public function material()
    {
        $this->validation->setRule('origin_id', 'Produk Asal', 'required');
        $this->validation->setRule('destination_id', 'Produk Tujuan', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();
        
        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $origin_id = $this->request->getPost('origin_id');
        $destination_id = $this->request->getPost('destination_id');

        $check_origin = $this->db->table('product')->select('product_id')->getWhere(['product_id' => $origin_id, 'product_is_deleted' => 0])->getRow('product_id');
        if(empty($check_origin)) {
            $this->respondFailed("Produk asal tidak ditemukan.");
        }
        $check_detination = $this->db->table('product')->select('product_id')->getWhere(['product_id' => $destination_id, 'product_is_deleted' => 0])->getRow('product_id');
        if(empty($check_detination)) {
            $this->respondFailed("Produk tujuan tidak ditemukan.");
        }
    }

}