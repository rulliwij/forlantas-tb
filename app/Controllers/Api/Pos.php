<?php

namespace App\Controllers\Api;

class Pos extends \App\Controllers\ApiAuthUserController {

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }

    public function sales_type_list() {
        $table = "sales_type";

        $defaultSort = "sales_type_id";
        $defaultDir = "ASC";

        $arrField = array(
            'sales_type_id',
            'sales_type_name',
            'sales_type_slug',
            'sales_type_margin',
            'sales_type_value'
        );

        $where = "";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 100 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function discount_list() {
        $table = "discount";

        $defaultSort = "discount_id";
        $defaultDir = "ASC";

        $arrField = array(
            'discount_id',
            'discount_title',
            'discount_scope',
            'discount_percent',
            'discount_nominal',
        );

        $where = "";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 100 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function customer_list() {
        $table = "customer";

        $defaultSort = "customer_name";
        $defaultDir = "ASC";

        $arrField = array(
            'customer_id',
            'customer_name',
        );

        $where = "customer_store_id = '{$this->user->user_auth_user_store_id}'";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function category_list() {
        $table = "category";

        $defaultSort = "category_id";
        $defaultDir = "ASC";

        $arrField = array(
            'category_id',
            'category_name',
        );

        $where = "";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function product_list() {
        $table = "product";

        $defaultSort = "product_name";
        $defaultDir = "ASC";

        $arrField = array(
            'product_id',
            'product_category_id',
            'product_category_name',
            'product_name',
            'product_image_url',
            'product_price',
            'product_sales_type_price_json',
            'product_is_have_composition',
            'stock_balance',
            'stock_minimal',

        );

        $where = "stock_product_store_id = '{$this->user->user_auth_user_store_id}' AND product_is_deleted = '0' AND product_is_on_sale = '1'";
        $join = "JOIN stock ON stock_product_id = product_id";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $row->product_price = (int) $row->product_price;
                $row->product_sales_type_price_json = $this->sales_type($row->product_price);
                if($row->product_is_have_composition == 0){
                    $row->stock_warning = $row->stock_balance < $row->stock_minimal ? true : false;
                }
                if($row->product_is_have_composition == 1){
                    $remaining_stock_composition = [];
                    $sql = "
                        SELECT 
                            product_composition_recipe_id,
                            product_composition_qty, 
                            stock.*
                        FROM product_composition
                        JOIN stock ON stock_product_id = product_composition_recipe_id
                        WHERE product_composition_master_id = '{$row->product_id}'
                    ";
                    $product_composition = $this->db->query($sql)->getResult();
                    
                    foreach($product_composition AS $composition){
                        $remaining_stock_composition[] = ($composition->stock_balance - $composition->stock_minimal) / $composition->product_composition_qty;
                    }
                    $row->stock_balance = floor(min($remaining_stock_composition));
                    $row->stock_warning = floor(min($remaining_stock_composition)) < $row->stock_minimal ? true : false;
                }
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function save_list() {
        // Get Close Register
        // $sql_close_register = "SELECT * FROM close_register WHERE close_register_input_user_id = '{$this->user->user_auth_user_id}' ORDER BY close_register_input_datetime DESC";
        
        // $get_close_register = $this->db->query($sql_close_register)->getRow();
        
        $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'pending'";
        // if(!empty($get_close_register)){
        //     $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'pending' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' AND sales_order_input_user_id = '{$this->user->user_auth_user_id}'";
        // }

        $table = "sales_order";

        $defaultSort = "sales_order_id";
        $defaultDir = "ASC";

        $arrField = array(
            'sales_order_id',
            'sales_order_number',
            'sales_order_table_number',
            'sales_order_sales_type_slug',
            'sales_order_sales_type_name',
            'sales_order_total_discount_percent',
            'sales_order_total_discount_nominal',
            'sales_order_customer_id',
            'sales_order_customer_name',
            'sales_order_input_datetime',
            'sales_order_input_user_id',
            'sales_order_input_user_fullname',
            'sales_order_input_user_username',
            'sales_order_sales_type_slug',
            'sales_order_sales_type_name'
        );

        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";
        // print_r($sql); die;
        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                
                $dataResultDetail = array();

                $sql = "
                    SELECT
                    sales_order_detail_category_id AS category_id,
                    sales_order_detail_product_id AS id,
                    sales_order_detail_product_name AS name,
                    sales_order_detail_product_qty AS qty,
                    sales_order_detail_product_selling_price AS sellingPrice,
                    sales_order_detail_product_discount_percent AS discountPercent,
                    sales_order_detail_product_discount_nominal AS discountNominal,
                    sales_order_detail_product_nett_price AS price
                    FROM sales_order_detail
                    WHERE sales_order_detail_sales_order_id = '{$row->sales_order_id}'
                ";

                $detail = $this->db->query($sql)->getResult();
                
                foreach ($detail as $row_detail){
                    $sql = "
                        SELECT product_price 
                        FROM product
                        WHERE product_id = {$row_detail->id}
                    ";

                    $product_price = $this->db->query($sql)->getRow();

                    $row_detail->discountPercent = (int) $row_detail->discountPercent;
                    $row_detail->discountNominal = (int) $row_detail->discountNominal;
                    $row_detail->discountTitle = "";
                    $row_detail->image = "";
                    $row_detail->sellingPrice = (int) $product_price->product_price;
                    $row_detail->price = (int) $row_detail->price + $row_detail->discountNominal;
                    $row_detail->qty = (int) $row_detail->qty;
                    
                    $dataResultDetail[] = nullToString($row_detail);
                }
                
                $row->sales_order_total_discount_percent = (int) $row->sales_order_total_discount_percent;

                $row->sales_order_detail = $dataResultDetail;
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function complete_list() {
        // Get Close Register
        $sql_close_register = "SELECT * FROM close_register WHERE close_register_input_user_id = '{$this->user->user_auth_user_id}' ORDER BY close_register_input_datetime DESC";
        
        $get_close_register = $this->db->query($sql_close_register)->getRow();
        
        $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'complete'";
        if(!empty($get_close_register)){
            $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'complete' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}'";
        }

        $table = "sales_order";

        $defaultSort = "sales_order_id";
        $defaultDir = "DESC";

        $arrField = array(
            'sales_order_id',
            'sales_order_number',
            'sales_order_table_number',
            'sales_order_total_discount_percent',
            'sales_order_customer_id',
            'sales_order_customer_name',
            'sales_order_input_datetime',
            'sales_order_input_user_id',
            'sales_order_input_user_fullname',
            'sales_order_input_user_username',
            'sales_order_grand_total'
        );

        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                
                $dataResultDetail = array();

                $sql = "
                    SELECT
                    sales_order_detail_product_id AS id,
                    sales_order_detail_product_qty AS qty,
                    sales_order_detail_product_selling_price AS price
                    FROM sales_order_detail
                    WHERE sales_order_detail_sales_order_id = '{$row->sales_order_id}'
                ";

                $detail = $this->db->query($sql)->getResult();
                
                foreach ($detail as $row_detail){
                    $row_detail->price = (int) $row_detail->price;
                    $row_detail->qty = (int) $row_detail->qty;
                    
                    $dataResultDetail[] = nullToString($row_detail);
                }
                
                $row->sales_order_total_discount_percent = (int) $row->sales_order_total_discount_percent;

                $row->sales_order_detail = $dataResultDetail;
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function total_save() {
        // Get Close Register
        // $sql_close_register = "SELECT * FROM close_register WHERE close_register_input_user_id = '{$this->user->user_auth_user_id}' ORDER BY close_register_input_datetime DESC";
        
        // $get_close_register = $this->db->query($sql_close_register)->getRow();
        
        $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'pending'";
        
        // if(!empty($get_close_register)){
        //     $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'pending' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' AND sales_order_input_user_id = '{$this->user->user_auth_user_id}'";
        // }

        $sql = "
            SELECT count(sales_order_id) AS total_hold
            FROM sales_order
            WHERE {$where}
        ";
        $total_hold = (int) $this->db->query($sql)->getRow()->total_hold;

        $this->respondSuccess("Jumlah pesanan pending.", array("total_hold" => $total_hold));
    }

    public function close_list() {
        
    }

    public function check() {
        $date1 = $date2 = $this->datetime;

        $sql = "
            SELECT close_register_input_datetime
            FROM close_register
            WHERE close_register_input_user_id = '{$this->user->user_auth_user_id}'
            ORDER BY close_register_id DESC
            LIMIT 1
        ";

        $datetime_close_register = $this->db->query($sql)->getRow();

        $str_where_cashier_cash = "";

        if (!empty($datetime_close_register)) {
            $str_where_cashier_cash = " AND cashier_cash_input_datetime > '{$datetime_close_register->close_register_input_datetime}'";
        }

        $sql = "
            SELECT cashier_cash_input_datetime
            FROM cashier_cash
            WHERE cashier_cash_input_user_id = '{$this->user->user_auth_user_id}' $str_where_cashier_cash
            ORDER BY cashier_cash_id ASC
            LIMIT 1
        ";

        $datetime_cashier_cash = $this->db->query($sql)->getRow();

        if (!empty($datetime_cashier_cash)) {
            $date1 = $datetime_cashier_cash->cashier_cash_input_datetime;
        }

        if ($date1 === $date2) {
            $this->respondSuccess("Shift belum dimulai.", array(
                "already_start_shift" => false,
                "start_shift_at" => ""
            ));
        }

        $this->respondSuccess("Shift sedang berlangsung.", array(
            "already_start_shift" => true,
            "start_shift_at" => $date1
        ));
    }

    public function save() {
        $this->validation->setRule('id', 'ID', 'required|numeric');
        $this->validation->setRule('table', 'Meja', 'required|alpha_numeric_space');
        $this->validation->setRule('sales_type_slug', 'Tipe Penjualan', 'required');
        $this->validation->setRule('sales_type_name', 'Tipe Penjualan', 'required');
        $this->validation->setRule('discount_percent', 'Diskon', 'required|numeric');
        $this->validation->setRule('discount_nominal', 'Diskon', 'required|numeric');
        $this->validation->setRule('nett_price', 'Harga nett', 'required|numeric');
        $this->validation->setRule('customer_id', 'Pelanggan', 'required|numeric');
        $this->validation->setRule('customer_name', 'Pelanggan', 'required');

        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Pastikan data sudah benar.", $errorData);
        }

        $items = json_decode($this->request->getPost('items'));

        if (empty($items) && count($items) <= 0) {
            $this->respondValidation("Pastikan data sudah benar.");
        }

        $totalQty = $totalPrice = $totalDiscountItemNominal = 0;

        foreach ($items as $item) {
            if (!isset($item->id) || !isset($item->price) || !isset($item->qty)) {
                $this->respondValidation("Pastikan data sudah benar.");
            }

            $totalQty += $item->qty;
            $totalPrice += $item->qty * $item->price;
            $totalDiscountItemNominal += $item->qty * $item->discountNominal;
        }

        $id = $this->request->getPost('id');
        $table = addslashes($this->request->getPost('table'));
        $sales_type_slug = $this->request->getPost('sales_type_slug');
        $sales_type_name = $this->request->getPost('sales_type_name');
        $discount_percent = $this->request->getPost('discount_percent');
        $discount_nominal = $this->request->getPost('discount_nominal');
        $nett_price = $this->request->getPost('nett_price');
        $customer_id = $this->request->getPost('customer_id');
        $customer_name = addslashes($this->request->getPost('customer_name'));

        $respond = array();

        $isError = FALSE;
        $msgError = "";

        $this->db->transBegin();

        try {
            $tableSalesOrder = $this->db->table('sales_order');
            $tableSalesOrderDetail = $this->db->table('sales_order_detail');

            $dataSalesOrder = array(
                "sales_order_store_id" => $this->user->user_store_id,
                "sales_order_number" => $this->generateSONumber(),
                "sales_order_table_number" => $table,
                "sales_order_sales_type_slug" => $sales_type_slug,
                "sales_order_sales_type_name" => $sales_type_name,
                "sales_order_date" => $this->date,
                "sales_order_total_product_qty" => $totalQty,
                "sales_order_total_product_price" => $totalPrice,
                "sales_order_total_discount_percent" => $discount_percent,
                "sales_order_total_discount_nominal" => $discount_nominal,
                "sales_order_total_discount_item_nominal" => $totalDiscountItemNominal,
                "sales_order_total_nett_price" => $nett_price,
                "sales_order_grand_total" => $nett_price,
                "sales_order_customer_id" => $customer_id,
                "sales_order_customer_name" => $customer_name,
                "sales_order_input_datetime" => $this->datetime,
                "sales_order_input_user_id" => $this->user->user_auth_user_id,
                "sales_order_input_user_fullname" => $this->user->user_fullname,
                "sales_order_input_user_username" => $this->user->user_username,
                "sales_order_status" => "pending",
            );

            // ketika update
            if ($id > 0) {
                // tidak update so number dan status
                unset($dataSalesOrder["sales_order_number"]);
                unset($dataSalesOrder["sales_order_status"]);

                // delete semua detail
                $tableSalesOrderDetail->where('sales_order_detail_sales_order_id', $id);
                $tableSalesOrderDetail->delete();

                // update
                $tableSalesOrder->where('sales_order_id', $id);
                $tableSalesOrder->update($dataSalesOrder);
                if ($this->db->affectedRows() < 0) {
                    throw new Exception("Gagal update sales order");
                }

                $sales_order_id = $id;
            } else {
                // insert baru
                if (!$tableSalesOrder->insert($dataSalesOrder)) {
                    throw new Exception("Gagal insert sales order");
                }
                $sales_order_id = $this->db->insertID();
            }

            foreach ($items as $item) {

                $sql = "
                    SELECT product_category_id, 
                    product_category_name, 
                    product_code, 
                    product_name, 
                    product_unit, 
                    product_purchase_price
                    FROM product
                    WHERE product_id = '{$item->id}'
                ";

                $product = $this->db->query($sql)->getRow();

                $nett = $item->price - $item->discountNominal;
                $subtotal = $item->qty * $nett;

                $dataSalesOrderDetail = array(
                    "sales_order_detail_sales_order_id" => $sales_order_id,
                    "sales_order_detail_category_id" => $product->product_category_id,
                    "sales_order_detail_category_name" => $product->product_category_name,
                    "sales_order_detail_product_id" => $item->id,
                    "sales_order_detail_product_code" => $product->product_code,
                    "sales_order_detail_product_name" => $product->product_name,
                    "sales_order_detail_product_qty" => $item->qty,
                    "sales_order_detail_product_unit" => $product->product_unit,
                    "sales_order_detail_product_purchase_price" => $product->product_purchase_price,
                    "sales_order_detail_product_selling_price" => $item->sellingPrice,
                    "sales_order_detail_product_discount_percent" => $item->discountPercent,
                    "sales_order_detail_product_discount_nominal" => $item->discountNominal,
                    "sales_order_detail_product_nett_price" => $nett,
                    "sales_order_detail_product_subtotal_price" => $subtotal,
                );

                // insert detail
                if (!$tableSalesOrderDetail->insert($dataSalesOrderDetail)) {
                    throw new Exception("Gagal insert sales order detail");
                }
            }

            $sql = "
                SELECT *
                FROM sales_order
                WHERE sales_order_id = '{$sales_order_id}'
            ";
            $data_sales = $this->db->query($sql)->getRow();

            // Get Close Register
            $sql_close_register = "SELECT * FROM close_register WHERE close_register_input_user_id = '{$this->user->user_auth_user_id}' ORDER BY close_register_input_datetime DESC";
            
            $get_close_register = $this->db->query($sql_close_register)->getRow();
            
            $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'pending' AND sales_order_input_user_id = '{$this->user->user_auth_user_id}'";
            if(!empty($get_close_register)){
                $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'pending' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' AND sales_order_input_user_id = '{$this->user->user_auth_user_id}'";
            }

            $sql = "
                SELECT count(sales_order_id) AS total_hold
                FROM sales_order
                WHERE {$where} 
            ";
            $total_hold = $this->db->query($sql)->getRow()->total_hold;
            
            $respond["data"] = nullToString($data_sales);
            $respond["total_hold"] = $total_hold;
        } catch (Exception $exc) {
            $isError = TRUE;
            $msgError = $exc->getMessage();
        }

        if ($this->db->transStatus() === FALSE || $isError === TRUE) {
            $this->db->transRollback();
            $this->respondFailed("Gagal menyimpan pesanan." . $msgError);
        }

        $this->db->transCommit();
        $this->respondSuccess("Pesanan berhasil disimpan.", $respond);
    }

    public function pay() {
        $this->validation->setRule('id', 'ID', 'required|numeric');
        $this->validation->setRule('payment_method', 'Metode pembayaran', 'required');
        $this->validation->setRule('table', 'Meja', 'required|alpha_numeric_space');
        $this->validation->setRule('sales_type_slug', 'Tipe Penjualan', 'required');
        $this->validation->setRule('sales_type_name', 'Tipe Penjualan', 'required');
        $this->validation->setRule('discount_percent', 'Diskon', 'required|numeric');
        $this->validation->setRule('discount_nominal', 'Diskon', 'required|numeric');
        $this->validation->setRule('nett_price', 'Harga nett', 'required|numeric');
        $this->validation->setRule('customer_id', 'Pelanggan', 'required|numeric');
        $this->validation->setRule('customer_name', 'Pelanggan', 'required');

        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Pastikan data sudah benar.", $errorData);
        }

        $items = json_decode($this->request->getPost('items'));

        if (empty($items) && count($items) <= 0) {
            $this->respondValidation("Pastikan data sudah benar.");
        }

        $totalQty = $totalPrice = $totalDiscountItemNominal = 0;

        foreach ($items as $item) {
            if (!isset($item->id) || !isset($item->price) || !isset($item->qty)) {
                $this->respondValidation("Pastikan data sudah benar.");
            }

            $totalQty += $item->qty;
            $totalPrice += $item->qty * $item->price;
            $totalDiscountItemNominal += $item->qty * $item->discountNominal;
        }

        $id = $this->request->getPost('id');
        $payment_method = $this->request->getPost('payment_method');
        $table = addslashes($this->request->getPost('table'));
        $sales_type_slug = $this->request->getPost('sales_type_slug');
        $sales_type_name = $this->request->getPost('sales_type_name');
        $discount_percent = $this->request->getPost('discount_percent');
        $discount_nominal = $this->request->getPost('discount_nominal');
        $nett_price = $this->request->getPost('nett_price');
        $customer_id = $this->request->getPost('customer_id');
        $customer_name = addslashes($this->request->getPost('customer_name'));

        $respond = array();

        $isError = FALSE;
        $msgError = "";

        $this->db->transBegin();

        try {
            $tableSalesOrder = $this->db->table('sales_order');
            $tableSalesOrderDetail = $this->db->table('sales_order_detail');
            $tableStock = $this->db->table('stock');
            $tableStockLog = $this->db->table('stock_log');

            $dataSalesOrder = array(
                "sales_order_store_id" => $this->user->user_store_id,
                "sales_order_number" => $this->generateSONumber(),
                "sales_order_transaction_number" => $this->generateTrxNumber(),
                "sales_order_payment_method" => $payment_method,
                "sales_order_table_number" => $table,
                "sales_order_sales_type_slug" => $sales_type_slug,
                "sales_order_sales_type_name" => $sales_type_name,
                "sales_order_date" => $this->date,
                "sales_order_total_product_qty" => $totalQty,
                "sales_order_total_product_price" => $totalPrice,
                "sales_order_total_discount_percent" => $discount_percent,
                "sales_order_total_discount_nominal" => $discount_nominal,
                "sales_order_total_discount_item_nominal" => $totalDiscountItemNominal,
                "sales_order_total_nett_price" => $nett_price,
                "sales_order_grand_total" => $nett_price,
                "sales_order_customer_id" => $customer_id,
                "sales_order_customer_name" => $customer_name,
                "sales_order_input_datetime" => $this->datetime,
                "sales_order_input_user_id" => $this->user->user_auth_user_id,
                "sales_order_input_user_fullname" => $this->user->user_fullname,
                "sales_order_input_user_username" => $this->user->user_username,
                "sales_order_status" => "complete",
            );

            // ketika update
            if ($id > 0) {
                // tidak update so number
                unset($dataSalesOrder["sales_order_number"]);

                // delete semua detail
                $tableSalesOrderDetail->where('sales_order_detail_sales_order_id', $id);
                $tableSalesOrderDetail->delete();

                // update
                $tableSalesOrder->where('sales_order_id', $id);
                $tableSalesOrder->update($dataSalesOrder);
                if ($this->db->affectedRows() < 0) {
                    throw new Exception("Gagal update sales order");
                }

                $sales_order_id = $id;
            } else {
                // insert baru
                if (!$tableSalesOrder->insert($dataSalesOrder)) {
                    throw new Exception("Gagal insert sales order");
                }
                $sales_order_id = $this->db->insertID();
            }

            $remaining_stock = [];

            foreach ($items as $item) {

                $sql = "
                    SELECT *
                    FROM product
                    JOIN stock ON stock_product_id = product_id
                    WHERE product_id = '{$item->id}' 
                    AND stock_product_store_id = '{$this->user->user_store_id}'
                ";

                $product = $this->db->query($sql)->getRow();

                $nett = $item->price - $item->discountNominal;
                $subtotal = $item->qty * $nett;

                $purchase_price = $product->product_purchase_price;

                $arr_detail_composition = array();

                if($product->product_is_have_composition == 1){
                    $sql = "
                        SELECT product_composition_id AS id,
                        product_composition_master_id AS master_id,
                        product_composition_recipe_id AS recipe_id,
                        product_composition_qty AS qty,
                        product_composition_unit AS unit,
                        product_name AS name,
                        product_price AS price,
                        product_purchase_price
                        FROM product_composition
                        JOIN product ON product_id = product_composition_recipe_id
                        WHERE product_composition_master_id = '{$item->id}'
                    ";

                    $data_composition = $this->db->query($sql)->getResult();

                    if (count($data_composition) > 0) {
                        
                        $purchase_price = 0;

                        foreach ($data_composition as $key => $row) {

                            $arr_detail_composition[] = array(
                                'id' => $row->recipe_id,
                                'name' => $row->name,
                                'qty' => $row->qty * $item->qty,
                                'purchase_price' => $row->product_purchase_price,
                                'total_purchase_price' => $row->qty * $item->qty * $row->product_purchase_price,
                                'price' => $row->price,
                                'total_price' => $row->qty * $item->qty * $row->price,
                            );

                            $purchase_price += $row->qty * $item->qty * $row->product_purchase_price;
                        }

                    }
                }

                $json_composition = json_encode($arr_detail_composition);

                $dataSalesOrderDetail = array(
                    "sales_order_detail_sales_order_id" => $sales_order_id,
                    "sales_order_detail_category_id" => $product->product_category_id,
                    "sales_order_detail_category_name" => $product->product_category_name,
                    "sales_order_detail_product_id" => $item->id,
                    "sales_order_detail_product_code" => $product->product_code,
                    "sales_order_detail_product_name" => $product->product_name,
                    "sales_order_detail_product_qty" => $item->qty,
                    "sales_order_detail_product_unit" => $product->product_unit,
                    "sales_order_detail_product_purchase_price" => $purchase_price,
                    "sales_order_detail_product_selling_price" => $item->sellingPrice,
                    "sales_order_detail_product_discount_percent" => $item->discountPercent,
                    "sales_order_detail_product_discount_nominal" => $item->discountNominal,
                    "sales_order_detail_product_nett_price" => $nett,
                    "sales_order_detail_product_subtotal_price" => $subtotal,
                    "sales_order_detail_composition_json" => $json_composition,
                );

                // insert detail
                if (!$tableSalesOrderDetail->insert($dataSalesOrderDetail)) {
                    throw new Exception("Gagal insert sales order detail");
                }

                if ($product->product_is_have_composition == 0 && $product->product_is_stock == 1) {
                    $tableStock->set('stock_balance', 'stock_balance-' . $item->qty, FALSE);
                    $tableStock->where('stock_id', $product->stock_id);
                    $tableStock->update();
                    if ($this->db->affectedRows() < 0) {
                        throw new Exception("Gagal mengurangi stok");
                    }

                    $data_log = array();
                    $data_log['stock_log_store_id'] = $this->user->user_store_id;
                    $data_log['stock_log_code'] = $dataSalesOrder["sales_order_transaction_number"];
                    $data_log['stock_log_product_id'] = $item->id;
                    $data_log['stock_log_product_category_name'] = $product->product_category_name;
                    $data_log['stock_log_product_code'] = $product->product_code;
                    $data_log['stock_log_product_name'] = $product->product_name;
                    $data_log['stock_log_qty'] = $item->qty;
                    $data_log['stock_log_movement_type'] = 2;
                    $data_log['stock_log_transaction_type'] = 2;
                    $data_log['stock_log_note'] = '[Penjualan] ' . $product->product_name;
                    $data_log['stock_log_product_purchase_price'] = $item->qty * $product->product_purchase_price;
                    $data_log['stock_log_product_sales_price'] = $subtotal;
                    $data_log['stock_log_input_datetime'] = $this->datetime;
                    $data_log['stock_log_input_user_id'] = $this->user->user_auth_user_id;
                    $data_log['stock_log_input_user_username'] = $this->user->user_fullname;
                    $data_log['stock_log_input_user_fullname'] = $this->user->user_username;

                    if (!$tableStockLog->insert($data_log)) {
                        throw new Exception("Gagal insert log stok");
                    }
                }

                if($product->product_is_have_composition == 1){
                    $sql = "
                        SELECT product_composition_recipe_id, product_composition_qty
                        FROM product_composition
                        WHERE product_composition_master_id = '{$item->id}'
                    ";

                    $compositions = $this->db->query($sql)->getResult();

                    foreach ($compositions as $key => $composition) {
                        $sql = "
                            SELECT *
                            FROM product
                            JOIN stock ON stock_product_id = product_id
                            WHERE product_id = '{$composition->product_composition_recipe_id}' AND stock_product_store_id = '{$this->user->user_store_id}'
                        ";

                        $product_composition = $this->db->query($sql)->getRow();

                        if($product_composition->product_is_stock == 1){
                            $tableStock->set('stock_balance', 'stock_balance-' . ($item->qty * $composition->product_composition_qty), FALSE);
                            $tableStock->where('stock_id', $product_composition->stock_id);
                            $tableStock->update();
                            if ($this->db->affectedRows() < 0) {
                                throw new Exception("Gagal mengurangi stok");
                            }


                            $data_log = array();
                            $data_log['stock_log_store_id'] = $this->user->user_store_id;
                            $data_log['stock_log_code'] = $dataSalesOrder["sales_order_transaction_number"];
                            $data_log['stock_log_product_id'] = $composition->product_composition_recipe_id;
                            $data_log['stock_log_product_category_name'] = $product_composition->product_category_name;
                            $data_log['stock_log_product_code'] = $product_composition->product_code;
                            $data_log['stock_log_product_name'] = $product_composition->product_name;
                            $data_log['stock_log_qty'] = ($item->qty * $composition->product_composition_qty);
                            $data_log['stock_log_movement_type'] = 2;
                            $data_log['stock_log_transaction_type'] = 2;
                            $data_log['stock_log_note'] = '[Resep penjualan] ' . $product->product_name;
                            $data_log['stock_log_product_purchase_price'] = ($item->qty * $composition->product_composition_qty) * $product_composition->product_purchase_price;
                            $data_log['stock_log_product_sales_price'] = ($item->qty * $composition->product_composition_qty) * $product_composition->product_price;
                            $data_log['stock_log_input_datetime'] = $this->datetime;
                            $data_log['stock_log_input_user_id'] = $this->user->user_auth_user_id;
                            $data_log['stock_log_input_user_username'] = $this->user->user_fullname;
                            $data_log['stock_log_input_user_fullname'] = $this->user->user_username;

                            if (!$tableStockLog->insert($data_log)) {
                                throw new Exception("Gagal insert log stok");
                            }
                        }
                    }
                }
                
                if($product->product_is_have_composition == 0){
                    $sql = "
                        SELECT * 
                        FROM stock
                        WHERE stock_product_id = '{$product->product_id}'
                    ";
                    $count_stock_remaining = $this->db->query($sql)->getRow();
                    $remaining_stock[] = [
                        'product_id' => $product->product_id,
                        'stock_balance' => $count_stock_remaining->stock_balance,
                        'stock_minimal' => $count_stock_remaining->stock_minimal,
                        'stock_warning' => $count_stock_remaining->stock_balance < $count_stock_remaining->stock_minimal ? true : false
                    ];
                }

                if($product->product_is_have_composition == 1){
                    $remaining_stock_composition = [];
                    $sql = "
                        SELECT * 
                        FROM stock
                        WHERE stock_product_id = '{$product->product_id}'
                    ";
                    $count_stock_remaining = $this->db->query($sql)->getRow();

                    foreach ($arr_detail_composition as $composition) {
                        $sql = "
                            SELECT * 
                            FROM stock
                            WHERE stock_product_id = '".$composition['id']."'
                        ";
                        $count_stock_composition_remaining = $this->db->query($sql)->getRow();
                        $remaining_stock_composition[] = ($count_stock_composition_remaining->stock_balance - $count_stock_composition_remaining->stock_minimal) / $composition['qty'];
                    }
                    $remaining_stock[] = [
                        'product_id' => $product->product_id,
                        'stock_balance' => floor(min($remaining_stock_composition)),
                        'stock_minimal' => $count_stock_remaining->stock_minimal,
                        'stock_warning' => floor(min($remaining_stock_composition)) < $count_stock_remaining->stock_minimal ? true : false
                    ];
                }
            }

            $sql = "
                SELECT *
                FROM sales_order
                WHERE sales_order_id = '{$sales_order_id}'
            ";
            $data_sales = $this->db->query($sql)->getRow();

            // Get Close Register
            $sql_close_register = "SELECT * FROM close_register WHERE close_register_input_user_id = '{$this->user->user_auth_user_id}' ORDER BY close_register_input_datetime DESC";
            
            $get_close_register = $this->db->query($sql_close_register)->getRow();
            
            $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'pending' AND sales_order_input_user_id = '{$this->user->user_auth_user_id}'";
            if(!empty($get_close_register)){
                $where = "sales_order_store_id = '{$this->user->user_auth_user_store_id}' AND sales_order_status = 'pending' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' AND sales_order_input_user_id = '{$this->user->user_auth_user_id}'";
            }

            $sql = "
                SELECT count(sales_order_id) AS total_hold
                FROM sales_order
                WHERE {$where}
            ";
            $total_hold = $this->db->query($sql)->getRow()->total_hold;

            $respond["data"] = nullToString($data_sales);
            $respond["total_hold"] = $total_hold;
            $respond["remaining_stock"] = $remaining_stock;
        } catch (Exception $exc) {
            $isError = TRUE;
            $msgError = $exc->getMessage();
        }

        if ($this->db->transStatus() === FALSE || $isError === TRUE) {
            $this->db->transRollback();
            $this->respondFailed("Gagal memproses pembayaran." . $msgError);
        }

        $this->db->transCommit();
        $this->respondSuccess("Pembayaran berhasil diproses.", $respond);
    }

    public function refund(){
        $this->validation->setRule('id', 'ID refund', 'required');
        $this->validation->setRule('pin', 'PIN', 'required|numeric|max_length[4]|min_length[4]');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $id = $this->request->getPost('id');
        $pin = $this->request->getPost('pin');
        
        if ($id == 0) {
            $this->respondFailed("Data tidak ditemukan.");
        }

        $check_id = $this->db->table('sales_order')
                        ->select('sales_order_id')
                        ->getWhere(['sales_order_id' => $id, 'sales_order_status' => 'complete'])
                        ->getRow('sales_order_id');
        
        if(empty($check_id)) {
            $this->respondFailed("Data tidak ditemukan.");
        }

        $key = "password_spv";
		$data_config = $this->db->table('config')->select('*')->getWhere(['config_key' => $key])->getRow();
		
		if (empty($data_config) || !password_verify($pin, $data_config->config_value)) {
            $this->respondValidation("Cek kembali form Anda.", array("pin" => "PIN salah"));
        }

        $respond = array();

        $isError = FALSE;
        $msgError = "";

        $this->db->transBegin();

        try {
            // Update status sales order
            $arr_data = [];
            $arr_data['sales_order_status'] = 'refund';
            
            $this->db->table('sales_order')->where('sales_order_id', $id)->update($arr_data);
            if ($this->db->affectedRows() < 0) {
                $this->respondFailed("Gagal memproses refund.");
            }

            // Get sales order
            $sales_order = $this->db->table('sales_order')
                ->select('sales_order_transaction_number')
                ->getWhere(['sales_order_id' => $id])
                ->getRow();

            // Get sales order detail
            $sales_order_detail = $this->db->table('sales_order_detail')
                ->select('*')
                ->getWhere(['sales_order_detail_sales_order_id' => $id])
                ->getResult();

            $remaining_stock = [];
                
            foreach ($sales_order_detail as $detail) {
                // get product
                $sql = "
                    SELECT product_is_have_composition
                    FROM product
                    JOIN stock ON stock_product_id = product_id
                    WHERE product_id = '{$detail->sales_order_detail_product_id}' 
                    AND stock_product_store_id = '{$this->user->user_store_id}'
                ";

                $product = $this->db->query($sql)->getRow();

                // If product have composition
                if($product->product_is_have_composition == 1){
                    $remaining_stock_composition = [];
                    
                    // get product stock
                    $stock_product = $this->db->table('stock')
                        ->select('*')
                        ->getWhere([
                            'stock_product_id' => $detail->sales_order_detail_product_id, 
                            'stock_product_store_id' => $this->user->user_store_id
                        ])
                        ->getRow();

                    // decode so detail composition json
                    $product_composition_so_detail = json_decode($detail->sales_order_detail_composition_json);

                    foreach ($product_composition_so_detail as $composition) {
                        // get product from composition id
                        $product_composition = $this->db->table('product')
                            ->select('*')
                            ->getWhere(['product_id' => $composition->id])
                            ->getRow();

                        // get product composition stock
                        $stock = $this->db->table('stock')
                            ->select('*')
                            ->getWhere([
                                'stock_product_id' => $composition->id, 
                                'stock_product_store_id' => $this->user->user_store_id
                            ])
                            ->getRow();
                        
                        // update product stock
                        $arr_product = [];
                        $arr_product['stock_balance'] = (int) $stock->stock_balance + (int) $composition->qty;

                        $this->db->table('stock')->where('stock_product_id', $composition->id)->update($arr_product);

                        if ($this->db->affectedRows() < 0) {
                            $this->respondFailed("Gagal memproses refund.");
                        }

                        // create stock log
                        $data_log = array();
                        $data_log['stock_log_store_id'] = $this->user->user_store_id;
                        $data_log['stock_log_code'] = $sales_order->sales_order_transaction_number;
                        $data_log['stock_log_product_id'] = $composition->id;
                        $data_log['stock_log_product_category_name'] = $product_composition->product_category_name;
                        $data_log['stock_log_product_code'] = $product_composition->product_code;
                        $data_log['stock_log_product_name'] = $product_composition->product_name;
                        $data_log['stock_log_qty'] = $composition->qty;
                        $data_log['stock_log_movement_type'] = 1;
                        $data_log['stock_log_transaction_type'] = 2;
                        $data_log['stock_log_note'] = '[Refund] ' . $product_composition->product_name;
                        $data_log['stock_log_product_purchase_price'] = $composition->qty * $product_composition->product_purchase_price;
                        $data_log['stock_log_product_sales_price'] = $product_composition->product_price;
                        $data_log['stock_log_input_datetime'] = $this->datetime;
                        $data_log['stock_log_input_user_id'] = $this->user->user_auth_user_id;
                        $data_log['stock_log_input_user_username'] = $this->user->user_fullname;
                        $data_log['stock_log_input_user_fullname'] = $this->user->user_username;

                        if (!$this->db->table('stock_log')->insert($data_log)) {
                            throw new Exception("Gagal insert log stok");
                        }

                        $remaining_stock_composition[] = ($stock->stock_balance - $stock->stock_minimal) / $composition->qty;
                    }


                    $remaining_stock[] = [
                        'product_id' => $detail->sales_order_detail_product_id,
                        'stock_balance' => floor(min($remaining_stock_composition)) + 1,
                        'stock_minimal' => $stock_product->stock_minimal,
                        'stock_warning' => floor(min($remaining_stock_composition)) < $stock_product->stock_minimal ? true : false
                    ];
                } 
                
                // If product haven't composition
                if($product->product_is_have_composition == 0){
                    // get product stock
                    $stock = $this->db->table('stock')
                        ->select('*')
                        ->getWhere([
                            'stock_product_id' => $detail->sales_order_detail_product_id, 
                            'stock_product_store_id' => $this->user->user_store_id
                        ])
                        ->getRow();

                    // update product stock
                    $arr_product = [];
                    $arr_product['stock_balance'] = (int) $stock->stock_balance + (int) $detail->sales_order_detail_product_qty;

                    $this->db->table('stock')->where('stock_product_id', $detail->sales_order_detail_product_id)->update($arr_product);

                    if ($this->db->affectedRows() < 0) {
                        $this->respondFailed("Gagal memproses refund.");
                    }

                    // create stock log
                    $data_log = array();
                    $data_log['stock_log_store_id'] = $this->user->user_store_id;
                    $data_log['stock_log_code'] = $sales_order->sales_order_transaction_number;
                    $data_log['stock_log_product_id'] = $detail->sales_order_detail_product_id;
                    $data_log['stock_log_product_category_name'] = $detail->sales_order_detail_category_name;
                    $data_log['stock_log_product_code'] = $detail->sales_order_detail_product_code;
                    $data_log['stock_log_product_name'] = $detail->sales_order_detail_product_name;
                    $data_log['stock_log_qty'] = $detail->sales_order_detail_product_qty;
                    $data_log['stock_log_movement_type'] = 1;
                    $data_log['stock_log_transaction_type'] = 2;
                    $data_log['stock_log_note'] = '[Refund] ' . $detail->sales_order_detail_product_name;
                    $data_log['stock_log_product_purchase_price'] = $detail->sales_order_detail_product_qty * $detail->sales_order_detail_product_purchase_price;
                    $data_log['stock_log_product_sales_price'] = $detail->sales_order_detail_product_selling_price;
                    $data_log['stock_log_input_datetime'] = $this->datetime;
                    $data_log['stock_log_input_user_id'] = $this->user->user_auth_user_id;
                    $data_log['stock_log_input_user_username'] = $this->user->user_fullname;
                    $data_log['stock_log_input_user_fullname'] = $this->user->user_username;

                    if (!$this->db->table('stock_log')->insert($data_log)) {
                        throw new Exception("Gagal insert log stok");
                    }
                    
                    $remaining_stock[] = [
                        'product_id' => $detail->sales_order_detail_product_id,
                        'stock_balance' => $stock->stock_balance + 1,
                        'stock_minimal' => $stock->stock_minimal,
                        'stock_warning' => $stock->stock_balance < $stock->stock_minimal ? true : false
                    ];
                }
            }
            $respond["data"] = [];
            $respond["remaining_stock"] = $remaining_stock;
        } catch (Exception $exc) {
            $isError = TRUE;
            $msgError = $exc->getMessage();
        }

        if ($this->db->transStatus() === FALSE || $isError === TRUE) {
            $this->db->transRollback();
            $this->respondFailed("Gagal Melakukan Refund." . $msgError);
        }

        $this->db->transCommit();
        $this->respondSuccess("Berhasil Melakukan Refund.", $respond);
    }

    public function close() {
        
    }

    private function generateSONumber() {
        $sql = "
            SELECT LPAD(count(sales_order_id) + 1, 3, 0) AS number
            FROM sales_order
            WHERE sales_order_date = '{$this->date}'
        ";

        $number = $this->db->query($sql)->getRow()->number;

        return $number;
    }

    private function generateTrxNumber() {
        $sql = "
            SELECT LPAD(count(sales_order_id) + 1, 3, 0) AS number
            FROM sales_order
            WHERE sales_order_date = '{$this->date}' AND sales_order_status = 'complete'
        ";

        $number = 'INV/' . date('ymd') . $this->db->query($sql)->getRow()->number;

        return $number;
    }

    private function sales_type ($price) {
        $sql = "SELECT *
            FROM sales_type
        ";
        $queryResult = $this->db->query($sql);
        $total = 0;
        $data_json = array();
        foreach ($queryResult->getResult() as $row) {
            if ($row->sales_type_margin == 'percent') {
                $kurang = ($price * $row->sales_type_value) /100;
                $total = $price + $kurang;
            } else {
                $total = $price + $row->sales_type_value;
            }
            unset ($row->sales_type_id);
            unset ($row->sales_type_name);
            unset ($row->sales_type_margin);
            $data_json[$row->sales_type_slug] = $total;
        }
        return json_encode($data_json); 
    }


}
