<?php

namespace App\Controllers\Api;

class Profile extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }
  
  public function edit() {
    $this->validation->setRule('name', 'Nama', 'required');
    $this->validation->setRule('email', 'Email', 'required|valid_email');
    $this->validation->setRule('mobilephone', 'No Handphone', 'required');
    $validationRun = $this->validation->withRequest($this->request)->run();
    if (!$validationRun) {
      $errorData = $this->validation->getErrors();
      $this->respondValidation("Cek kembali form Anda.", $errorData);
    }
    $arr_data = array();
    $arr_data['user_fullname'] = $this->request->getPost('name');
    $arr_data['user_email'] = $this->request->getPost('email');
    $arr_data['user_mobilephone'] = $this->request->getPost('mobilephone');
    $arr_data['user_image'] = $img = $this->request->getPost('img');;
    $this->db->table('user')->where('user_id', $this->user->user_auth_user_id)->update($arr_data);
    if ($this->db->affectedRows() < 0) {
      $this->respondFailed("Gagal memperbarui data profil.");
    }
    $sql = "
      SELECT
      user_fullname,
      user_email,
      user_mobilephone,
      user_image
      FROM user
      WHERE user_id = '{$this->user->user_auth_user_id}'
      LIMIT 1
    ";

    $dataUser = $this->db->query($sql)->getRow();

    $respond = nullToString($dataUser);
    
    $this->respondSuccess("Berhasil memperbarui data profil.", $respond);
  }

  public function edit_password() {
    $this->validation->setRule('oldPassword', 'Password Lama', 'required');
    $this->validation->setRule('newPassword', 'Password Baru', 'required|min_length[5]|max_length[10]');
    $this->validation->setRule('confirmPassword', 'Ulangi Password Baru', 'required|matches[newPassword]|min_length[5]|max_length[10]');
    $validationRun = $this->validation->withRequest($this->request)->run();
    if (!$validationRun) {
        $errorData = $this->validation->getErrors();
        $this->respondValidation("Cek kembali form Anda.", $errorData);
    }

    $oldPassword = $this->request->getPost('oldPassword');
    $newPassword = $this->request->getPost('newPassword');
    $user_id = $this->user->user_auth_user_id;
    $password_encrypted = $this->db->table('user')->select('user_password')->getWhere(['user_id' => $user_id])->getRow('user_password');
    if (!empty($password_encrypted)) {
      if (password_verify($oldPassword, $password_encrypted)) {
        $passHash = password_hash($newPassword, PASSWORD_DEFAULT);

        $arr_data = array();
        $arr_data['user_password'] = $passHash;
        $this->db->table('user')->where('user_id', $user_id)->update($arr_data);
        if ($this->db->affectedRows() < 0) {
          $this->respondFailed("Gagal memperbarui data profil.");
        }
      } else {
        $this->respondFailed("Gagal memperbarui password. Password lama yang adna masukkan salah");  
      }
    } else {
      $this->respondFailed("Gagal memperbarui password.");
    }
    $this->respondSuccess("Berhasil memperbarui password.");
  }
}