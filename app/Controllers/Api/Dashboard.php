<?php

namespace App\Controllers\Api;

class Dashboard extends \App\Controllers\ApiAuthUserController {

	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
		parent::initController($request, $response, $logger);
	}


    public function list() {
        $start_date = $this->request->getGet('start_date');
        $end_date = $this->request->getGet('end_date');
        $store_id = $this->request->getGet('store_id');

        $where = "";

        if (isset($start_date) && !empty($start_date)) {
            $where_date = "AND DATE(sales_order_input_complete_datetime) = '{$start_date}'";

            if (isset($end_date) && !empty($end_date)) {
                $where_date = "AND (DATE(sales_order_input_complete_datetime) BETWEEN '{$start_date}' AND '{$end_date}')";
            }

            $where .= $where_date;
        }

        if (isset($store_id) && !empty($store_id)) {
            $where .= "AND sales_order_store_id = '{$store_id}'";
        }

        $sql = "
            SELECT COUNT(sales_order_id) AS total_struck,
            SUM(sales_order_total_product_price) AS total_bruto,
            SUM(sales_order_total_nett_price) AS total_netto,
            SUM(sales_order_total_product_qty) AS total_qty,
            SUM(sales_order_detail_product_purchase_price) AS total_purchase
            FROM sales_order
            JOIN sales_order_detail ON sales_order_detail_sales_order_id = sales_order_id
            WHERE sales_order_status = 'complete' {$where}
        ";
        $result = $this->db->query($sql)->getRow();
        $result->total_struck = (int) $result->total_struck;
        $result->total_bruto = (int) $result->total_bruto;
        $result->total_netto = (int) $result->total_netto;
        $result->total_qty = (int) $result->total_qty;
        $result->total_profit = (int) ($result->total_netto - $result->total_purchase);

        $response["summary"] = nullToString($result);

        $sql = "
            SELECT
            sales_order_detail_product_code,
            sales_order_detail_product_name,
            IFNULL(SUM(sales_order_detail_product_qty), 0) as total_qty
            FROM sales_order_detail
            JOIN sales_order ON sales_order_id = sales_order_detail_sales_order_id
            WHERE sales_order_status = 'complete' {$where}
            GROUP BY sales_order_detail_product_id
            ORDER BY SUM(sales_order_detail_product_qty) DESC
            LIMIT 5
        ";
        $top_product = $this->db->query($sql)->getResult();

        $arr_top_product = array();
        foreach ($top_product as $row) {

            $row->total_qty = (int) $row->total_qty;

            $arr_top_product[] = nullToString($row);
        }

        $response["top_product"] = $arr_top_product;

        $sql = "
            SELECT sales_order_sales_type_name, 
            COUNT(sales_order_id) AS total_struck
            FROM sales_order
            WHERE sales_order_status = 'complete' {$where}
            GROUP BY sales_order_sales_type_name
            ORDER BY COUNT(sales_order_id) DESC
            LIMIT 5
        ";

        $top_sales_type = $this->db->query($sql)->getResult();

        $arr_top_sales_type = array();

        foreach ($top_sales_type as $row) {

            $row->total_struck = (int) $row->total_struck;

            $arr_top_sales_type[] = nullToString($row);
        }

        $response["top_sales_type"] = $arr_top_sales_type;
        
        $sql = "
            SELECT sales_order_payment_method, 
            COUNT(sales_order_id) AS total_struck
            FROM sales_order
            WHERE sales_order_status = 'complete' {$where}
            GROUP BY sales_order_payment_method
            ORDER BY COUNT(sales_order_id) DESC
            LIMIT 5
        ";

        $top_payment_method = $this->db->query($sql)->getResult();

        $arr_top_payment_method = array();

        foreach ($top_payment_method as $row) {

            $row->total_struck = (int) $row->total_struck;

            $arr_top_payment_method[] = nullToString($row);
        }

        $response["top_payment_method"] = $arr_top_payment_method;


        $sql = "
            SELECT DATE(sales_order_input_datetime) AS date, 
            COUNT(sales_order_id) AS total_struck,
            SUM(sales_order_total_product_price) AS total_bruto,
            SUM(sales_order_total_nett_price) AS total_netto,
            SUM(sales_order_total_product_qty) AS total_qty,
            SUM(sales_order_total_nett_price - (
                SELECT SUM(sales_order_detail_product_purchase_price)
                FROM sales_order_detail
                WHERE sales_order_detail_sales_order_id = sales_order_id
                )
            ) AS total_profit
            FROM sales_order
            WHERE sales_order_status = 'complete' {$where}
            GROUP BY DATE(sales_order_input_datetime)
        ";

        $query_chart = $this->db->query($sql);
        
        if ($query_chart->resultID->num_rows > 0) {
            $result = $query_chart->getResult();
            
            $start_date_looping = $result[0]->date;
            $end_date_looping = $result[count($result) - 1]->date;
            
            if (isset($start_date) && !empty($start_date)) {
                $start_date_looping = $start_date;
                if (isset($end_date) && !empty($end_date)) {
                    $end_date_looping = $end_date_looping;
                }
            }
            
            $begin = new \DateTime($start_date_looping);
            $end = new \DateTime($end_date_looping);
            
            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($begin, $interval, $end);
            
            $arr_date = array();

            foreach ($period as $dt) {
                $arr_date[$dt->format("Y-m-d")] = array(
                    "total_struck" => 0,
                    "total_bruto" => 0,
                    "total_netto" => 0,
                    "total_qty" => 0,
                    "total_profit" => 0,
                );
            }

            foreach ($result as $row) {
                $arr_date[$row->date] = array(
                    "total_struck" => (int) $row->total_struck,
                    "total_bruto" => (int) $row->total_bruto,
                    "total_netto" => (int) $row->total_netto,
                    "total_qty" => (int) $row->total_qty,
                    "total_profit" => (int) $row->total_profit,
                );
            }

            $arr_graph = array();

            foreach ($arr_date as $key => $value) {
                $arr_graph[] = array_merge(array("date" => $key), $value);
            }

            $response["total_selling_chart"] = $arr_graph;
        }

        $where_stock = "";
        $product_label_stock = "(product_name) AS product_label,";
        if (isset($store_id) && !empty($store_id)) {
            $where_stock .= "AND stock_product_store_id = '{$store_id}'";
            $product_label_stock = "CONCAT(product_name, ' - ', store_name) AS product_label,";
        }

        $sql = "
            SELECT
            $product_label_stock
            product_unit,
            stock_balance,
            product_category_name
            FROM stock
            JOIN product ON stock_product_id = product_id
            JOIN store ON store_id = stock_product_store_id
            WHERE 1 $where_stock AND product_is_have_composition = '0' AND product_is_deleted = '0'
            ORDER BY stock_balance ASC, product_id ASC
            LIMIT 5
        ";

        $alert_stock = $this->db->query($sql)->getResult();

        $arr_alert_stock = array();
        foreach ($alert_stock as $row) {

            $row->stock_balance = (int) $row->stock_balance;

            $arr_alert_stock[] = nullToString($row);
        }

        $response["alert_stock"] = $arr_alert_stock;

        $this->respondSuccess("Data dashboard.", $response);
    }

    private function get_str_category($separator = ', ', $category_parent_id, $category_id, $first = true) {
        if ($category_parent_id != 0) {
            $category = $this->db->select('product_category_parent_id, product_category_id, product_category_name')
                    ->get_where('product_category', array('product_category_id' => $category_parent_id))
                    ->row();
            return $category->product_category_name . $separator . $this->get_str_category($separator, $category->product_category_parent_id, $category->product_category_id, false);
        } else {
            if ($first) {
                return $category_id;
            }
        }
    }

    public function notif_stock(){
        $store_id = $this->request->getGet('store_id');

        $where_stock = "AND stock_balance <= stock_minimal";
        $product_label_stock = "(product_name) AS product_label,";
        if (isset($store_id) && !empty($store_id)) {
            $where_stock .= "AND stock_product_store_id = '{$store_id}'";
            $product_label_stock = "CONCAT(product_name, ' - ', store_name) AS product_label,";
        }
        $sql = "
            SELECT
            $product_label_stock
            product_unit,
            stock_balance,
            product_category_name
            FROM stock
            JOIN product ON stock_product_id = product_id
            JOIN store ON store_id = stock_product_store_id
            WHERE 1 $where_stock 
            AND product_is_have_composition = '0' 
            AND product_is_on_sale = '0' 
            AND product_is_deleted = '0'
            ORDER BY stock_balance ASC, product_id ASC
        ";

        $alert_stock = $this->db->query($sql)->getResult();

        $arr_alert_stock = array();
        foreach ($alert_stock as $row) {

            $row->stock_balance = (int) $row->stock_balance;

            $arr_alert_stock[] = nullToString($row);
        }

        $response["notif_stock"] = $arr_alert_stock;

        $this->respondSuccess("Data dashboard.", $response);
    }

}