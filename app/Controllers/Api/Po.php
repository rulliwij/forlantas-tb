<?php

namespace App\Controllers\Api;

class Po extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }
  
    public function list() {
        $table = "po";

        $defaultSort = "po_id";
        $defaultDir = "DESC";

        $arrField = array(
            'po_id',
            'po_store_id',
            'po_supplier_id',
            'po_code',
            'po_title',
            'po_date',
            'po_note',
            'po_total_product_price',
            'po_total_biaya_lain',
            'po_grand_total',
            'po_qty',
            'po_input_datetime',
            'po_input_user_id',
            'po_input_user_name',
            'supplier_id',
            'supplier_name',
            'supplier_address',
            'supplier_mobilephone',
            'supplier_address',
        );

        $where = "";
        $join = "LEFT JOIN supplier ON po_supplier_id = supplier_id";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";
            
            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;
            
            $result = $queryResult->getResult();
            
            foreach ($result as $row) {
                $sql = "
                    SELECT *
                    FROM po_detail
                    WHERE po_detail_po_id = {$row->po_id}
                ";
                $detail = $this->db->query($sql)->getResult();
                $row->detail = json_encode($detail);

                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function create()
    {
        $this->validation->setRule('title', 'Judul', 'required');
        $this->validation->setRule('total_product_price', 'Biaya produk', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        if($this->user->user_user_group_id != 3){
            if(empty(json_decode($this->request->getPost('productJson')))){
                $this->respondValidation("Cek kembali form Anda.", array("productJson" => "Produk tidak boleh kosong!"));
            }
        }

        $datetime = date('Y-m-d H:i:s');

        $data['po_store_id'] = 1;
        $data['po_supplier_id'] = !empty($this->request->getPost('supplier')) ? $this->request->getPost('supplier') : 0;
        $data['po_code'] = 'PO-'.$this->generateCode('po', 'po_code');
        $data['po_title'] = $this->request->getPost('title');
        $data['po_date'] = date('Y-m-d');
        $data['po_note'] = $this->request->getPost('note');
        $data['po_total_product_price'] = $this->request->getPost('total_product_price');
        $data['po_total_biaya_lain'] = $this->request->getPost('total_biaya_lain');
        $product_json = json_decode($this->request->getPost('productJson'));
        if($this->user->user_user_group_id != 3){
            $data['po_grand_total'] = $this->request->getPost('grand_total');
            $data['po_is_product'] = "1";
            $qty = 0;
            if(!empty($product_json)){
                foreach($product_json as $product){
                    $qty++;
                }
            }
            $data['po_qty'] = $qty;
        }else{
            $data['po_grand_total'] = $this->request->getPost('total_product_price');
            $data['po_is_product'] = "0";
            $data['po_qty'] = $this->request->getPost('total_product_qty');
        }
        $data['po_input_datetime'] = $datetime;
        $data['po_input_user_id'] = $this->user->user_auth_user_id;
        $data['po_input_user_name'] = $this->db->table('user')->select('user_username')->getWhere(['user_id' => $this->user->user_auth_user_id])->getRow('user_username');
        
        $title = $this->db->table('po')->select('po_title')->getWhere(['po_title' => $this->request->getPost('title')]);
        if ($this->db->affectedRows() > 0) {
            $this->respondFailed("Gagal memperbarui pembelian. Nama pembelian sudah digunakan.");
        }
        if (!$this->db->table('po')->insert($data)) {
            $this->respondFailed("Gagal menambahkan pembelian.");
        }

        if($this->user->user_user_group_id != 3){
            $po_id = $this->db->query('SELECT LAST_INSERT_ID() AS last_insert_id')->getRow('last_insert_id');
            $product_json = json_decode($this->request->getPost('productJson'));
            if(!empty($product_json)){
                foreach($product_json as $product){
                    $product_id = $product->id;
                    $product_code = $product->code;
                    $product_name = $product->name;
                    $product_category_name = $product->category_name;
                    $product_qty = $product->product_qty;
                    $product_unit = $product->unit;
                    $product_price = $product->product_price;
        
                    $arr_data_product = [];
                    $arr_data_product['po_detail_po_id'] = $po_id;
                    $arr_data_product['po_detail_product_id'] = $product_id;
                    $arr_data_product['po_detail_product_category_name'] = $product_category_name;
                    $arr_data_product['po_detail_product_code'] = $product_code;
                    $arr_data_product['po_detail_product_name'] = $product_name;
                    $arr_data_product['po_detail_product_qty'] = $product_qty;
                    $arr_data_product['po_detail_product_unit'] = $product_unit;
                    $arr_data_product['po_detail_product_price'] = $product_price;
                    $arr_data_product['po_detail_product_subtotal_price'] = $product_price * $product_qty;
        
                    if (!$this->db->table('po_detail')->insert($arr_data_product)) {
                        $this->respondFailed("Gagal menambahkan detail pembelian.");
                    }
    
                    $last_price = $this->db->table('product')->select('product_purchase_price')->getWhere(['product_id' => $product_id])->getRow();
                    if(!empty($last_price->product_purchase_price)){
                        if($product_price > $last_price->product_purchase_price){
                            $arr_data['product_purchase_price'] = $product_price;
                            $this->db->table('product')->where('product_id', $product_id)->update($arr_data);
    
                            if ($this->db->affectedRows() < 0) {
                                $this->respondFailed("Gagal memperbarui harga produk.");
                            }
                        }
                    }
    
                    $cek_stock_balance = $this->db->table('stock')->select('stock_id')->getWhere(['stock_product_id' => $product_id])->getRow('stock_id');
                    if(!empty($cek_stock_balance)){
                        $tableStock = $this->db->table('stock');
                        $tableStock->set('stock_balance', 'stock_balance +' . $product_qty, FALSE);
                        $tableStock->where('stock_product_id', $product_id);
                        $tableStock->where('stock_product_store_id', $this->user->user_auth_user_store_id);
                        $tableStock->update();
                        if ($this->db->affectedRows() < 0) {
                             throw new Exception("Gagal update stock");
                        }
                    }else{
                        $arr_data_stock = [];
                        $arr_data_stock['stock_product_id'] = $product_id;
                        $arr_data_stock['stock_product_store_id'] = $this->user->user_auth_user_store_id;
                        $arr_data_stock['stock_balance'] = $product_qty;
                        if (!$this->db->table('stock')->insert($arr_data_stock)) {
                            $this->respondFailed("Gagal menambahkan stok produk.");
                        }
                    }
    
    
                    $sales_price = $this->db->table('product')->select('product_price')->getWhere(['product_id' => $product_id])->getRow('product_price');
                    $arr_stock_log = [];
                    $arr_stock_log['stock_log_store_id'] = $this->user->user_auth_user_store_id;
                    $arr_stock_log['stock_log_code'] = $data['po_code'];
                    $arr_stock_log['stock_log_product_id'] = $product_id;
                    $arr_stock_log['stock_log_product_category_name'] = $product_category_name;
                    $arr_stock_log['stock_log_product_code'] = $product_code;
                    $arr_stock_log['stock_log_product_name'] = $product_name;
                    $arr_stock_log['stock_log_qty'] = $product_qty;
                    $arr_stock_log['stock_log_movement_type'] = 1;
                    $arr_stock_log['stock_log_transaction_type'] = 1;
                    $arr_stock_log['stock_log_note'] = "Pembelian " . $this->request->getPost('note');
                    $arr_stock_log['stock_log_input_datetime'] = $datetime;
                    $arr_stock_log['stock_log_input_user_id'] = $this->user->user_auth_user_id;
                    $arr_stock_log['stock_log_input_user_username'] = $this->user->user_username;
                    $arr_stock_log['stock_log_input_user_fullname'] = $this->user->user_fullname;
                    $arr_stock_log['stock_log_product_purchase_price'] = ($product_price > $last_price->product_purchase_price) ? $product_price : $last_price->product_purchase_price;
                    $arr_stock_log['stock_log_product_sales_price'] = $sales_price;
    
                    if (!$this->db->table('stock_log')->insert($arr_stock_log)) {
                        throw new Exception("Gagal insert stock log");
                    }
    
                }
            }
        }

        $this->respondSuccess("Berhasil menambahkan pembelian.");
    }

    function generateCode($table_name = '', $fieldname = '', $extra = '', $digit = 5) {
        $sql = "
            SELECT
            IFNULL(LPAD(MAX(CAST(RIGHT(" . $fieldname . ", " . $digit . ") AS SIGNED) + 1), " . $digit . ", '0'), '" . sprintf('%0' . $digit . 'd', 1) . "') AS code 
            FROM " . $table_name . "
            " . $extra . "
          ";
        $query = $this->db->query($sql);
        if ($query->resultID->num_rows > 0) {
            $row = $query->getRow();
            return $row->code;
        } else {
            return '';
        }
    }

    public function detail($po_id){
        $po_id = $this->request->getPost('po_id');
        // $po_id = (int) ($this->get('po_id')) ? $this->get('po_id') : 0 ;
        print_r($po_id); die;
        $sql = "
            SELECT *
            FROM po_detail
            WHERE po_detail_po_id = {$po_id}
        ";
        return $this->db->query($sql)->getResult();
    }

}