<?php

namespace App\Controllers\Api;

class Pin extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }

    function create(){
        $this->validation->setRule('old_pin', 'PIN lama', 'required|numeric|max_length[4]|min_length[4]');
        $this->validation->setRule('pin', 'PIN baru', 'required|numeric|max_length[4]|min_length[4]');
        $this->validation->setRule('confirm', 'Ulangi PIN baru', 'required|numeric|matches[pin]|max_length[4]|min_length[4]');
        $validationRun = $this->validation->withRequest($this->request)->run();
        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
		}
		$user_id = $this->user->user_user_group_id;
		if($user_id != 1){
			$this->respondValidation("Hanya pemilik yang di ijinkan mengubah pin.");
		}
		

		$key =  "password_spv";
		$data_config = $this->db->table('config')
		->select('*')->getWhere(['config_key' => $key])->getRow();
		$confirm = password_hash($this->request->getPost('confirm'), PASSWORD_BCRYPT);

        $arr_data = [];
		
		if(!empty($data_config)){

			if(!password_verify($this->request->getPost('old_pin'), $data_config->config_value)) {
					$this->respondValidation("Cek kembali form Anda.", array("old_pin" => "PIN lama salah"));
			}

			$arr_data['config_value'] = $confirm;
			$this->db->table('config')->where('config_key', $key)->update($arr_data);
			if ($this->db->affectedRows() < 0) {
				$this->respondFailed("Gagal mengubah pin.");
			}
			$this->respondSuccess("Berhasil mengubah PIN.");
		} else {
			$arr_data['config_key'] = $key;
        	$arr_data['config_value'] = $confirm;
			if (!$this->db->table('config')->insert($arr_data)) {
				$this->respondFailed("Gagal mengubah pin.");
			}
			$this->respondSuccess("Berhasil mengubah PIN.");
		}
	}
	
	function check_validation(){
		$this->validation->setRule('pin', 'PIN', 'required|numeric');
		$validationRun = $this->validation->withRequest($this->request)->run();
        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
		}

		$user_id = $this->user->user_user_group_id;
		$pin = $this->request->getPost('pin');
		if($user_id != 1){
			$this->respondValidation("Maaf anda tidak dapat mengakses menu ini, silahkan hubungi pemilik toko.");
		}
		$key = "password_spv";
		$data_config = $this->db->table('config')
		->select('*')->getWhere(['config_key' => $key])->getRow();
		
		if (!empty($data_config) && password_verify($pin, $data_config->config_value)) {
				$this->respondSuccess("Berhasil mendapatkan akses.");
		}

		$this->respondFailed("Pin yang anda masukan salah.");
		
	}
}