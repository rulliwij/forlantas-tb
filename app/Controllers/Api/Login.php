<?php

namespace App\Controllers\Api;

class Login extends \App\Controllers\BaseApiController {

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }

    public function verify() {
        $this->validation->setRule('username', 'Username', 'required|alpha_dash|min_length[6]|max_length[20]');
        $this->validation->setRule('password', 'Password', 'required|alpha_numeric|min_length[4]|max_length[20]');

        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $username = addslashes($this->request->getPost('username'));
        $password = $this->request->getPost('password');

        $sql = "
            SELECT *
            FROM user
            WHERE user_username = '{$username}'
            LIMIT 1
        ";
        $queryUser = $this->db->query($sql);

        $user = $queryUser->getRow();

        if ($queryUser->resultID->num_rows == 0 || !password_verify($password, $user->user_password)) {
            $this->respondFailed("Username atau Password salah.");
        }

        $commonLib = service('CommonLib');

        $token = $commonLib->generateToken($this->request->getIPAddress());
        $respond = array();

        $isError = FALSE;
        $msgError = "";

        $this->db->transBegin();

        try {
            
            $tableUserAuth = $this->db->table('user_auth');
            
            //delete user_auth yg lainnya
            $tableUserAuth->where('user_auth_user_id', $user->user_id);
            $tableUserAuth->delete();
            
            //insert ke auth_admin
            $dataUserAuth = array(
                "user_auth_user_id" => $user->user_id,
                "user_auth_user_store_id" => $user->user_store_id,
                "user_auth_token" => $token,
                // "user_auth_user_agent" => $admin->admin_name,
                "user_auth_ip_address" => $this->request->getIPAddress(),
                "user_auth_created_time" => $this->datetime,
                "user_auth_expired_time" => date('Y-m-d H:i:s', strtotime($this->datetime . "+7 days")),
            );
            if (!$tableUserAuth->insert($dataUserAuth)) {
                throw new Exception("Gagal insert user auth");
            }

            //update last login
            $tableUser = $this->db->table('user');
            $tableUser->where('user_id', $user->user_id);
            $tableUser->update(array("user_last_login" => $this->datetime));
            if ($this->db->affectedRows() < 0) {
                throw new Exception("Gagal update last login");
            }


            $sql = "
                SELECT
                user_user_group_id,
                user_store_id,
                user_username,
                user_fullname,
                user_email,
                user_mobilephone,
                user_image,
                user_last_login,
                user_auth_token
                FROM user
                JOIN user_auth ON user_auth_user_id = user_id
                WHERE user_id = '{$user->user_id}'
                ORDER BY user_auth_id DESC
                LIMIT 1
            ";

            $dataUser = $this->db->query($sql)->getRow();

            $respond = nullToString($dataUser);
        } catch (Exception $exc) {
            $isError = TRUE;
            $msgError = $exc->getMessage();
        }

        if ($this->db->transStatus() === FALSE || $isError === TRUE) {
            $this->db->transRollback();
            $this->respondFailed("Login gagal.");
        }

        $this->db->transCommit();
        $this->respondSuccess("Login berhasil.", $respond);
    }

}
