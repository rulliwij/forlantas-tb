<?php

namespace App\Controllers\Api;

class Close_register extends \App\Controllers\ApiAuthUserController {
  
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }
  
    public function close_register_report() {
		// Get Close Register
        $sql_close_register = "
            SELECT * 
            FROM close_register 
            WHERE close_register_input_user_id = '{$this->user->user_auth_user_id}' 
            ORDER BY close_register_input_datetime DESC";
        
        $get_close_register = $this->db->query($sql_close_register)->getRow();
		$sql_cashier_cash = "";
		$sql_SO_cash = "";
		$sql_SO_ovo = "";
		$sql_SO_gopay = "";
		$sql_SO_transfer = "";
		$sql_SO_edc = "";
        if(empty($get_close_register)){
			// SQL Cashier Cash
			$sql_cashier_cash = "SELECT * FROM cashier_cash WHERE cashier_cash_input_user_id = {$this->user->user_auth_user_id} ORDER BY cashier_cash_input_datetime DESC"; 

			// Check Cash In
			$check_cash_in = $this->db->query($sql_cashier_cash)->getRow();
			if(empty($check_cash_in)){
				$this->respondFailed("Shift belum dimulai, silahkan masukan kas masuk");
            }
            
            // SQL Sales Order Refund
			$get_SO_refund = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'refund' GROUP BY sales_order_status";
			
			// SQL Sales Order Cash
			$sql_SO_cash = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'cash' GROUP BY sales_order_payment_method";

			// SQL Sales Order Gopay
			$sql_SO_gopay = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'gopay' GROUP BY sales_order_payment_method";

			// SQL Sales Order Ovo
			$sql_SO_ovo = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'ovo' GROUP BY sales_order_payment_method";

			// SQL Sales Order Transfer
            $sql_SO_transfer = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'transfer' GROUP BY sales_order_payment_method";
            
            // SQL Sales Order EDC
            $sql_SO_edc = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'edc' GROUP BY sales_order_payment_method";
		  
        } else {
			// SQL Cashier Cash
            $sql_cashier_cash = "SELECT * FROM cashier_cash WHERE cashier_cash_input_user_id = {$this->user->user_auth_user_id} AND cashier_cash_input_datetime > '{$get_close_register->close_register_input_datetime}' ORDER BY cashier_cash_input_datetime DESC";
            
			// Check Cash In
			$check_cash_in = $this->db->query($sql_cashier_cash)->getRow();
			if(empty($check_cash_in)){
				$this->respondFailed("Shift belum dimulai, silahkan masukan kas masuk");
            }
            
            // SQL Sales Order Refund
			$get_SO_refund = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'refund' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' GROUP BY sales_order_status";

			// SQL Sales Order Cash
			$sql_SO_cash = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'cash' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' GROUP BY sales_order_payment_method";

			// SQL Sales Order Gopay
			$sql_SO_gopay = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'gopay' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' GROUP BY sales_order_payment_method";

			// SQL Sales Order Ovo
			$sql_SO_ovo = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'ovo' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' GROUP BY sales_order_payment_method";

			// SQL Sales Order Transfer
            $sql_SO_transfer = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'transfer' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' GROUP BY sales_order_payment_method";
            
            // SQL Sales Order EDC
            $sql_SO_edc = "SELECT SUM(sales_order_grand_total) AS total_price FROM sales_order WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} AND sales_order_status = 'complete' AND sales_order_payment_method = 'edc' AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}' GROUP BY sales_order_payment_method";
		}
		// Get Cashier Cash
		$get_cashier_cash = $this->db->query($sql_cashier_cash)->getResult();
        
        $total_cash_in = 0;
        $total_cash_out = 0;
        foreach($get_cashier_cash as $row){
            $total_cash_in += (int)$row->cashier_cash_in;
            $total_cash_out += (int)$row->cashier_cash_out;
        }
        // Get SO
        $get_SO_refund = !empty($this->db->query($get_SO_refund)->getRow()) ? (int)$this->db->query($get_SO_refund)->getRow()->total_price : 0;
		$get_SO_cash = !empty($this->db->query($sql_SO_cash)->getRow()) ? (int)$this->db->query($sql_SO_cash)->getRow()->total_price : 0;
		$get_SO_gopay = !empty($this->db->query($sql_SO_gopay)->getRow()) ? (int)$this->db->query($sql_SO_gopay)->getRow()->total_price : 0;
		$get_SO_ovo = !empty($this->db->query($sql_SO_ovo)->getRow()) ? (int)$this->db->query($sql_SO_ovo)->getRow()->total_price : 0;
		$get_SO_transfer = !empty($this->db->query($sql_SO_transfer)->getRow()) ? (int)$this->db->query($sql_SO_transfer)->getRow()->total_price : 0;
		$get_SO_edc = !empty($this->db->query($sql_SO_edc)->getRow()) ? (int)$this->db->query($sql_SO_edc)->getRow()->total_price : 0;
        
        $result = [
            'total_cash_in' => $total_cash_in,
            'total_cash_out' => $total_cash_out,
            'total_cash' => $total_cash_in - $total_cash_out,
            'total_income_cash' => $get_SO_cash,
            'total_income_non_cash' => $get_SO_gopay + $get_SO_ovo + $get_SO_transfer + $get_SO_edc,
            'total_so_ovo' => $get_SO_ovo,
            'total_so_gopay' => $get_SO_gopay,
            'total_so_transfer' => $get_SO_transfer,
            'total_so_edc' => $get_SO_edc,
            'total_so_refund' => $get_SO_refund
        ];
        
        $data = array(
            'data' => $result
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function end_shift(){
        $this->validation->setRule('input_cash_recived', 'Input Kas Diterima', 'required|numeric');
        $this->validation->setRule('cash_out', 'Kas Keluar', 'required|numeric');
        $this->validation->setRule('cash_in', 'Kas Masuk', 'required|numeric');
        $this->validation->setRule('income_cash', 'Kas Masuk', 'required|numeric');
        $this->validation->setRule('Income_non_cash', 'Kas Masuk Non Tunai', 'required|numeric');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        $input_cash_recived = $this->request->getPost('input_cash_recived');
        $cash_out = $this->request->getPost('cash_out');
        $cash_in = $this->request->getPost('cash_in');
        $income_cash = $this->request->getPost('income_cash');
        $income_non_cash = $this->request->getPost('Income_non_cash');
        $detail = $this->request->getPost('detail_json');
        $grand_total = ($cash_in + $income_cash + $income_non_cash) - $cash_out;

        if($grand_total < 0){
            $this->respondFailed("Gagal mengakhiri shift. Total tidak boleh kurang dari 0");
        }

		$sql_user = "SELECT * FROM user WHERE user_id = '{$this->user->user_auth_user_id}'";
        $get_user = $this->db->query($sql_user)->getRow();
        
        // Get Close Register
        $sql_close_register = "
            SELECT * 
            FROM close_register 
            WHERE close_register_input_user_id = '{$this->user->user_auth_user_id}' 
            ORDER BY close_register_input_datetime DESC
        ";
        $get_close_register = $this->db->query($sql_close_register)->getRow();

        if(empty($get_close_register)){
            // SQL Cashier Cash
			$sql_cashier_cash = "
                SELECT * 
                FROM cashier_cash 
                WHERE cashier_cash_input_user_id = {$this->user->user_auth_user_id} 
                ORDER BY cashier_cash_input_datetime DESC
            ";

            // Check Cash In
			$check_cash_in = $this->db->query($sql_cashier_cash)->getRow();
			if(empty($check_cash_in)){
				$this->respondFailed("Shift belum dimulai, silahkan masukan kas masuk");
            }

            // SQL SO
			$get_SO = "
                SELECT sales_order_transaction_number, 
                sales_order_payment_method,
                sales_order_input_datetime,
                sales_order_grand_total
                FROM sales_order 
                WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} 
                AND sales_order_status = 'complete' 
            ";
        }else{
            // SQL Cashier Cash
            $sql_cashier_cash = "
                SELECT * 
                FROM cashier_cash 
                WHERE cashier_cash_input_user_id = {$this->user->user_auth_user_id} 
                AND cashier_cash_input_datetime > '{$get_close_register->close_register_input_datetime}' 
                ORDER BY cashier_cash_input_datetime DESC
            ";
            
			// Check Cash In
			$check_cash_in = $this->db->query($sql_cashier_cash)->getRow();
			if(empty($check_cash_in)){
				$this->respondFailed("Shift belum dimulai, silahkan masukan kas masuk");
            }

             // SQL SO
            $get_SO = "
                SELECT sales_order_transaction_number, 
                sales_order_payment_method,
                sales_order_input_datetime,
                sales_order_grand_total
                FROM sales_order 
                WHERE sales_order_input_user_id = {$this->user->user_auth_user_id} 
                AND sales_order_status = 'complete' 
                AND sales_order_input_datetime > '{$get_close_register->close_register_input_datetime}'
            ";
        }

        $get_SO = $this->db->query($get_SO)->getResult();

        $arr_data = [];
        $arr_data['close_register_store_id'] = $this->user->user_auth_user_store_id;
        $arr_data['close_register_code'] = $this->generateNumber();
        $arr_data['close_register_date'] = $this->date;
        $arr_data['close_register_total_cashier_cash_in'] = $cash_in;
        $arr_data['close_register_total_cash_received'] = $input_cash_recived;
        $arr_data['close_register_total_income_cash'] = $income_cash;
        $arr_data['close_register_total_income_non_cash'] = $income_non_cash;
        $arr_data['close_register_total_income_detail_json'] = $detail;
        $arr_data['close_register_total_cashier_cash_out'] = $cash_out;
        $arr_data['close_register_grand_total'] = $grand_total;
        $arr_data['close_register_input_datetime'] = $this->datetime;
        $arr_data['close_register_input_user_id'] = $this->user->user_auth_user_id;
        $arr_data['close_register_input_user_fullname'] = $get_user->user_fullname;
        $arr_data['close_register_input_user_username'] = $get_user->user_username;
        $arr_data['close_register_update_datetime'] = $this->datetime;
        $arr_data['close_register_update_user_fullname'] = $get_user->user_fullname;
        $arr_data['close_register_update_user_username'] = $get_user->user_username;
        if (!$this->db->table('close_register')->insert($arr_data)) {
            $this->respondFailed("Gagal mengakhiri shift.");
        }

        $detail = json_decode($detail);

        $response = array(
            "date" => $this->datetime,
            "admin" => $get_user->user_fullname,
            "cash_in" => $cash_in,
            "cash_out" => $cash_out,
            "total_cash" => $cash_in - $cash_out,
            "sales_cash" => $income_cash,
            "sales_edc" => $detail->edc,
            "sales_transfer" => $detail->transfer,
            "sales_ovo" => $detail->ovo,
            "sales_gopay" => $detail->gopay,
            "total_noncash" => $income_non_cash,
            "total_sales" => $income_cash + $income_non_cash,
            "detail" => $get_SO
        );
        
        $this->respondSuccess("Berhasil mengakhiri shift.", $response);
	}
	
	private function generateNumber() {
        $sql = "
            SELECT LPAD(count(close_register_id) + 1, 3, 0) AS number
            FROM close_register
            WHERE close_register_input_datetime = '{$this->date}'
        ";

        $number = $this->db->query($sql)->getRow()->number;

        return $number;
    }
}