<?php

namespace App\Controllers\Api;

class Product extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }
  
    public function list() {
        $table = "product";

        $defaultSort = "product_id";
        $defaultDir = "DESC";
        
        $arrField = array(
            'product_id', 
            'product_category_id', 
            'product_category_name', 
            'product_code', 
            'product_name', 
            'product_desc', 
            'product_price', 
            'product_sales_type',
            'product_addons_json',
            'product_purchase_price', 
            'product_unit', 
            'product_discount_percent', 
            'product_discount_value', 
            'product_weight', 
            'product_barcode', 
            'product_image_url', 
            'product_type', 
            'product_is_on_sale', 
            'product_is_have_composition',
            'product_is_stock',
            'product_is_deleted', 
            'product_input_datetime', 
            'product_input_user_id', 
            'product_input_user_name', 
            'product_update_user_id', 
            'product_update_user_name', 
            'product_update_datetime',
            'stock_product_id',
            'stock_product_store_id',
            'stock_minimal',
        );
        
        $where = "product_is_deleted = 0";
        $join = "JOIN stock ON product_id = stock_product_id";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));
        
        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }
        
        $start = ($page - 1) * $limit;
        
        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;
        
        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }
        
        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }
        
        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }
        
        $strField = empty($arrField) ? '*' : implode(',', $arrField);
        
        $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
        ";
        
        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();
        
        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $sql_get_product_recipe = "
                    SELECT 
                        product_composition_recipe_id,
                        product_composition_qty,
                        product_composition_unit,
                        product_composition_note,
                        product_name,
                        product_category_id,
                        product_id
                    FROM product_composition
                    JOIN product ON product_id = product_composition_recipe_id
                    WHERE product_composition_master_id = '{$row->product_id}'";
                    
                $get_product_recipe = $this->db->query($sql_get_product_recipe)->getResult();
                $row->recipe_detail = $get_product_recipe;

                if($row->product_discount_value > 0){
                    $row->product_discount_type = "Nominal";
                    $row->product_discount_nominal = $row->product_discount_value;
                }else if($row->product_discount_percent > 0){
                    $row->product_discount_type = "Persen";
                    $row->product_discount_nominal = $row->product_discount_percent;
                }else{
                    $row->product_discount_type = "";
                    $row->product_discount_nominal = "0";
                }

                if($row->product_type == "1"){
                    $row->product_type = "Persediaan";
                }else if($row->product_type == "2"){
                    $row->product_type = "Jasa";
                }else if($row->product_type == "0"){
                    $row->product_type = "Bahan";
                }
                
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );
        
        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function create(){
        $this->validation->setRule('name', 'Nama', 'required');
        $this->validation->setRule('category', 'Kategori', 'required');
        $this->validation->setRule('price', 'Harga', 'required|numeric');
        if($this->request->getPost('isStockReduction') == 'true'){
            $this->validation->setRule('currentStock', 'Stok Saat Ini', 'required');
            $this->validation->setRule('minStock', 'Stok Minimal', 'required');
        }
        if(!empty($this->request->getPost('typeDiscount')) && $this->request->getPost('typeDiscount') != 'undefined'){
            if($this->request->getPost('typeDiscount') == "Persen"){
                $this->validation->setRule('valueDiscount', 'Nilai Diskon', 'required|less_than[100]');
            }else{
                $this->validation->setRule('valueDiscount', 'Nilai Diskon', 'required|less_than['.$this->request->getPost('price').']');
            }
        }else if(!empty($this->request->getPost('valueDiscount'))){
            $this->validation->setRule('typeDiscount', 'Jenis Diskon', 'required'); 
        }

        $validationRun = $this->validation->withRequest($this->request)->run();
        
        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        
        $name = $this->request->getPost('name');
        $check_name = $this->db->table('product')->select('product_name')->getWhere(['product_name' => $name, 'product_is_deleted' => 0])->getRow('product_name');
        if (!empty($check_name)) {
            $this->respondFailed("Nama produk sudah ada.");
        }

        $category = $this->request->getPost('category');
        $category_name = $this->db->table('category')->select('category_name')->getWhere(['category_id' => $category])->getRow('category_name');
        if(empty($category_name)) {
            $this->respondFailed("Kategori tidak ditemukan.");
        }

        // $code = 'TT-'.$this->generateNumber(6);
        $code = $this->generateCode('product', 'product_code');
        $price = $this->request->getPost('price');
        $purchase_price = $this->request->getPost('purchasePrice');
        $type_discount = $this->request->getPost('typeDiscount');
        $value_discount = $this->request->getPost('valueDiscount');
        $unit = $this->request->getPost('unit');
        $description = $this->request->getPost('description');
        $weight = $this->request->getPost('weight');
        $product_type = $this->request->getPost('typeProduct');
        $product_sales_type = $this->request->getPost('typeSales') == 'true' ? 1 : 0;
        $product_is_on_sale = $this->request->getPost('isNotSale') == 'true' ? 0 : 1;
        $product_is_stock = $this->request->getPost('isStockReduction') == 'true' ? 1 : 0;
        $product_recipe_json = json_decode($this->request->getPost('productJson'));
        $product_addons_json = !empty(json_decode($this->request->getPost('addonsJson'))) ? $this->request->getPost('addonsJson') : '';
        $img = $this->request->getPost('img');
        $user_id = $this->user->user_auth_user_id;
        $user_username = $this->db->table('user')->select('user_username')->getWhere(['user_id' => $user_id])->getRow('user_username');
        $user_name = $this->db->table('user')->select('user_fullname')->getWhere(['user_id' => $user_id])->getRow('user_fullname');
        $datetime = date('Y-m-d H:i:s');

        if(empty($user_name)) {
            $this->respondFailed("Data user tidak ditemukan.");
        }
        
        $arr_data = [];
        $arr_data['product_category_id'] = $category;
        $arr_data['product_category_name'] = $category_name;
        $arr_data['product_name'] = $name;
        $arr_data['product_code'] = $code;
        $arr_data['product_desc'] = $description;
        $arr_data['product_price'] = $price;
        $arr_data['product_addons_json'] = $product_addons_json;
        $arr_data['product_purchase_price'] = $purchase_price;
        $arr_data['product_unit'] = $unit;
        $arr_data['product_weight'] = $weight;
        $arr_data['product_image_url'] = $img;
        $arr_data['product_sales_type'] = $product_sales_type;
        $arr_data['product_is_on_sale'] = $product_is_on_sale;
        $arr_data['product_is_stock'] = $product_is_stock;
        $arr_data['product_input_user_id'] = $user_id;
        $arr_data['product_input_user_name'] = $user_name;
        $arr_data['product_input_datetime'] = $datetime;

        if($value_discount != 0){
            if($type_discount == 'Nominal'){
                $arr_data['product_discount_value'] = $value_discount;
            }else{
                $arr_data['product_discount_percent'] = $value_discount;
            }
        }

        if($product_type == "Persediaan"){
            $arr_data['product_type'] = "1";
        }else if($product_type == "Jasa"){
            $arr_data['product_type'] = "2";
        }else if($product_type == "Bahan"){
            $arr_data['product_type'] = "0";
        }else{
            $arr_data['product_type'] = $product_type;
        }

        if(!empty($product_recipe_json)){
            $arr_data['product_is_have_composition'] = '1';
        }else{
            $arr_data['product_is_have_composition'] = '0';
        }

        if (!$this->db->table('product')->insert($arr_data)) {
            $this->respondFailed("Gagal menambahkan produk.");
        }else{
            $product_id = $this->db->query('SELECT LAST_INSERT_ID() AS last_insert_id')->getRow('last_insert_id');
            if(!empty($product_recipe_json)){
                foreach($product_recipe_json as $product_recipe){
                    $product_recipe_qty = $product_recipe->qty;
                    $product_recipe_unit = $product_recipe->unit;
                    $product_recipe_id = $product_recipe->recipe_id;
                    $note = htmlspecialchars($product_recipe->note);
        
                    $arr_data_recipe = [];
                    $arr_data_recipe['product_composition_master_id'] = $product_id;
                    $arr_data_recipe['product_composition_recipe_id'] = $product_recipe_id;
                    $arr_data_recipe['product_composition_qty'] = $product_recipe_qty;
                    $arr_data_recipe['product_composition_unit'] = $product_recipe_unit;
                    $arr_data_recipe['product_composition_note'] = $note;
                    $arr_data_recipe['product_composition_input_datetime'] = $datetime;
                    $arr_data_recipe['product_composition_input_user_id'] = $user_id;
                    $arr_data_recipe['product_composition_input_user_username'] = $user_username;
                    $arr_data_recipe['product_composition_input_user_fullname'] = $user_name;
        
                    if (!$this->db->table('product_composition')->insert($arr_data_recipe)) {
                        $this->respondFailed("Gagal menambahkan resep.");
                    }
                }
            }

            $current_stock = $this->request->getPost('currentStock');
            $min_stock = $this->request->getPost('minStock');

            $arr_stock = [];
            $arr_stock['stock_product_id'] = $product_id;
            $arr_stock['stock_product_store_id'] = 1;
            $arr_stock['stock_balance'] = $current_stock;
            $arr_stock['stock_minimal'] = $min_stock;
            if (!$this->db->table('stock')->insert($arr_stock)) {
                $this->respondFailed("Gagal menambahkan stok produk.");
            }

            if($product_is_stock == 1 && $current_stock > 0){
                $arr_stock_log = [];
                $arr_stock_log['stock_log_store_id'] = 1;
                $arr_stock_log['stock_log_code'] = $code;
                $arr_stock_log['stock_log_product_id'] = $product_id;
                $arr_stock_log['stock_log_product_category_name'] = $category_name;
                $arr_stock_log['stock_log_product_code'] = $code;
                $arr_stock_log['stock_log_product_name'] = $name;
                $arr_stock_log['stock_log_qty'] = $current_stock;
                $arr_stock_log['stock_log_movement_type'] = 1;
                $arr_stock_log['stock_log_transaction_type'] = 1;
                $arr_stock_log['stock_log_note'] = "Stok awal produk";
                $arr_stock_log['stock_log_input_datetime'] = $datetime;
                $arr_stock_log['stock_log_input_user_id'] = $user_id;
                $arr_stock_log['stock_log_input_user_username'] = $user_username;
                $arr_stock_log['stock_log_input_user_fullname'] = $user_name;
                $arr_stock_log['stock_log_product_purchase_price'] = $purchase_price * $current_stock;
                $arr_stock_log['stock_log_product_sales_price'] = 0;
                if (!$this->db->table('stock_log')->insert($arr_stock_log)) {
                    $this->respondFailed("Gagal menambahkan riwayat stok produk.");
                }
            }

            $this->respondSuccess("Berhasil menambahkan produk.");
        }
    }

    public function update(){
        $this->validation->setRule('name', 'Nama', 'required');
        $this->validation->setRule('price', 'Harga', 'numeric');
        $this->validation->setRule('category', 'Kategori', 'required');
        if($this->request->getPost('isStockReduction') == 'true'){
            $this->validation->setRule('minStock', 'Stok Minimal', 'required');
        }
        if(!empty($this->request->getPost('typeDiscount')) && $this->request->getPost('typeDiscount') != 'undefined'){
            if($this->request->getPost('typeDiscount') == "Persen"){
                $this->validation->setRule('valueDiscount', 'Nilai Diskon', 'required|less_than[100]');
            }else{
                $this->validation->setRule('valueDiscount', 'Nilai Diskon', 'required|less_than['.$this->request->getPost('price').']');
            }
        }else if(!empty($this->request->getPost('valueDiscount'))){
            $this->validation->setRule('typeDiscount', 'Jenis Diskon', 'required'); 
        }

        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $name = $this->request->getPost('name');
        $category = $this->request->getPost('category');
        $category_name = $this->db->table('category')->select('category_name')->getWhere(['category_id' => $category])->getRow('category_name');
        if(empty($category_name)) {
            $this->respondFailed("Kategori tidak ditemukan.");
        }

        $price = $this->request->getPost('price');
        $purchase_price = $this->request->getPost('purchasePrice');
        $type_discount = $this->request->getPost('typeDiscount');
        $value_discount = $this->request->getPost('valueDiscount');
        $unit = $this->request->getPost('unit');
        $description = $this->request->getPost('description');
        $img = $this->request->getPost('img');
        $weight = $this->request->getPost('weight');
        $product_type = $this->request->getPost('typeProduct');
        $product_sales_type = $this->request->getPost('typeSales') == 'true' ? 1 : 0;
        $product_is_on_sale = $this->request->getPost('isNotSale') == 'true' ? 0 : 1;
        $product_recipe_json = json_decode($this->request->getPost('productJson'));
        $product_addons_json = !empty(json_decode($this->request->getPost('addonsJson'))) ? $this->request->getPost('addonsJson') : '';
        $user_id = $this->user->user_auth_user_id;
        $user_name = $this->db->table('user')->select('user_fullname')->getWhere(['user_id' => $user_id])->getRow('user_fullname');
        $user_username = $this->db->table('user')->select('user_username')->getWhere(['user_id' => $user_id])->getRow('user_username');
        $datetime = date('Y-m-d H:i:s');
        if(empty($user_name)) {
            $this->respondFailed("Data user tidak ditemukan.");
        }
        $id = $this->request->getPost('id');
        $check_id = $this->db->table('product')->select('product_id')->join('stock', 'stock.stock_product_id = product.product_id')->getWhere(['product_id' => $id, 'product_is_deleted' => 0])->getRow('product_id');
        
        if(empty($check_id)) {
            $this->respondFailed("Product tidak ditemukan.");
        }

        $arr_data = [];
        $arr_data['product_category_id'] = $category;
        $arr_data['product_category_name'] = $category_name;
        $arr_data['product_name'] = $name;
        $arr_data['product_desc'] = $description;
        $arr_data['product_price'] = $price;
        $arr_data['product_addons_json'] = $product_addons_json;
        $arr_data['product_purchase_price'] = $purchase_price;
        $arr_data['product_unit'] = $unit;
        $arr_data['product_image_url'] = $img;
        $arr_data['product_weight'] = $weight;
        $arr_data['product_sales_type'] = $product_sales_type;
        $arr_data['product_is_on_sale'] = $product_is_on_sale; 
        $arr_data['product_update_user_id'] = $user_id;
        $arr_data['product_update_user_name'] = $user_name;
        $arr_data['product_update_datetime'] = $datetime;
        $arr_data['product_discount_value'] = 0;
        $arr_data['product_discount_percent'] = 0;

        if($value_discount != 0){
            if($type_discount == 'Nominal'){
                $arr_data['product_discount_value'] = $value_discount;
            }else{
                $arr_data['product_discount_percent'] = $value_discount;
            }
        }

        if($product_type == "Persediaan"){
            $arr_data['product_type'] = "1";
        }else if($product_type == "Jasa"){
            $arr_data['product_type'] = "2";
        }else if($product_type == "Bahan"){
            $arr_data['product_type'] = "0";
        }else{
            $arr_data['product_type'] = $product_type;
        }

        if(!empty($product_recipe_json)){
            $arr_data['product_is_have_composition'] = '1';
        }else{
            $arr_data['product_is_have_composition'] = '0';
        }

        $this->db->table('product')->where('product_id', $id)->update($arr_data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal memperbarui produk.");
        } else {
            $this->db->table('product_composition')->where('product_composition_master_id', $id)->delete();
            if ($this->db->affectedRows() < 0) {
                $this->respondFailed("Gagal mengubah resep.");
            }

            $this->db->table('product_composition')->where('product_composition_master_id', $id)->delete();
            if ($this->db->affectedRows() < 0) {
                $this->respondFailed("Gagal menghapus resep produk.");
            }
            

            if(!empty($product_recipe_json)){
                $this->db->table('product_composition')->where('product_composition_master_id', $id)->delete();
                if ($this->db->affectedRows() < 0) {
                    $this->respondFailed("Gagal menghapus resep produk.");
                }
                foreach($product_recipe_json as $product_recipe){
                    $product_recipe_qty = $product_recipe->qty;
                    $product_recipe_unit = $product_recipe->unit;
                    $product_recipe_id = $product_recipe->recipe_id;
                    $note = htmlspecialchars($product_recipe->note);


                    $arr_data_recipe = [];
                    $arr_data_recipe['product_composition_master_id'] = $id;
                    $arr_data_recipe['product_composition_recipe_id'] = $product_recipe_id;
                    $arr_data_recipe['product_composition_qty'] = $product_recipe_qty;
                    $arr_data_recipe['product_composition_unit'] = $product_recipe_unit;
                    $arr_data_recipe['product_composition_note'] = $note;
                    $arr_data_recipe['product_composition_input_datetime'] = $datetime;
                    $arr_data_recipe['product_composition_input_user_id'] = $user_id;
                    $arr_data_recipe['product_composition_input_user_username'] = $user_username;
                    $arr_data_recipe['product_composition_input_user_fullname'] = $user_name;

                    if (!$this->db->table('product_composition')->insert($arr_data_recipe)) {
                        $this->respondFailed("Gagal mengubah resep.");
                    }
                }
            }

            $min_stock = $this->request->getPost('minStock');

            $arr_stock = [];
            $arr_stock['stock_minimal'] = $min_stock;
            if (!$this->db->table('stock')->where('stock_product_id', $id)->update($arr_stock)) {
                $this->respondFailed("Gagal menambahkan stock produk.");
            }
            $this->respondSuccess("Berhasil memperbarui produk.");
        }
    }

    public function delete(){
        $id = $this->request->getPost('id');
        if ($id == 0) {
            $this->respondFailed("Produk tidak ditemukan.");
        }

        $check_id = $this->db->table('product')->select('product_id')->getWhere(['product_id' => $id, 'product_is_deleted' => 0])->getRow('product_id');
        if(empty($check_id)) {
            $this->respondFailed("Produk tidak ditemukan.");
        }
        $arr_data = [];
        $arr_data['product_is_deleted'] = 1;
        $this->db->table('product')->where('product_id', $id)->update($arr_data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal menghapus produk.");
        }
        $this->respondSuccess("Berhasil menghapus produk.");
    }

    public function options(){
        $data = $this->db->table('product')->select('product_id AS value, product_name AS text')->getWhere(['product_is_deleted' => 0])->getResultArray();
        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    function generateNumber($length) {
        $pin_str = "1234567809";
        for ($i = 0; $i < strlen($pin_str); $i++) {
            $pin_chars[$i] = $pin_str[$i];
        }
        // randomize the chars
        srand((float) microtime() * 1000000);
        shuffle($pin_chars);
        $pin = "";
        for ($i = 0; $i < 20; $i++) {
            $char_num = rand(1, count($pin_chars));
            $pin .= $pin_chars[$char_num - 1];
        }
        $pin = substr($pin, 0, $length);

        return $pin;
    }

    function generateCode($table_name = '', $fieldname = '', $extra = '', $digit = 5) {
        $sql = "
            SELECT
            IFNULL(LPAD(MAX(CAST(RIGHT(" . $fieldname . ", " . $digit . ") AS SIGNED) + 1), " . $digit . ", '0'), '" . sprintf('%0' . $digit . 'd', 1) . "') AS code 
            FROM " . $table_name . "
            " . $extra . "
          ";
        $query = $this->db->query($sql);
        if ($query->resultID->num_rows > 0) {
            $row = $query->getRow();
            return $row->code;
        } else {
            return '';
        }
    }

}