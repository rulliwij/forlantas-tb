<?php

namespace App\Controllers\Api;

class Cashier extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }
  
    public function list() {
        $table = "cashier_cash";

        $defaultSort = "cashier_cash_id";
        $defaultDir = "DESC";

        $arrField = array(
            'cashier_cash_id',
            'cashier_cash_store_id',
            'cashier_cash_in',
            'cashier_cash_out',
            'cashier_cash_accumulation',
            'cashier_cash_note',
            'cashier_cash_input_datetime',
            'cashier_cash_input_user_id',
            'cashier_cash_input_user_fullname',
            'cashier_cash_input_user_username',
        );

        $where = "";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    function in(){
        $this->validation->setRule('nominal', 'Nominal', 'required|numeric');
        $validationRun = $this->validation->withRequest($this->request)->run();
        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        $nominal = $this->request->getPost('nominal');
        $note = $this->request->getPost('note');
        $datetime = date('Y-m-d H:i:s');
        if ($nominal < 0) {
            $this->respondFailed("Nominal yang anda masukkan tidak sesuai.");
        }
        $user_id = $this->user->user_auth_user_id;
        $data_user = $this->db->table('user')
        ->select('user_fullname, user_username')->getWhere(['user_id' => $user_id])->getRow();
        // $data_cash = $this->db->table('cashier_cash')
        // ->select('SUM(cashier_cash_in) AS total_in, SUM(cashier_cash_out) AS total_out')
        // ->getWhere(['product_id' => $id])->getRow();
        $arr_data = [];
        $arr_data['cashier_cash_store_id'] = 1;
        $arr_data['cashier_cash_in'] = $nominal;
        $arr_data['cashier_cash_accumulation'] = $nominal;
        $arr_data['cashier_cash_note'] = $note;
        $arr_data['cashier_cash_input_datetime'] = $datetime;
        $arr_data['cashier_cash_input_user_id'] = $user_id;
        $arr_data['cashier_cash_input_user_fullname'] = $data_user->user_fullname;
        $arr_data['cashier_cash_input_user_username'] = $data_user->user_username;
        if (!$this->db->table('cashier_cash')->insert($arr_data)) {
            $this->respondFailed("Gagal menambahkan kas masuk.");
        }
        $this->respondSuccess("Berhasil menambahkan kas masuk.");
    }

    function out(){
        $this->validation->setRule('nominal', 'Nominal', 'required|numeric');
        $validationRun = $this->validation->withRequest($this->request)->run();
        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        $nominal = $this->request->getPost('nominal');
        $note = $this->request->getPost('note');
        $datetime = date('Y-m-d H:i:s');
        if ($nominal <= 0) {
            $this->respondFailed("Nominal yang anda masukkan tidak sesuai.");
        }
        $user_id = $this->user->user_auth_user_id;
        $data_user = $this->db->table('user')
        ->select('user_fullname, user_username')->getWhere(['user_id' => $user_id])->getRow();
        // $data_cash = $this->db->table('cashier_cash')
        // ->select('SUM(cashier_cash_in) AS total_in, SUM(cashier_cash_out) AS total_out')
        // ->getWhere(['product_id' => $id])->getRow();
        $arr_data = [];
        $arr_data['cashier_cash_store_id'] = 1;
        $arr_data['cashier_cash_out'] = $nominal;
        $arr_data['cashier_cash_accumulation'] = $nominal;
        $arr_data['cashier_cash_note'] = $note;
        $arr_data['cashier_cash_input_datetime'] = $datetime;
        $arr_data['cashier_cash_input_user_id'] = $user_id;
        $arr_data['cashier_cash_input_user_fullname'] = $data_user->user_fullname;
        $arr_data['cashier_cash_input_user_username'] = $data_user->user_username;
        if (!$this->db->table('cashier_cash')->insert($arr_data)) {
            $this->respondFailed("Gagal menambahkan kas keluar.");
        }
        $this->respondSuccess("Berhasil menambahkan kas keluar.");
    }

}