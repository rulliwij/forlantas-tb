<?php

namespace App\Controllers\Api;

class User extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }
  
    public function list() {
        $table = "user";

        $defaultSort = "user_id";
        $defaultDir = "DESC";

        $arrField = array(
            'user_id',
            'user_user_group_id',
            'user_store_id',
            'user_username',
            'user_password',
            'user_fullname',
            'user_email',
            'user_mobilephone',
            'user_image',
            'user_last_login',
            'user_is_active',
            'store_name',
            'user_group_name',
        );

        $where = "user_is_active = 1 AND user_is_deleted = 0";
        $join = "
            JOIN user_group ON user_group_id = user_user_group_id
            JOIN store ON store_id = user_store_id
        ";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            LIMIT {$start}, {$limit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function create(){
        $this->validation->setRule('userGroup', 'Grup karyawan', 'required');
        $this->validation->setRule('userStore', 'Toko', 'required');
        $this->validation->setRule('username', 'Username', 'required|is_unique[user.user_username,{username}]|min_length[4]');
        $this->validation->setRule('password', 'Password', 'required|min_length[6]');
        $this->validation->setRule('fullname', 'Nama lengkap', 'required|alpha_space');
        if(!empty($this->request->getPost('email'))){
            $this->validation->setRule('email', 'Email', 'valid_email|is_unique[user.user_email,{email}]');
        }
        $this->validation->setRule('mobilePhone', 'No. Telepon', 'required|is_natural|min_length[10]');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $arr_data = [];
        $arr_data['user_user_group_id'] = $this->request->getPost('userGroup');
        $arr_data['user_store_id'] = $this->request->getPost('userStore');
        $arr_data['user_username'] = $this->request->getPost('username');
        $arr_data['user_password'] = password_hash($this->request->getPost('password'),PASSWORD_BCRYPT);
        $arr_data['user_fullname'] = $this->request->getPost('fullname');
        $arr_data['user_email'] = !empty($this->request->getPost('email')) ? $this->request->getPost('email') : "";
        $arr_data['user_mobilephone'] = $this->request->getPost('mobilePhone');
        $arr_data['user_image'] = $this->request->getPost('image');
        $arr_data['user_is_active'] = 1;
        $arr_data['user_is_deleted'] = 0;
        if (!$this->db->table('user')->insert($arr_data)) {
            $this->respondFailed("Gagal menambahkan karyawan.");
        }
        
        $this->respondSuccess("Berhasil menambahkan karyawan.");
    }

    public function update(){
        if(!empty($this->request->getPost('email'))){
            $this->validation->setRule('email','Email','valid_email');
        }

        if(!empty($this->request->getPost('password'))){
            $this->validation->setRule('password','Password','min_length[6]');
        }

        $this->validation->setRule('userGroup', 'Grup karyawan', 'required');
        $this->validation->setRule('userStore', 'Toko', 'required');
        $this->validation->setRule('username', 'Username', 'required|min_length[4]');
        $this->validation->setRule('fullname', 'Fullname', 'required|alpha_space');
        $this->validation->setRule('mobilePhone', 'No. Telepon', 'required|is_natural|min_length[10]');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        $id = $this->request->getPost('user_id');

        if ($id == 0) {
            $this->respondFailed("Karyawan tidak ditemukan.");
        }

        $check_username = $this->db->table('user')->select('user_id')->getWhere(['user_id !='=> $id, 'user_username' => $this->request->getPost('username')])->getRow('user_id');
        if(!empty($check_username)) {
            $this->respondValidation("Cek kembali form Anda.",["username"=>"Username sedang digunakan."]);
        }

        $check_email = $this->db->table('user')->select('user_id')->getWhere(['user_id !='=> $id, 'user_email' => $this->request->getPost('email')])->getRow('user_id');
        if(!empty($check_email)) {
            $this->respondValidation("Cek kembali form Anda.",["email"=>"Email sedang digunakan"]);
        }

        $check_id = $this->db->table('user')->select('user_id')->getWhere(['user_id' => $id])->getRow('user_id');
        if(empty($check_id)) {
            $this->respondFailed("Karyawan tidak ditemukan.");
        }

        $arr_data = [];
        $arr_data['user_user_group_id'] = $this->request->getPost('userGroup');
        $arr_data['user_store_id'] = $this->request->getPost('userStore');
        $arr_data['user_username'] = $this->request->getPost('username');
        $arr_data['user_fullname'] = $this->request->getPost('fullname');
        if(!empty($this->request->getPost('password'))){
            $arr_data['user_password'] = password_hash($this->request->getPost('password'),PASSWORD_BCRYPT);
        }
        $arr_data['user_email'] = !empty($this->request->getPost('email')) ? $this->request->getPost('email') : "";
        $arr_data['user_mobilephone'] = $this->request->getPost('mobilePhone');
        $arr_data['user_image'] = $this->request->getPost('image');
        $arr_data['user_is_active'] = 1;
        $arr_data['user_is_deleted'] = 0;

        if(!empty($this->request->getPost('password'))){
            $arr_data['user_password'] = password_hash($this->request->getPost('password'),PASSWORD_BCRYPT);
        }
        
        $this->db->table('user')->where('user_id', $id)->update($arr_data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal mengupdate karyawan.");
        }

        $this->respondSuccess("Berhasil mengupdate karyawan.");
    }

    public function delete(){
        $id = $this->request->getPost('user_id');
        if ($id == 0) {
            $this->respondFailed("Karyawan tidak ditemukan.");
        }

        $check_id = $this->db->table('user')->select('user_id')->getWhere(['user_id' => $id])->getRow('user_id');
        if(empty($check_id)) {
            $this->respondFailed("Karyawan tidak ditemukan.");
        }

        $arr_data = [];
        $arr_data['user_is_active'] = 0;
        $arr_data['user_is_deleted'] = 1;
        $this->db->table('user')->where('user_id', $id)->update($arr_data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal menghapus karyawan.");
        }

        $this->respondSuccess("Berhasil menghapus karyawan.");
    }

}