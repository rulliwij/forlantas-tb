<?php

namespace App\Controllers\Api;

class Customer extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }
  
    public function list() {
        $table = "customer";

        $defaultSort = "customer_id";
        $defaultDir = "DESC";

        $arrField = array(
            'customer_id',
            'customer_name',
            'customer_email',
            'customer_gender',
            'customer_birthplace',
            'customer_birthdate',
            'customer_address',
            'customer_mobilephone'
        );

        $where = "";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $row->customer_birthdate = $row->customer_birthdate == 0 ? '' : $row->customer_birthdate;
                $row->customer_mobilephone = !empty($row->customer_mobilephone) ? $row->customer_mobilephone : '';
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
  }

    public function create(){
        $this->validation->setRule('name', 'Nama', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $arr_data = [];
        $arr_data['customer_store_id'] = 1;
        $arr_data['customer_name'] = $this->request->getPost('name');
        $arr_data['customer_email'] = $this->request->getPost('email');
        $arr_data['customer_gender'] = $this->request->getPost('gender');
        $arr_data['customer_birthplace'] = "";
        $arr_data['customer_birthdate'] = "";
        $arr_data['customer_address'] = $this->request->getPost('address');
        $arr_data['customer_mobilephone'] = $this->request->getPost('mobilephone');
        if (!$this->db->table('customer')->insert($arr_data)) {
            $this->respondFailed("Gagal menambahkan pelanggan.");
        }
        
        $this->respondSuccess("Berhasil menambahkan pelanggan.");
    }

    public function update()
    {
        $this->validation->setRule('name', 'Nama', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        $name = $this->request->getPost('name');
        $id = $this->request->getPost('id');
        $arr_data = [];
        $arr_data['customer_store_id'] = 1;
        $arr_data['customer_name'] = $this->request->getPost('name');
        $arr_data['customer_email'] = $this->request->getPost('email');
        $arr_data['customer_gender'] = $this->request->getPost('gender');
        $arr_data['customer_birthplace'] = "";
        $arr_data['customer_birthdate'] = "";
        $arr_data['customer_address'] = $this->request->getPost('address');
        $arr_data['customer_mobilephone'] = $this->request->getPost('mobilephone');
        $this->db->table('customer')->where('customer_id', $this->request->getPost('id'))->update($arr_data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal memperbarui pelanggan.");
        }
        
        $this->respondSuccess("Berhasil memperbarui pelanggan.");
    }
}