<?php

namespace App\Controllers\Api;

class Stock extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }

  public function list() {
    $table = "stock";
        $defaultSort = "stock_id";
        $defaultDir = "DESC";

    $arrField = array(
        'stock_id',
        'stock_product_id',
        'stock_product_store_id',
        'stock_balance',
        'product_code',
        'product_name',
    );

    $where = "product_is_deleted = 0";
    $join = "JOIN product ON stock_product_id = product_id";

    $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
    $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

    $search = (array) $this->request->getGet('search');
    $filter = (array) $this->request->getGet('filter');
    $sort = (string) $this->request->getGet('sort');
    $dir = (string) strtoupper($this->request->getGet('dir'));

    if ($dir !== 'ASC' && $dir !== 'DESC') {
        $dir = $defaultDir;
    }

    $start = ($page - 1) * $limit;

    $joinDetail = empty($join) ? "" : $join;
    $whereDetail = empty($where) ? " 1 = 1 " : $where;

    if (is_array($search)) {
        $whereDetail .= buildWhereSearch($search, $arrField);
    }

    if (is_array($filter)) {
        $whereDetail .= buildWhereFilter($filter, $arrField);
    }

    if (!in_array($sort, $arrField)) {
        $sort = $defaultSort;
    }

    $strField = empty($arrField) ? '*' : implode(',', $arrField);

    $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
    ";

    $queryResult = $this->db->query($sql);

    $totalData = 0;
    $dataResult = array();

    if ($queryResult->resultID->num_rows > 0) {

        $sqlTotal = "SELECT FOUND_ROWS() AS row";

        $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

        $result = $queryResult->getResult();

        foreach ($result as $row) {
            $dataResult[] = nullToString($row);
        }
    }

    $data = array(
        'data' => $dataResult,
        'pagination' => pageGenerator($totalData, $page, $limit)
    );

    $this->respondSuccess("Berhasil mendapatkan data.", $data);
  }
  
  public function list_log() {
    $table = "stock_log";
        $defaultSort = "stock_log_id";
        $defaultDir = "DESC";

    $arrField = array(
        'stock_log_id',
        'stock_log_store_id',
        'stock_log_code',
        'stock_log_product_id',
        'stock_log_product_category_name',
        'stock_log_product_code',
        'stock_log_product_name',
        'stock_log_qty',
        'stock_log_movement_type',
        'stock_log_transaction_type',
        'stock_log_note',
        'stock_log_input_datetime',
        'stock_log_input_user_id',
        'stock_log_input_user_username',
        'stock_log_input_user_fullname',
    );

    $where = "";
    $join = "";

    $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
    $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

    $search = (array) $this->request->getGet('search');
    $filter = (array) $this->request->getGet('filter');
    $sort = (string) $this->request->getGet('sort');
    $dir = (string) strtoupper($this->request->getGet('dir'));

    if ($dir !== 'ASC' && $dir !== 'DESC') {
        $dir = $defaultDir;
    }

    $start = ($page - 1) * $limit;

    $joinDetail = empty($join) ? "" : $join;
    $whereDetail = empty($where) ? " 1 = 1 " : $where;

    if (is_array($search)) {
        $whereDetail .= buildWhereSearch($search, $arrField);
    }

    if (is_array($filter)) {
        $whereDetail .= buildWhereFilter($filter, $arrField);
    }

    if (!in_array($sort, $arrField)) {
        $sort = $defaultSort;
    }

    $strField = empty($arrField) ? '*' : implode(',', $arrField);

    $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
    ";

    $queryResult = $this->db->query($sql);

    $totalData = 0;
    $dataResult = array();

    if ($queryResult->resultID->num_rows > 0) {

        $sqlTotal = "SELECT FOUND_ROWS() AS row";

        $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

        $result = $queryResult->getResult();

        foreach ($result as $row) {
            $dataResult[] = nullToString($row);
        }
    }

    $data = array(
        'data' => $dataResult,
        'pagination' => pageGenerator($totalData, $page, $limit)
    );

    $this->respondSuccess("Berhasil mendapatkan data.", $data);
  }
}