<?php

namespace App\Controllers\Api;

class Stock extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }

  public function list() {
    $table = "stock";
    $defaultSort = "stock_id";
    $defaultDir = "DESC";

    $arrField = array(
        'stock_id',
        'stock_product_id',
        'stock_product_store_id',
        'stock_balance',
        'product_code',
        'product_name',
        'beli',
        'jual',
        'laba',
    );

    $where = "product_is_deleted = 0 AND stock_product_store_id = '{$this->user->user_auth_user_store_id}' AND stock_log_store_id = '{$this->user->user_auth_user_store_id}'";
    $join = "
        JOIN product ON stock_product_id = product_id
        JOIN stock_log ON stock_log_product_id = product_id
    ";

    $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
    $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');
    
    $search = (array) $this->request->getGet('search');
    $filter = (array) $this->request->getGet('filter');
    $sort = (string) $this->request->getGet('sort');
    $dir = (string) strtoupper($this->request->getGet('dir'));

    if ($dir !== 'ASC' && $dir !== 'DESC') {
        $dir = $defaultDir;
    }

    $start = ($page - 1) * $limit;

    $joinDetail = empty($join) ? "" : $join;
    $whereDetail = empty($where) ? " 1 = 1 " : $where;

    if (($keyDelete = array_search('beli', $arrField)) !== false) {
        unset($arrField[$keyDelete]);
    }

    if (($keyDelete = array_search('jual', $arrField)) !== false) {
        unset($arrField[$keyDelete]);
    }

    if (($keyDelete = array_search('laba', $arrField)) !== false) {
        unset($arrField[$keyDelete]);
    }

    if (is_array($search)) {
        $whereDetail .= buildWhereSearch($search, $arrField);
    }

    if (is_array($filter)) {
        $whereDetail .= buildWhereFilter($filter, $arrField);
    }

    if (!in_array($sort, $arrField)) {
        $sort = $defaultSort;
    }

    $strField = empty($arrField) ? '*' : implode(',', $arrField);

    $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        SUM(stock_log_product_purchase_price) AS beli,
        SUM(stock_log_product_sales_price) AS jual,
        (SUM(stock_log_product_sales_price) - SUM(stock_log_product_purchase_price)) AS laba,
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        GROUP BY stock_product_id
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
    ";
    
    $queryResult = $this->db->query($sql);

    $totalData = 0;
    $dataResult = array();

    if ($queryResult->resultID->num_rows > 0) {

        $sqlTotal = "SELECT FOUND_ROWS() AS row";

        $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

        $result = $queryResult->getResult();
        foreach ($result as $row) {
            // $stock_log = $this->db->table('stock_log')
            // ->select('SUM(stock_log_product_purchase_price) AS beli, SUM(stock_log_product_sales_price) AS jual')->getWhere(['stock_log_product_id' => $row->stock_product_id])->getRow();
            // if (!empty($stock_log->beli)) {
            //     $row->beli = $stock_log->beli;
            // } else {
            //     $row->beli = 0;
            // }
            // if (!empty($stock_log->jual)) {
            //     $row->jual = $stock_log->jual;
            // } else {
            //     $row->jual = 0;
            // }
            // $row->laba = $row->jual - $row->beli;
            // if ($row->jual < 1) {
            //     $row->jual = "0";
            // }
            // if ($row->beli < 1) {
            //     $row->beli = "0";
            // }
            // if ($row->laba < 1) {
            //     $row->laba = "0";
            // }
            $dataResult[] = nullToString($row);
        }
    }

    $data = array(
        'data' => $dataResult,
        'pagination' => pageGenerator($totalData, $page, $limit)
    );

    $this->respondSuccess("Berhasil mendapatkan data.", $data);
  }
  
  public function list_log() {
    $table = "stock_log";
        $defaultSort = "stock_log_id";
        $defaultDir = "DESC";

    $arrField = array(
        'stock_log_id',
        'stock_log_store_id',
        'stock_log_code',
        'stock_log_product_id',
        'stock_log_product_category_name',
        'stock_log_product_code',
        'stock_log_product_name',
        'stock_log_qty',
        'stock_log_movement_type',
        'stock_log_transaction_type',
        'stock_log_note',
        'stock_log_input_datetime',
        'stock_log_input_user_id',
        'stock_log_input_user_username',
        'stock_log_input_user_fullname',
        // 'product_purchase_price',
        // 'product_price',
    );

    $where = "";
    // $join = "JOIN product ON stock_log_product_id = product_id";
    $join = "";

    $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
    $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

    $search = (array) $this->request->getGet('search');
    $filter = (array) $this->request->getGet('filter');
    $sort = (string) $this->request->getGet('sort');
    $dir = (string) strtoupper($this->request->getGet('dir'));

    if ($dir !== 'ASC' && $dir !== 'DESC') {
        $dir = $defaultDir;
    }

    $start = ($page - 1) * $limit;

    $joinDetail = empty($join) ? "" : $join;
    $whereDetail = empty($where) ? " 1 = 1 " : $where;

    if (is_array($search)) {
        $whereDetail .= buildWhereSearch($search, $arrField);
    }

    if (is_array($filter)) {
        $whereDetail .= buildWhereFilter($filter, $arrField);
    }

    if (!in_array($sort, $arrField)) {
        $sort = $defaultSort;
    }

    $strField = empty($arrField) ? '*' : implode(',', $arrField);

    $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
    ";

    $queryResult = $this->db->query($sql);

    $totalData = 0;
    $dataResult = array();

    if ($queryResult->resultID->num_rows > 0) {

        $sqlTotal = "SELECT FOUND_ROWS() AS row";

        $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

        $result = $queryResult->getResult();

        foreach ($result as $row) {
            // $beli = $row->product_purchase_price * $row->stock_log_qty;
            // $jual = 0;
            // $laba = 0;
            // if ($row->stock_log_movement_type == 2) {
            //     $jual = $row->product_price * $row->stock_log_qty;
            //     $laba = $jual - $beli;
            //     if ($laba < 0) {
            //         $laba = 0;
            //     }
            // }
            // if ($beli > 0) {
            //     $row->beli = $beli;
            // } else {
            //     $row->beli = "0";
            // }

            // if ($jual > 0) {
            //     $row->jual = $jual;
            // } else {
            //     $row->jual = "0";
            // }

            // if ($laba > 0) {
            //     $row->laba = $laba;
            // } else {
            //     $row->laba = "0";
            // }
            $dataResult[] = nullToString($row);
        }
    }
    $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        SUM(CASE WHEN stock_log_movement_type = 1 THEN stock_log_qty ELSE 0 END) AS total_in,
        SUM(CASE WHEN stock_log_movement_type = 2 THEN stock_log_qty ELSE 0 END) AS total_out,
        SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_qty ELSE 0 END) AS buy,
        SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_qty ELSE 0 END) AS sell,
        SUM(CASE WHEN stock_log_transaction_type = 3 THEN stock_log_qty ELSE 0 END) AS adjustment
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
    ";

    $querySummary = $this->db->query($sql)->getRow();

    $data = array(
        'data' => $dataResult,
        'other_data' => $querySummary,
        'pagination' => pageGenerator($totalData, $page, $limit)
    );

    $this->respondSuccess("Berhasil mendapatkan data.", $data);
  }

  public function list_laba_rugi() {
    $table = "stock";
    $defaultSort = "stock_id";
    $defaultDir = "DESC";

    $arrField = array(
        'stock_id',
        'stock_product_id',
        'stock_product_store_id',
        'stock_balance',
        'product_code',
        'product_name',
        'stock_log_input_datetime',
        'stok_sekarang',
        'beli',
        'jual',
        'laba',
    );

    $where = "product_is_deleted = 0 AND stock_product_store_id = '{$this->user->user_auth_user_store_id}' AND stock_log_store_id = '{$this->user->user_auth_user_store_id}'";
    $join = "
        JOIN product ON stock_product_id = product_id
        JOIN stock_log ON stock_log_product_id = product_id
    ";

    $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
    $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');
    
    $search = (array) $this->request->getGet('search');
    $filter = (array) $this->request->getGet('filter');
    $sort = (string) $this->request->getGet('sort');
    $dir = (string) strtoupper($this->request->getGet('dir'));

    if ($dir !== 'ASC' && $dir !== 'DESC') {
        $dir = $defaultDir;
    }

    $start = ($page - 1) * $limit;

    $joinDetail = empty($join) ? "" : $join;
    $whereDetail = empty($where) ? " 1 = 1 " : $where;

    if (($keyDelete = array_search('stok_sekarang', $arrField)) !== false) {
        unset($arrField[$keyDelete]);
    }

    if (($keyDelete = array_search('beli', $arrField)) !== false) {
        unset($arrField[$keyDelete]);
    }

    if (($keyDelete = array_search('jual', $arrField)) !== false) {
        unset($arrField[$keyDelete]);
    }

    if (($keyDelete = array_search('laba', $arrField)) !== false) {
        unset($arrField[$keyDelete]);
    }

    if (is_array($search)) {
        $whereDetail .= buildWhereSearch($search, $arrField);
    }

    if (is_array($filter)) {
        $whereDetail .= buildWhereFilter($filter, $arrField);
    }

    if (!in_array($sort, $arrField)) {
        $sort = $defaultSort;
    }

    $strField = empty($arrField) ? '*' : implode(',', $arrField);

    $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        (SUM(CASE WHEN stock_log_movement_type = 1 THEN stock_log_qty ELSE 0 END) - SUM(CASE WHEN stock_log_movement_type = 2 THEN stock_log_qty ELSE 0 END)) AS stok_sekarang,
        (SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_product_purchase_price ELSE 0 END)) AS beli,
        (SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_product_sales_price ELSE 0 END)) AS jual,
        ((SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_product_sales_price ELSE 0 END)) - (SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_product_purchase_price ELSE 0 END))) AS laba,
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        GROUP BY stock_product_id
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
    ";
    
    $queryResult = $this->db->query($sql);

    $totalData = 0;
    $dataResult = array();

    if ($queryResult->resultID->num_rows > 0) {

        $sqlTotal = "SELECT FOUND_ROWS() AS row";

        $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

        $result = $queryResult->getResult();
        foreach ($result as $row) {
            $dataResult[] = nullToString($row);
        }
    }

    $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        (SUM(CASE WHEN stock_log_movement_type = 1 THEN stock_log_qty ELSE 0 END) - SUM(CASE WHEN stock_log_movement_type = 2 THEN stock_log_qty ELSE 0 END)) AS stok_sekarang,
        (SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_product_purchase_price ELSE 0 END)) AS beli,
        (SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_product_sales_price ELSE 0 END)) AS jual,
        ((SUM(CASE WHEN stock_log_transaction_type = 2 THEN stock_log_product_sales_price ELSE 0 END)) - (SUM(CASE WHEN stock_log_transaction_type = 1 THEN stock_log_product_purchase_price ELSE 0 END))) AS laba
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
    ";

    $querySummary = $this->db->query($sql)->getRow();

    $data = array(
        'data' => $dataResult,
        'other_data' => $querySummary,
        'pagination' => pageGenerator($totalData, $page, $limit)
    );

    $this->respondSuccess("Berhasil mendapatkan data.", $data);
  }
}