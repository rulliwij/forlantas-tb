<?php

namespace App\Controllers\Api;

class Category extends \App\Controllers\ApiAuthUserController {
  
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
  }
  
    public function list() {
        $table = "category";

        $defaultSort = "category_id";
        $defaultDir = "DESC";

        $arrField = array(
            'category_id',
            'category_parent_id',
            'category_name',
            'category_icon',
            'category_image',
            'category_order_by',
            'category_is_active',
        );

        $where = "category_is_active = 1";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function options(){
        $data = $this->db->table('category')->select('category_id AS value, category_name AS text')->getWhere(['category_is_active' => 1])->getResultArray();
        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function create()
    {
        $this->validation->setRule('name', 'Nama', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        
        $data['category_name'] = $this->request->getPost('name');
        $data['category_icon'] = $this->request->getPost('icon');
        $data['category_image'] = $this->request->getPost('img');
        $check_name = $this->db->table('category')->select('category_name')->getWhere(['category_name' => $this->request->getPost('name')]);
        if ($this->db->affectedRows() > 0) {
            $this->respondValidation("Cek kembali form Anda.", array('name' => "Nama sudah digunakan"));
        }
        if (!$this->db->table('category')->insert($data)) {
            $this->respondFailed("Gagal menambahkan kategori.");
        }

        $response['category_id'] = $this->db->insertID();

        $this->respondSuccess("Berhasil menambahkan kategori.", $response);
    }

    public function update()
    {
        $this->validation->setRule('name', 'Nama', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();
        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        $name = $this->request->getPost('name');
        $id = $this->request->getPost('id');
        $check_name = $this->db->table('category')->select('category_name, category_id')->getWhere(['category_name' => $name]);
        if ($this->db->affectedRows() > 0) {
            if ($check_name->getRow('category_id') !== $id){
                 $this->respondValidation("Cek kembali form Anda.", array('name' => "Nama sudah digunakan"));
            }
        }
        $data['category_name'] = $name;
        $data['category_icon'] = $this->request->getPost('icon');
        $data['category_image'] = $this->request->getPost('img');
        $this->db->table('category')->where('category_id', $this->request->getPost('id'))->update($data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal memperbarui kategori.");
        }
        $this->respondSuccess("Berhasil memperbarui kategori.");
    }

    public function delete()
    {
        $this->validation->setRule('id', 'Kategori', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $check_product = $this->db->table('product')->select('product_id')->getWhere(['product_category_id' => $this->request->getPost('id')])->getRow('product_id');
        if (!empty($check_product)) {
            $this->respondFailed("Gagal menghapus kategori. Terdapat produk yang sudah menggunakan kategori ini");
        }

        $this->db->table('category')->where('category_id', $this->request->getPost('id'))->delete();
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal menghapus kategori.");
        }

        $this->respondSuccess("Berhasil menghapus kategori.");
    }

}