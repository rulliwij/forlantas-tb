<?php

namespace App\Controllers\Api;

class Discount extends \App\Controllers\ApiAuthUserController {

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }
    
    public function list() {
        $table = "discount";

        $defaultSort = "discount_title";
        $defaultDir = "ASC";

        $arrField = array(
            'discount_id',
            'discount_title',
            'discount_scope',
            'discount_percent',
            'discount_nominal'
        );

        $where = "";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') == 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $strLimit = "";

        if ($limit > 0) {
            $strLimit = "LIMIT {$start}, {$limit}";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS
            {$strField}
            FROM {$table}
            {$joinDetail}
            WHERE {$whereDetail}
            ORDER BY {$sort} {$dir}
            {$strLimit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $this->respondSuccess("Berhasil mendapatkan data.", $data);
    }

    public function create(){
        $this->validation->setRule('discount_title', 'Nama', 'required');
        $this->validation->setRule('discount_scope', 'Cakupan', 'required');
        $this->validation->setRule('discount_percent', 'Diskon Persen', 'required');
        // $this->validation->setRule('discount_nominal', 'Diskon Nominal', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }
        $name = $this->request->getPost('discount_title');
        $scope = $this->request->getPost('discount_scope');
        $percent= $this->request->getPost('discount_percent');
        $nominal= 0;

        $check_name = $this->db->table('discount')->select('discount_title')->getWhere(['discount_title' => $name])->getRow('discount_title');
        if (!empty($check_name)) {
            $this->respondFailed("Nama diskon penjualan sudah ada.");
        }

        $arr_data = [];
        $arr_data['discount_title'] = $name;
        $arr_data['discount_scope'] = $scope;
        $arr_data['discount_percent'] = $percent;
        $arr_data['discount_nominal'] = $nominal;
        if (!$this->db->table('discount')->insert($arr_data)) {
            $this->respondFailed("Gagal menambahkan diskon.");
        }
        
        $this->respondSuccess("Berhasil menambahkan diskon.");
    }

    public function update(){
        $this->validation->setRule('discount_title', 'Nama', 'required');
        $this->validation->setRule('discount_scope', 'Cakupan', 'required');
        $this->validation->setRule('discount_percent', 'Diskon Persen', 'required');
        // $this->validation->setRule('discount_nominal', 'Diskon Nominal', 'required');
        $validationRun = $this->validation->withRequest($this->request)->run();

        if (!$validationRun) {
            $errorData = $this->validation->getErrors();
            $this->respondValidation("Cek kembali form Anda.", $errorData);
        }

        $id = $this->request->getPost('discount_id');
        $name = $this->request->getPost('discount_title');
        $scope = $this->request->getPost('discount_scope');
        $percent= $this->request->getPost('discount_percent');
        $nominal= 0;

        if ($id == 0) {
            $this->respondFailed("Diskon tidak ditemukan.");
        }

        $check_id = $this->db->table('discount')->select('discount_id')->getWhere(['discount_id' => $id])->getRow('discount_id');
        if(empty($check_id)) {
            $this->respondFailed("Diskon tidak ditemukan.");
        }

        $arr_data = [];
        $arr_data['discount_title'] = $name;
        $arr_data['discount_scope'] = $scope;
        $arr_data['discount_percent'] = $percent;
        $arr_data['discount_nominal'] = $nominal;
        $this->db->table('discount')->where('discount_id', $id)->update($arr_data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal memperbarui diskon.");
        }
        $this->respondSuccess("Berhasil memperbarui diskon.");
    }

    public function delete(){
        $id = $this->request->getPost('id');
        if ($id == 0) {
          $this->respondFailed("Diskon tidak ditemukan.");
        }

        $check_id = $this->db->table('discount')->select('discount_id')->getWhere(['discount_id' => $id])->getRow('discount_id');
        if(empty($check_id)) {
            $this->respondFailed("Diskon tidak ditemukan.");
        }

        $this->db->table('discount')->where('discount_id', $id)->delete();
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal menghapus diskon.");
        }

        $this->respondSuccess("Berhasil menghapus diskon.");
    }
}
