<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class BaseApiController extends Controller {

    protected $rest;
    protected $db;
    protected $validation;
    protected $helpers = ["common", "form", "url"];
    public $date;
    public $datetime;

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
        $this->date = date('Y-m-d');
        $this->datetime = date('Y-m-d H:i:s');

        $this->rest = \Config\Services::RestLib();

        $this->db = \Config\Database::connect();
        $this->validation = \Config\Services::validation();
    }

    /**
     * Digunakan untuk membuat respon sukses.
     *
     * @param string            $msg
     * @param array|string|null $result
     *
     * @return void
     */
    public function respondSuccess($msg = "OK.", $result = array()) {
        $respond["status"] = "success";
        $respond["msg"] = $msg;
        $respond["result"] = (object) $result;

        $this->rest->createRespond(200, $respond);
    }

    /**
     * Digunakan untuk membuat respon failed.
     *
     * @param string            $msg
     * @param array|string|null $result
     *
     * @return void
     */
    public function respondFailed($msg = "Bad Request.", $result = array()) {
        $respond["status"] = "failed";
        $respond["msg"] = $msg;
        $respond["result"] = (object) $result;

        $this->rest->createRespond(200, $respond);
    }

    /**
     * Digunakan untuk membuat respon failed.
     *
     * @param string            $msg
     * @param array|string|null $result
     *
     * @return void
     */
    public function respondValidation($msg = "Bad Request.", $error = array()) {
        $respond["status"] = "validation";
        $respond["msg"] = $msg;
        $respond["result"] = (object) array(
                    "error" => (object) $error
        );

        $this->rest->createRespond(200, $respond);
    }

    /**
     * Digunakan untuk membuat respon bahwa user tidak mempunyai permission atau unauthorized.
     *
     * @param string            $msg
     * @param array|string|null $errorData
     *
     * @return void
     */
    public function respondUnauthorized($msg = "Unauthorized.", $result = array()) {
        $respond["status"] = "unauthorized";
        $respond["msg"] = $msg;
        $respond["result"] = (object) $result;

        $this->rest->createRespond(401, $respond);
    }

    /**
     * Digunakan untuk membuat respon request tidak ditemukan.
     *
     * @param string            $message
     * @param array|string|null $errorData
     *
     * @return void
     */
    public function respondNotFound($msg = "Not Found.", $result = array()) {
        $respond["status"] = "notfound";
        $respond["msg"] = $msg;
        $respond["result"] = (object) $result;

        $this->rest->createRespond(404, $respond);
    }

}
