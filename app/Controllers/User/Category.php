<?php 
namespace App\Controllers\User;

use App\Controllers\BaseUserController;

class Category extends BaseUserController
{

	public function list()
	{
		$table = "category";

        $defaultSort = "category_id";
        $defaultDir = "DESC";

        $arrField = array(
        	'category_id',
        	'category_user_id',
        	'category_name',
        	'category_icon',
        	'category_image',
        	'category_is_active',
        	'category_is_deleted',
        );

        $where = "category_is_active = 1 AND category_is_deleted = 0 AND category_user_id = '{$this->dataMember->user_id}'";
        $join = "";

        $limit = (integer) $this->request->getGet('limit') <= 0 ? 10 : (integer) $this->request->getGet('limit');
        $page = (integer) $this->request->getGet('page') <= 0 ? 1 : (integer) $this->request->getGet('page');

        $search = (array) $this->request->getGet('search');
        $filter = (array) $this->request->getGet('filter');
        $sort = (string) $this->request->getGet('sort');
        $dir = (string) strtoupper($this->request->getGet('dir'));

        if ($dir !== 'ASC' && $dir !== 'DESC') {
            $dir = $defaultDir;
        }

        $start = ($page - 1) * $limit;

        $joinDetail = empty($join) ? "" : $join;
        $whereDetail = empty($where) ? " 1 = 1 " : $where;

        if (is_array($search)) {
            $whereDetail .= buildWhereSearch($search, $arrField);
        }

        if (is_array($filter)) {
            $whereDetail .= buildWhereFilter($filter, $arrField);
        }

        if (!in_array($sort, $arrField)) {
            $sort = $defaultSort;
        }

        $strField = empty($arrField) ? '*' : implode(',', $arrField);

        $sql = "
        SELECT SQL_CALC_FOUND_ROWS
        {$strField}
        FROM {$table}
        {$joinDetail}
        WHERE {$whereDetail}
        ORDER BY {$sort} {$dir}
        LIMIT {$start}, {$limit}
        ";

        $queryResult = $this->db->query($sql);

        $totalData = 0;
        $dataResult = array();

        if ($queryResult->resultID->num_rows > 0) {

            $sqlTotal = "SELECT FOUND_ROWS() AS row";

            $totalData = (integer) $this->db->query($sqlTotal)->getRow()->row;

            $result = $queryResult->getResult();

            foreach ($result as $row) {
                $dataResult[] = nullToString($row);
            }
        }

        $data = array(
            'data' => $dataResult,
            'pagination' => pageGenerator($totalData, $page, $limit)
        );

        $status = 'OK';
        return $this->appResponse($this->response::HTTP_OK, 'Berhasil mendapatkan data kategori!', $data, $status);
	}

	public function options(){
        $data = $this->db->table('category')->select('category_id AS value, category_name AS text')->getWhere(['category_is_active' => 1, 'category_is_deleted' => 0, 'category_user_id' => $this->dataMember->user_id])->getResultArray();
        $status = "OK";
        return $this->appResponse($this->response::HTTP_OK, 'Berhasil mendapatkan data options kategori!', $data, $status);
    }

    public function create()
    {
    	$this->validation->setRules([
			'name' => [
				'label' => 'nama',
				'rules' => 'required',
				'errors' => [
					'required' => 'tidak boleh kosong',
				],
			],
		]);
        $validationRun = $this->validation->withRequest($this->request)->run();
        $arrError = $this->validation->getErrors();
        if (!$validationRun) {
            return $this->appResponse($this->response::HTTP_BAD_REQUEST, $arrError);
        } else {
        	$message = "";
        	try {
        		$isError = FALSE;
        		$data['category_name'] = $this->request->getPost('name');
        		$data['category_user_id'] = $this->dataMember->user_id;
        		$data['category_icon'] = $this->request->getPost('icon');
        		$data['category_image'] = $this->request->getPost('img');
        		$check_name = $this->db->table('category')->select('category_name')->getWhere(['category_name' => $this->request->getPost('name'), 'category_user_id' => $this->dataMember->user_id, 'category_is_deleted' => 0]);
        		if ($this->db->affectedRows() > 0) {
        			// $status = "ERROR";
        			// return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Nama sudah digunakan!');
        			$isError = TRUE;
        			throw new \Exception("Nama kategori sudah digunakan!", 1);
        			
        		}
        		if (!$this->db->table('category')->insert($data)) {
        			$isError = TRUE;
        			throw new \Exception("Gagal menambahkan kategori!", 1);
        			// $status = "ERROR";
        			// $this->respondFailed("Gagal menambahkan kategori.");
        		}
        		$response['category_id'] = $this->db->insertID();
        	} catch(\Exception $ex) {
        		$isError = TRUE;
				$message = $ex->getMessage();
        	}

        	if (!$isError) {
        		if ($this->db->transStatus() == TRUE) {
	                $this->db->transCommit();
	                $status = 'OK';
	                $status = "OK";
        			return $this->appResponse($this->response::HTTP_OK, 'Berhasil menambahkan kategori!', $response, $status);
	            } else {
	                $this->db->transRollback();
	                $status = 'ERROR';
	                return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Gagal menambahkan kategori! '.$message, array(), $status);
	            }
        	} else {
        		$status = 'ERROR';
        		return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Gagal menambahkan kategori! '.$message, array(), $status);
        	}
        }
    }

    public function delete($id = null)
    {
    	$this->validation->setRules([
			'id' => [
				'label' => 'ID',
				'rules' => 'required',
				'errors' => [
					'required' => 'tidak boleh kosong',
				],
			],
		]);
		$validationRun = $this->validation->withRequest($this->request)->run();
        $arrError = $this->validation->getErrors();
        if (!$validationRun) {
            return $this->appResponse($this->response::HTTP_BAD_REQUEST, $arrError);
        } else {
        	$message = "";
        	try {
        		print_r($this->request->getPost('id'));die;
        	} catch (\Exception $ex) {
        		$isError = TRUE;
        		$message = $ex->getMessage();
        	}
        }
    }

    public function update($id = null)
    {
        print_r('$check_name');die;
        $this->validation->setRule([
        	'name' => [
				'label' => 'nama',
				'rules' => 'required',
				'errors' => [
					'required' => 'tidak boleh kosong',
				],
			],
			'id' => [
				'label' => 'id',
				'rules' => 'required',
				'errors' => [
					'required' => 'tidak boleh kosong',
				],
			],
        ]);
        $validationRun = $this->validation->withRequest($this->request)->run();
        $arrError = $this->validation->getErrors();
        if (!$validationRun) {
            return $this->appResponse($this->response::HTTP_BAD_REQUEST, $arrError);
        }
        $name = $this->request->getPost('name');
        $id = $this->request->getPost('id');
        $check_name = $this->db->table('category')->select('category_name, category_id')->getWhere(['category_name' => $name, 'category_user_id' => $this->dataMember->user_id, 'category_is_deleted' => 0]);
        if ($this->db->affectedRows() > 0) {
            if ($check_name->getRow('category_id') !== $id){
            	return $this->appResponse($this->response::HTTP_BAD_REQUEST, array('name' => "Nama sudah digunakan"));
            }
        }
        $data['category_name'] = $name;
        $data['category_icon'] = $this->request->getPost('icon');
        $data['category_image'] = $this->request->getPost('img');
        $this->db->table('category')->where('category_id', $$id)->update($data);
        if ($this->db->affectedRows() < 0) {
            $this->respondFailed("Gagal memperbarui kategori.");
            return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Gagal memperbarui kategori.');
        }
        return $this->appResponse($this->response::HTTP_OK, 'Berhasil memperbarui kategori.');
    }

}