<?php

namespace App\Controllers;

use App\Controllers\BaseApiController;

class ApiAuthUserController extends BaseApiController{
    
    public $user = array();

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
        
        $this->user = $this->userAuth();
    }
    
    private function userAuth() {
        $rest = \Config\Services::RestLib();
        
        $token = $rest->getBearerToken();
        
        $sql = "
            SELECT *
            FROM user_auth
            JOIN user ON user_id = user_auth_user_id
            WHERE user_auth_token = '{$token}'
            ORDER BY user_auth_id DESC
            LIMIT 1
        ";

        $queryUserAuth = $this->db->query($sql);

        if ($queryUserAuth->resultID->num_rows == 0) {
            $this->respondUnauthorized("Anda terdeteksi login di perangkat lain.");
        }

        $userAuth = $queryUserAuth->getRow();

        return $userAuth;
    }
}