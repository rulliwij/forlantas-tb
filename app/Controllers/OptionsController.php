<?php

namespace App\Controllers;

class OptionsController extends BaseController {

    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
        parent::initController($request, $response, $logger);
    }

    public function index() {
        return $this->response
                ->setHeader('Access-Control-Allow-Origin', '*') //for allow any domain, insecure
                ->setHeader('Access-Control-Allow-Headers', '*') //for allow any headers, insecure
                ->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE') //method allowed
                ->setStatusCode(200); //status code
        exit;
    }

}