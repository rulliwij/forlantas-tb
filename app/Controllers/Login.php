<?php 
namespace App\Controllers;

use App\Controllers\BaseController;

class Login extends BaseController
{

	public function index()
	{
		$this->validation->setRules([
			'mobilephone' => [
				'label' => 'mobilephone',
				'rules' => 'required',
				'errors' => [
					'required' => 'tidak boleh kosong',
				],
			],
		]);

		$validationRun = $this->validation->withRequest($this->request)->run();
		$arrError = $this->validation->getErrors();
		if (!$validationRun) {
			return $this->appResponse($this->response::HTTP_BAD_REQUEST, $arrError);
		} else {
			$isError = false;
			$arrData = array();
			$result = array();
			$this->db->transBegin();
			try {
				$mobilephone = $this->request->getPost('mobilephone');
				$datetime = date('Y-m-d H:i:s');
				$sql = "SELECT * FROM user WHERE user_mobilephone = '$mobilephone' AND user_is_active = 1 AND user_is_deleted = 0";
				$queryUser = $this->db->query($sql);
		        if ($queryUser->resultID->num_rows == 0) {
		        	return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Data nomor handphone tidak ditemukan!');
		        }
		        $user = $queryUser->getRow();
		        $otpCode = $this->generateOtp(4);
		        $arrData['user_otp'] = $otpCode;
		        $arrData['user_otp_created'] = $datetime;
		        $arrData['user_otp_expired'] = date('Y-m-d H:i:s', strtotime($datetime . "+7 days"));
		        $this->db->table('user')
		        ->where('user_id', $user->user_id)
		        ->update($arrData);
		        if ($this->db->affectedRows() < 1) {
		        	throw new \Exception("Gagal memperbarui kode otp", 1);
		        }
			} catch(\Exception $ex) {
				$isError = TRUE;
				$message = $ex->getMessage();
			}

			if (!$isError) {
				if ($this->db->transStatus() == TRUE) {
	                $this->db->transCommit();
	                $status = 'OK';
	                $result = array(
	                	'id' => $user->user_id
	                );
	                return $this->appResponse($this->response::HTTP_OK, 'Generate OTP berhasil!', $result, $status);
	            } else {
	                $this->db->transRollback();
	                $status = 'ERROR';
	                return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Generate OTP Gagal!', $result, $status);
	            }
			} else {
				$status = 'ERROR';
				return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Generate OTP Gagal!', $result, $status);
			}
			
			
		}
	}

	public function validationOtp($value='')
	{
		$this->validation->setRules([
			'id' => [
				'label' => 'id',
				'rules' => 'required',
				'errors' => [
					'required' => 'tidak boleh kosong',
				],
			],
			'otp' => [
				'label' => 'otp',
				'rules' => 'required|exact_length[4]',
				'errors' => [
					'required' => 'tidak boleh kosong',
					'exact_length' => 'jumlah kode otp tidak valid',
				],
			],
			'device' => [
				'label' => 'device',
				'rules' => 'required',
				'errors' => [
					'required' => 'tidak boleh kosong',
				],
			],
		]);

		$validationRun = $this->validation->withRequest($this->request)->run();
		$arrError = $this->validation->getErrors();
		if (!$validationRun) {
			return $this->appResponse($this->response::HTTP_BAD_REQUEST, $arrError);
		} else {
			try {
				$isError = FALSE;
				$id = $this->request->getPost('id');
				$otp = $this->request->getPost('otp');
				$device = $this->request->getPost('device');
				$fcm_token = $this->request->getPost('fcm_token');
				$datetime = date('Y-m-d H:i:s');
				$result = array();
				$sql = " SELECT * FROM user WHERE user_id = '{$id}' AND user_is_active = 1 AND user_is_deleted = 0";
				$queryUser = $this->db->query($sql);
				if ($queryUser->resultID->num_rows == 0) {
					return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Data user tidak ditemukan!');
				}
				$user = $queryUser->getRow();
				if ($user->user_otp !== $otp) {
					return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Kode OTP yang anda masukkan salah!');
				}
				if ($datetime > $user->user_otp_expired) {
					return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Kode OTP yang anda masukkan sudah tidak dapat dipakai! Silahkan login ulang!');	
				}
				$token = $this->generateToken($this->request->getIPAddress());
				$dataAuth = array(
					'user_auth_user_id' => $user->user_id,
					'user_auth_token' => $token,
					'user_auth_created' => $datetime,
					'user_auth_expired' => date('Y-m-d H:i:s', strtotime($datetime . "+7 days")),
					'user_auth_name' => $user->user_name,
					'user_auth_fullname' => $user->user_fullname,
					'user_auth_mobilephone' => $user->user_mobilephone,
					'user_auth_merchant_name' =>  $user->user_merchant_name,
					'user_auth_device_id' =>  $device,
				);
				$insert = $this->db->table('user_auth')->insert($dataAuth);
				if(!$insert) {
					throw new \Exception("Gagal generate token", 1);
				}
	            $tableUser = $this->db->table('user');
	            $tableUser->where('user_id', $user->user_id);
	            $tableUser->update(array("user_otp" => ""));
	            if ($this->db->affectedRows() < 0) {
	                throw new Exception("Gagal memperbarui kode otp!");
	            }
	            
			} catch(\Exception $ex) {
				$isError = TRUE;
				$message = $ex->getMessage();
			}

			if (!$isError) {
				if ($this->db->transStatus() == TRUE) {
	                $this->db->transCommit();
	                $sqlUser = "
			            SELECT user_auth_id,
			            user_auth_user_id,
			            user_auth_token,
			            user_auth_device_id,
			            user_auth_created,
			            user_auth_expired,
			            user_auth_name,
			            user_auth_fullname,
			            user_auth_mobilephone,
			            user_auth_image,
			            user_auth_fcm_token,
						user_auth_merchant_name
						FROM user_auth
		                WHERE user_auth_id = '{$user->user_id}'
		                ORDER BY user_auth_id DESC
		                LIMIT 1
		            ";
		            $dataUser = $this->db->query($sqlUser)->getRow();
	            	$result = nullToString($dataUser);
	                $status = 'OK';
	                return $this->appResponse($this->response::HTTP_OK, 'Login Berhasil!', $result, $status);
	            } else {
	                $this->db->transRollback();
	                $status = 'ERROR';
	                return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Gagal Login!', $result, $status);
	            }
			} else {
				$status = 'ERROR';
				return $this->appResponse($this->response::HTTP_BAD_REQUEST, 'Gagal Login!', $result, $status);
			}
		}
	}

	private function generateOtp($length)
    {
        $pinStr = "1234567809";
        for ($i = 0; $i < strlen($pinStr); $i++) {
            $pinChars[$i] = $pinStr[$i];
        }
        // randomize the chars
        srand((float) microtime() * 1000000);
        shuffle($pinChars);
        $pin = "";
        for ($i = 0; $i < 20; $i++) {
            $char_num = rand(1, count($pinChars));
            $pin .= $pinChars[$char_num - 1];
        }
        $pin = substr($pin, 0, $length);

        return $pin;
    }

    private function generateToken($param) {
        return sha1($param . microtime());
    }

}